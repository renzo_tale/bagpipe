
class roadRunnerParallel():
    # Run commands using qsub
    def __init__(self, engine):
        from lib.roadrunner.jobs import job
        # Single job parameters
        self.command = engine.submissionCMD
        self.statCMD = engine.checkStatus
        self.statEXT = engine.checkOutcome
        self.delCMD = engine.deleteJID
        self.jobArrayN = engine.jobArrayN
        self.jobCores = engine.jobCores
        self.taskIdFlag = engine.taskIdFlag
        self.jobIdFlag = engine.jobIdFlag
        self.jobNameFlag = engine.jobNameFlag
        self.processJobOutput = engine.processJobOutput
        self.prgname = None
        self.jobname = None
        self.additional_opts = None
        # Submitted job parameters
        self.commandlist = []
        self.commandNtasks = {}
        self.submittedJobs = {}
        self.completedJobs = []
        self.failedIdx = []
        self.other = []
        self.PIDstatus = {}
        self.modeName = engine.name
        self.myjobs = job()

    # Script parser
    def parse(self, *args, **kwargs):
        from os.path import isfile as Isfile
        if "script_name" not in kwargs:
            print("script_name not provided.")
            return 0

        # Basic options
        if kwargs.get("script_name", None) is None or not Isfile(kwargs["script_name"]):
            if kwargs.get("script_name", None) is None: print("No script provided.")
            if not Isfile(kwargs.get("script_name", None)): print("Cannot find the script.")
            return 0
        self.jobname = kwargs.get("jobname", None)
        self.prgname = kwargs.get("script_name", None)

        # Additional options
        if kwargs.get("options", None) is not None:
            self.additional_opts = kwargs.get("options", None)

        # Program arguments
        for arg in args:
            self.other.append(arg)

        ## Completed parsing

    # Save the parsed commands in a list
    def saveCommand(self):
        if self.prgname is None:
            print("Cannot run the script.")
            return 0
        if self.modeName == "local":
            cmd = []
        else:
            cmd = [self.command]
        if self.jobname is not None:
            cmd += ["-N", self.jobname]
        if self.additional_opts is not None:
            cmd += [self.additional_opts]
        cmd += [self.prgname]
        if len(self.other) > 0:
            for arg in self.other: cmd += [arg]
        cmd = ' '.join(map(str, cmd))
        self.commandlist.append(cmd)
        self.reset("args")
        #self.myjobs.add(cmd)
        #return cmd
        added = self.myjobs.add(cmd)
        return cmd, added
        

    # Resent once everything run successfully
    def reset(self, reset):
        from lib.roadrunner.jobs import job
        if reset == "args" or reset == "all":
            self.prgname = None
            self.jobname = None
            self.additional_opts = None
            self.other = []
        if reset == "commands" or reset == "all":
            self.commandlist = []
            self.submittedJobs = {}
            self.completedJobs = []
            self.failedIdx = []
            self.PIDstatus = {}
            self.myjobs.reset()

   
    # Run N Road Runners (Beep Beep! x N)
    def runCluster(self, concurrent):
        import time
        if len(self.commandlist) == 0: 
            return [[], []]
        try:
            # Submit job up to concurrent value
            concurrent = int(concurrent)
            if concurrent > self.myjobs.njobs: concurrent = len(self.commandlist)
            if concurrent == 1: print("Runinng {} job.".format(concurrent))
            else: print("Runinng {} jobs at a time.".format(concurrent))

            while self.myjobs.nsub < concurrent:
                n, cmd = self.myjobs.getNext()
                if n is not None:
                    print("Submitting: {}".format(cmd))
                    pid, tasks = self.submit(cmd, 0.05)
                    self.myjobs.submitted(n, pid)
                    time.sleep(0.5)
                    self.myjobs.saveCheckpoint()

            # Keep submitting jobs to mantain the predefined concurrency
            oldStats = ""
            while self.myjobs.nfinished != self.myjobs.njobs:
                job_status = {n: self.sentinel( self.myjobs.get_running(n = n)[1] ) for n in self.myjobs.get_running() }
                time.sleep(0.5)
                tot = sum( [job_status[n] for n in job_status] )
                # If everything is running, wait 10 secs and continue
                if tot == 0:
                    newStats = self.formatStats()
                    if newStats != oldStats: 
                        print(newStats)
                        oldStats = newStats
                    self.myjobs.saveCheckpoint()
                    time.sleep(0.5)
                    continue

                # Otherwise, check failures and/or success, and submit next job
                for n in job_status:
                    # Move to running jobs
                    if job_status[n] == 1:
                        self.myjobs.running(n)
                        self.myjobs.saveCheckpoint()
                    # Move to failed jobs
                    if job_status[n] == 4:
                        self.myjobs.failed(n)
                        self.myjobs.saveCheckpoint()
                    # Move to finished jobs
                    elif job_status[n] == 2:
                        fail = self.checkFails(self.myjobs.get_running(n = n)[1])
                        if fail == 1:
                            self.myjobs.failed(n)
                            self.myjobs.saveCheckpoint()
                        else:
                            self.myjobs.succeed(n)
                            self.myjobs.saveCheckpoint()
                    # Check and submit more jobs
                    if self.myjobs.nsub < concurrent and self.myjobs.issued() > self.myjobs.get_n_processes():
                        n, cmd = self.myjobs.getNext()
                        if n not in self.myjobs.get_running() and n not in self.myjobs.get_finished():
                            print("Submitting: {}".format(cmd))
                            pid, tasks = self.submit(cmd, 0.05)
                            self.myjobs.submitted(n, pid)
                            self.myjobs.saveCheckpoint()
                time.sleep(0.5)
                self.myjobs.saveCheckpoint()
                newStats = self.formatStats()
                if newStats != oldStats: 
                    print(newStats)
                    oldStats = newStats
            newStats = self.formatStats()
            if newStats != oldStats: 
                print(newStats)
            
            # Clean saved data on submission
            outcome = [self.completedJobs, self.failedIdx]
            self.reset("commands")
            return outcome
        except KeyboardInterrupt:
            import sys
            print("User interruption")
            RunningToKill = [n for n in self.myjobs.jobs["Running"] ]
            QueuedToKill = [n for n in self.myjobs.jobs["Queued"] ]
            for n in RunningToKill:
                self.killPID(self.myjobs.jobs["Running"][n][1], 0.05)
                self.myjobs.killed(n)
                self.myjobs.saveCheckpoint()
                self.myjobs.dumpArchive()
            for n in QueuedToKill:
                self.killPID(self.myjobs.jobs["Queued"][n][1], 0.05)
                self.myjobs.killed(n)
                self.myjobs.saveCheckpoint()
                self.myjobs.dumpArchive()
            sys.exit()
 
    # Run a local job.
    def workLocal(self, cmd):
        import subprocess as sbp
        p = sbp.Popen(cmd, shell=True)
        p.wait()


    # Run N Road Runners (Beep Beep! x N)
    def runLocal(self, concurrent):
        from multiprocessing.pool import ThreadPool
        try:
            # Submit job up to concurrent value
            concurrent = int(concurrent)
            threadPool = ThreadPool(concurrent)
            for cmd in self.commandlist:
                threadPool.apply_async(self.workLocal, (cmd,))
            threadPool.close()
            threadPool.join()
        except KeyboardInterrupt:
            threadPool.terminate()
            threadPool.join()
            return 0

    # Run module.
    def run(self, concurrent):
        if self.modeName == "SGE":
            print("Running in cluster mode")
            # outcome = self.runCluster(concurrent)
            outcome = self.runCluster(concurrent)
        else: 
            outcome = self.runLocal(concurrent)
        return outcome


    # Run the one Road Runner (Beep Beep!).
    def runSingle(self, njob):
        import time
        # Submit job up to concurrent value
        if len(self.commandlist) < njob: 
            print("Cannot run job #{}".format(njob))
            return 1
        if self.commandlist[njob] in self.completedJobs:
            proceed = "n"
            proceed = raw_input("This command has already been done, are you sure to repeat it? y/N").lower()
            if proceed == "n" or proceed == "":
                return 0
            else:
                del self.completedJobs[self.completedJobs.index(self.commandlist[njob])]
        pid, tasks = self.submit(self.commandlist[njob], 0.05)
        print("Submitted: {}".format(self.commandlist[njob]))
        self.submittedJobs[pid] = self.commandlist[njob]
        time.sleep(10)
        # Keep submitting jobs to mantain the predefined concurrency
        while self.commandlist[njob] not in self.completedJobs:
            pid_status = {pid:self.sentinel(pid) for pid in self.submittedJobs}
            time.sleep(10)
            tot = sum([pid_status[pid] for pid in pid_status])
            # If everything is running, wait 10 secs and continue
            if tot == 0:
                time.sleep(10)
                print(self.formatStats())
                continue
            # Otherwise, check failures and/or success, and submit next job
            for pid in pid_status:
                val = pid_status[pid]
                time.sleep(20)
                if val == 1 or self.checkFails(pid):
                    self.PIDstatus[pid] = "Failed"
                    self.failedIdx.append(self.commandlist.index(self.submittedJobs[pid]))
                elif val == 2:
                    self.completedJobs.append(self.commandlist.index(self.submittedJobs[pid]))
                    del self.submittedJobs[pid]
            print(self.formatStats())
        # Clean saved data on submission
        outcome = [self.completedJobs, self.failedIdx]
        self.reset("commands")
        return outcome


    # Print statistics over the runtime
    def formatStats(self):
        outStr = "\nStatus report: \n"
        jobs = {"Queued": sorted( [self.myjobs.jobs["Queued"][n][1] for n in self.myjobs.jobs["Queued"]] )}
        jobs.update( {"Running" : sorted( [self.myjobs.jobs["Running"][n][1] for n in self.myjobs.jobs["Running"]] ) } )
        jobs.update( {"Failed": sorted( [self.myjobs.jobs["Failed"][n][1] for n in self.myjobs.jobs["Failed"]] ) } )
        jobs.update( {"Succeed": sorted( [self.myjobs.jobs["Succeed"][n][1] for n in self.myjobs.jobs["Succeed"]] ) } )
        for res in ["Queued", "Running", "Succeed", "Failed"]:
            outStr += "{}: {}\n".format(res, '|'.join(map(str, jobs[res] ) ) )
        return outStr


    # Submit a new job
    def submit(self, cmd, waitSec):
        import time
        import subprocess as sbp
        p = sbp.Popen(cmd, shell = True, stdout=sbp.PIPE)
        stout = p.stdout.read().decode("utf-8")
        time.sleep(waitSec)
        pid, tasks = self.getNtasks(stout.strip())
        return pid, tasks

    # Kill a job
    def killPID(self, pid, waitSec):
        import time
        import subprocess as sbp
        print "Killing {}".format(pid)
        proceed = False
        while not proceed:
            p = sbp.Popen("{} {}".format(self.delCMD, pid), shell = True, stdout=sbp.PIPE)
            time.sleep(.5)
            checkOut = True
            while checkOut:
                try:
                    stats = sbp.check_output([self.statCMD])
                    stats = stats.decode("utf-8").splitlines()
                    checkOut = False
                except:
                    continue
            inline = sum([1 for line in stats if pid in line])
            if inline == 0: 
                proceed = True
        return "Killed {}".format(pid)


    # Define number of tasks for an array job
    def getNtasks(self, val):
        val = val.split()[2].split(".")
        if len(val) == 1:
            return val[0], None
        else:
            pid = val[0]
            vi, ve, interval = [k for i in val[1].split("-") for k in i.split(":")] 
            return [pid, [i for i in range(int(vi), int(ve) + 1, int(interval))] ]
    

    # Control single job status
    def sentinel(self, pid):
        import subprocess as sbp
        import time

        # Get date of the current job
        today = time.strftime("%m/%d/%Y")

        # Get initial status of the job, and assume it is runnin
        checkOut = True
        while checkOut:
            try:
                stats = sbp.check_output([self.statCMD])
                stats = stats.decode("utf-8").splitlines()
                checkOut = False
            except:
                continue
        # Check that job is running
        tasks = []
        # Screen job is running ones
        for line in stats:
            line = line.strip().split()
            if pid is not None and pid in line:
                tasks.append(line)
            elif pid is None and self.jobname[0:9] in line and today in line[5]:
                tasks.append(line)
        return self.status(pid, tasks)



    # Get status of all jobs submitted
    def status(self, pid, tasks):
        import subprocess as sbp
        import time
        # If no jobs with the specified pid/job name are present, return 2
        tasksStatus = 0
        if len(tasks) == 0:
            self.PIDstatus[pid] = "Done"
            return 2
        else:
            for task in tasks:
                if task[4] == "Eqw":
                    try:
                        sbp.check_output("{} {}".format(self.delCMD, pid)).decode("utf-8")
                        self.PIDstatus[pid] = "Fail"
                        time.sleep(5)
                        tasksStatus = 4
                    except:
                        sbp.check_output("{} {}".format(self.delCMD, pid)).decode("utf-8")
                        self.PIDstatus[pid] = "Fail"
                        time.sleep(5)
                        tasksStatus = 4
                elif task[4] == "qw":
                    try: 
                        self.PIDstatus[pid] = "Wait"
                    except:
                        self.PIDstatus[task[0]] = "Wait"
                    tasksStatus = 0
                elif task[4] == "r": 
                    try:
                        self.PIDstatus[pid] = "Run "
                    except:
                        self.PIDstatus[task[0]] = "Run "
                    tasksStatus = 1
        return tasksStatus


    # Process qacct job output
    def getJobOutputs(self, pid):
        import subprocess as sbp
        import time

        # create status variables
        jobn = None
        joboutp = None
        tasksoutput = {}

        # Get job details
        stats = ''
        while stats is None or stats == '':
            try:
                stats = sbp.check_output(["{} {}".format(self.statEXT, pid)], shell=True)
            except:
                time.sleep(1)
                stats = ''
        stats = stats.decode("utf-8").splitlines()

        # Check output for 1-n job(s)
        for line in stats:
            line = line
            if "=" in line:
                if jobn is not None and joboutp is not None:
                    tasksoutput[jobn] = joboutp
                    jobn = None
                    joboutp = None
                continue
            line = line.strip().split()
            if line[0] == "taskid":
                jobn = line[1]
            elif line[0] == "failed":
                joboutp = line[1]
        if jobn is not None and joboutp is not None:
            tasksoutput[str(jobn)] = str(joboutp)

        return tasksoutput
        

    # Understand whether a job failed or not
    def checkFails(self, pid):
        # Check if pid is available    
        if pid is None:
            print("Cannot check job status if no pid is found.")
            return 0

        # Get job outcomes
        tasksoutput = self.getJobOutputs(pid)
        # Define the jobs that failed, and print why 
        failed = 0
        for key in tasksoutput.keys():
            if tasksoutput[key] != "0" and key != "undefined":
                failed += 1
                continue
            if tasksoutput[key] != "0" and key == "undefined":
                failed += 1
        # If one or more jobs failed, return error code.
        if failed:
            return 1
        return 0


    #
    # Dev functions
    #
      

    # Control single job status
    def sentinel_dev(self, pid, tasks):
        import subprocess as sbp
        import time

        # Get date of the current job
        today = time.strftime("%m/%d/%Y")

        # Get initial status of the job, and assume it is runnin
        checkOut = True
        while checkOut:
            try:
                stats = sbp.check_output([self.statCMD])
                stats = stats.decode("utf-8").splitlines()
                checkOut = False
            except:
                continue
        
        # Check that job is running
        jobs = []
        # Screen job is running ones
        for line in stats:
            line = line.strip().split()
            if pid is not None and pid in line:
                jobs.append(line)
            elif pid is None and self.jobname[0:9] in line and today in line[5]:
                jobs.append(line)
        return self.status_dev(pid, jobs, tasks)
        


    # Get status of all jobs submitted
    def status_dev(self, pid, lines, tasksStatus):
        import subprocess as sbp
        import time
        # If no jobs with the specified pid/job name are present, return 2
        if len(lines) == 0:
            self.PIDstatus[pid] = "Done"
            return 2
        else:
            # Fix status format for each line
            lines_f = []
            for line in lines:
                lf = []
                if "@" not in line[7]:
                    lf = line[0:7] + ["empty@address"] + line[7:len(line)]
                else:
                    lf = line
                if len(lf) < 10:
                    lf = lf + ["1"]
            print(lines_f)

            tasksStatus = {}
            for line in lines_f:
                if line[4] == "Eqw":
                    try:
                        sbp.check_output("{} {}".format(self.delCMD, pid)).decode("utf-8")
                        self.PIDstatus[pid] = "Fail"
                        time.sleep(5)
                        tasksStatus.update({tid:"Fail" for tid in self.getNtasks(line[9])})
                    except:
                        sbp.check_output("{} {}".format(self.delCMD, pid)).decode("utf-8")
                        self.PIDstatus[pid] = "Fail"
                        time.sleep(5)
                        tasksStatus.update({tid:"Fail" for tid in self.getNtasks(line[9])})
                elif line[4] == "qw":
                    try: 
                        tasksStatus.update({i:"Wait" for i in self.getNtasks(line[9])})
                        self.PIDstatus[pid] = "Wait"
                    except:
                        tasksStatus.update({i:"Wait" for i in self.getNtasks(line[9])})
                        self.PIDstatus[line[0]] = "Wait"

                elif line[4] == "r": 
                    try:
                        tasksStatus.update({i:"Run" for i in self.getNtasks(line[9])})
                        self.PIDstatus[pid] = "Run "
                    except:
                        tasksStatus.update({i:"Run" for i in self.getNtasks(line[9])})
                        self.PIDstatus[line[0]] = "Run "
        return tasksStatus



