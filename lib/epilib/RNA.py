########################################################
# This file contains all methods working on fastq data #
# including trimming, fastqc and pigz                  #
########################################################

import os 
from os.path import isfile as Isfile
from numpy import repeat as rp
from lib.Utilities import Header
from lib.Utilities import creates_pname

def index_refs_RNA(opts, outcommand, steps, roadrunner):
    try: os.mkdir('./REFERENCE/')
    except: 'Folder ./REFERENCE ready.'
    # Start create header for SH file.
    dependencies = ["hisat2", "samtools"]
    h = Header(opts)
    prname = "./SCRIPTS/" + next(steps) + "-RNAindexer.sh"
    pname = creates_pname("ai")
    h.processname(pname)
    h.setcores(1)
    minmemory_tot = 64
    h.setmemory(minmemory_tot/int(opts.nthreads))
    h.runtime(18)
    h.stderrSet("./LOGS/{}.err".format(pname))
    h.stdoutSet("./LOGS/{}.out".format(pname))
    h.setprj(opts.prj)
    h.holdjid(opts.wait + opts.waitad)
    h.holdjidad(None)
    opts.wait += pname + ","
    #opts.wait = "genomeindex"
    h.addmailaddr(opts.mailaddr)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 

    header += "if [ ! -e REFERENCE ]; then mkdir REFERENCE; fi\n"
    header += '''refA={}\n'''.format(opts.ref_genome_path)
    header += '''cp $refA REFERENCE/myref.fa\n'''
    header += '''samtools faidx ./REFERENCE/myref.fa\n'''
    header += '''awk 'BEGIN{OFS="\t"};$1!~"mm_"{print $1,0,$2,$1}' ./REFERENCE/myref.fa.fai > ./LISTS/ctg2keep.bed\n'''
    header += '''hisat2-build REFERENCE/myref.fa REFERENCE/myref\n'''

    outf = open(prname,'w')
    outf.write(header)


    roadrunner.parse(script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    cmd = "qsub %s"
    outcommand.write('%s\n' % cmd%prname)

    return None





# FastQC analysis
def FastQC_RNA(opts, samples, sample_dict, filename, outcommand, nstep, nsamples, roadrunner):

    prname = "./SCRIPTS/" + nstep + '-FastQC.sh'

    # Start create header for SH file.
    dependencies = ["java", "fastqc"]
    h = Header(opts)
    nlibraries = 0
    for sample in sample_dict:
        nlibraries += len(sample_dict[sample])
    pname = creates_pname("fq")
    h.processname(pname)
    h.setcores(opts.nthreads)
    minmemory_tot = 32
    h.setnjobs(nlibraries)
    h.setconcurrency(opts.nprocesses)
    h.setmemory(minmemory_tot/int(opts.nthreadsAlignment))
    h.runtime(6)
    h.addmailaddr(opts.mailaddr)
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' | tr ',' ' ' )\n\n'''
    header += '''if [ !  -e ./QC ]; then mkdir ./QC; fi\n'''
    header += '''if [ ! -e ./QC/$sample ]; then mkdir ./QC/$sample; fi\n'''


    try: os.mkdir('./QC/')
    except: 'Folder ./QC ready.'

    outf = open(prname, 'w')
    outf.write(header)
    
    outf.write('%s\n' % ' '.join(['fastqc $reads -o QC/$sample', '--threads ${}'.format(roadrunner.jobCores)]))
    outf.close()

    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()

    cmd = 'qsub {} {}'.format(prname, filename)
    outcommand.write('%s\n' % (cmd))
    print 'Done fastqc script.'



# Make trimgalore submission script
def trimgalore_RNA(opts, samples, readDict, filename, outcommand, nstep, nsamples, roadrunner):

    prname = "./SCRIPTS/" + nstep + "-trimgalore.sh"
    
    # Start create header for SH file.
    if "trimgalore" in opts.customModules:
        dependencies = ["trimgalore"]
    else:
        dependencies = ["trimgalore", "anaconda", "CondaEnv"]
    h = Header(opts)
    #h.processname("trimdata")
    nlibraries = 0
    for sample in readDict:
        nlibraries += len(readDict[sample])
    pname = creates_pname("tg")
    h.processname(pname)
    h.setcores(opts.nthreadsAlignment)
    minmemory_tot = 24
    h.setmemory(minmemory_tot/int(opts.nthreadsAlignment))
    h.runtime(48)
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    h.setnjobs(nlibraries)
    h.setconcurrency(opts.nprocesses)
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    h.setprj(opts.prj)
    opts.waitad += pname + ","
    h.addmailaddr(opts.mailaddr)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get()
    header += '''\n''' 
    #header += '''if [ ! -z $trimgalore ]; then export PATH=$PATH:$trimgalore; fi\n''' 
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' | tr ',' ' ' )\n\n'''
    header += '''Rreads=$( python -c "import sys;print ['--paired ' + ' '.join(sys.argv[1:]) if len(sys.argv[1:]) == 2 else ' '.join(sys.argv[1:])][0]" $reads)\n\n'''
    header += '''rName=`python -c "import sys; print(sys.argv[1].replace('.interleaved', '').replace('.fq.gz', '.fastq.gz').replace('.fastq.gz', '').split('/')[-1] )" $reads`\n'''
    header += '''echo $rName\n'''
    header += '''if [ !  -e ./TRIM ]; then mkdir ./TRIM; fi\n'''
    header += '''if [ ! -e ./TRIM/$sample ]; then\n\tmkdir ./TRIM/$sample;\nfi\n'''
    header += '''if [ `trim_galore --version | awk ' $1 == "version" {print $2}' | python -c 'import sys; print(sys.stdin>="0.6.0")' ` == "True" ]; then\n'''
    header += '''\ttrim_galore --cores ${} -o ./TRIM/$sample $Rreads\n'''.format(roadrunner.jobCores)
    header += '''else\n'''
    header += '''\ttrim_galore -o ./TRIM/$sample $Rreads\n'''
    header += '''fi\n'''
    outf = open(prname,'w')
    outf.write(header)
   
    print 'Setting trimmomatic pipeline...'    

    outf.close()




    # Creating list of outputs for the alignment analysis.
    try: os.mkdir('TRIM')
    except: print 'Folder TRIM/ ready'
    outlist = open('./LISTS/trimmedlist.txt', 'w')
    for sample in samples:
        # Making sample folder.
        try: os.mkdir(os.path.join('TRIM', sample))
        except: print 'Folder TRIM/' + sample + ' exists.'
        
        # Create input file names
        for lista in readDict[sample]:
            # Define base names
            file_names = [os.path.join("./TRIM", sample, i.split("/")[-1].replace('1.fq.gz', "1_val_1.fq.gz").replace('2.fq.gz', "2_val_2.fq.gz").replace('1.fastq.gz', "1_val_1.fq.gz").replace('2.fastq.gz', "2_val_2.fq.gz")) for i in lista]    
            outlist.write('%s\t%s\n' % (sample, ','.join(file_names)))
    outlist.close()

    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))
    print 'List of trimming outputs: ./LISTS/trimmedlist.txt'

    return './LISTS/trimmedlist.txt'





# Make alignment script
def AlignRNA(opts, samples, filename, outcommand, nstep, roadrunner):
    prname = "./SCRIPTS/" + nstep + '-hisat2.sh'
    print 'Setting up pipeline for stringtie...'
    if not os.path.exists("ALIGN"): os.mkdir('ALIGN/')
    outf = open(prname, 'w')
    nalignments = 0
    for sample in samples:
        nalignments += len(samples[sample])
    
    # Start create header for SH file.
    dependencies = ["bowtie2", "samtools", "hisat2", "gatk"]
    h = Header(opts)
    pname = creates_pname("b2")
    h.processname(pname)
    h.setcores(opts.nthreadsAlignment)
    h.setmemory(16)
    if opts.scatterSize != 0:
        h.runtime(47)
    else:
        h.runtime(160)
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    if nalignments > opts.nsamples: h.setnjobs(nalignments)
    else: h.setnjobs(opts.nsamples)
    h.setconcurrency(opts.nprocesses)
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    
    # Check if need to merge afterwards, otherwise run with array locker
    list2merge = [sample for sample in samples if len(samples[sample]) > 1]
    if len(list2merge) == 0:
        opts.waitad += pname + ","
    else:
        opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' | tr ',' ' ' )\n'''
    header += '''read1=($reads)\n'''
    header += '''rName=$(basename $read1)\n'''
    header += '''rName=$(python -c 'import sys; print [sys.argv[1].replace(".fq.gz", ".fastq.gz").replace(".fastq.gz", ""), sys.argv[1].replace(".fq.gz", ".fastq.gz").replace("_1_val_1.fastq.gz", "").replace("_1.fastq.gz", "").replace("_R1.fastq.gz", "").replace("_R2.fastq.gz", "").replace("_2_val_2.fastq.gz", "").replace("_2.fastq.gz", "")]["_1.fastq.gz" in sys.argv[1].replace(".fq.gz", ".fastq.gz") or "_R1.fastq.gz" in sys.argv[1].replace(".fq.gz", ".fastq.gz")]' $rName)\n'''
    header += '''interleaved=`python -c "import sys;print ' '.join(['-U {}'.format(i) if '_1.fq.gz' not in i and '_R1.fq.gz' not in i and '_1.fastq.gz' not in i and '_R1.fastq.gz' not in i else '-1 {}'.format(i) if '_1.fq.gz' in i else '-2 {}'.format(i) for i in sys.argv[1:] ])" $reads`\n'''

    outf.write(header)
    bowtiecommand = '''echo "hisat2 --dta -X 1000 -p ${0} -k 10 -x REFERENCE/myref $interleaved | samtools view -bu - | samtools sort -@ ${0} -m 5G - > ALIGN/$sample/$rName.bam"\n'''.format(roadrunner.jobCores)
    bowtiecommand += '''\nhisat2 --dta -p ${0} -k 10 -x REFERENCE/myref $interleaved \
	| samtools view -bu - | samtools sort -@ ${0} -m 5G - > ALIGN/$sample/$rName.bam\n'''.format(roadrunner.jobCores)
    bowtiecommand += '''samtools index ALIGN/${sample}/${rName}.bam\n'''
    outf.write(bowtiecommand)
    
    
    # Get alignment statistic
    outf.write('%s\n' % '\n# Get alignment statistics.')
    outf.write('%s\n' % ' '.join([opts.path_to_GATK, 'FlagStat', 
                                    '-I ./ALIGN/$sample/$rName.sort.bam', opts.GATKoptions.get("FlagStat", "")]))
    outf.write('%s\n' % '\n# Get alignment statistics.\nsamtools flagstat ./ALIGN/$sample/$rName.sort.bam > ./ALIGN/$sample/$rName.alignmentStats ')


    outlist = open('./LISTS/alignedlist.txt', 'w')
    tmprows = []
    for line in open(filename):
        sample, reads = line.strip().split()
        if not os.path.exists(os.path.join('./ALIGN/', sample)): os.mkdir('./ALIGN/' + sample)
        reads = reads.split(",")
        rName = '_'.join(reads[0].split("/")[-1].replace("_val_1.fastq.gz", "").replace("_val_1.fq.gz", "").replace(".fastq.gz", "").split("_")[:-1])
        if len(rName) == 0:
            rName = reads[0].split("/")[-1].replace("_val.fastq.gz", "").replace("_val.fq.gz", "").replace(".fq.gz", ".fastq.gz").replace(".fastq.gz", "")
        tmprows.append('%s\t%s\n' % (sample, 'ALIGN/'+sample+'/'+rName+'.sort.bam'))
    newrows = []
    for row in tmprows:
        if row not in newrows: newrows.append(row)
    for l in newrows:
        outlist.write('%s' % (l))
    outlist.close()
    

    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('%s\n' % ('qsub %s '%prname + filename))

    print 'List of aligned reads: ./LISTS/alignedlist.txt'
    return './LISTS/alignedlist.txt'


# Merge bowtie2 ATAC-seq alignments

# Merge multiple reads per sample
def merge_reads_RNA(opts, samples, sampledict, filename, outcommand, nstep, nsamples, roadrunner):

    # check that reads need to be merged; if not close and return previous step file name.
    list2merge = [sample for sample in samples if len(sampledict[sample]) > 1]
    if len(list2merge) == 0:
        return filename
    else:
        nSample2Merge = len(list2merge)


    prname = "./SCRIPTS/" + nstep + '-mergeReads.sh'
    print 'Setting up pipeline to merge aligned reads...'
    if not os.path.exists('./ALIGN/'): os.mkdir('./ALIGN/')
    outf = open(prname, 'w')

    # Start create header for SH file.
    dependencies = ["bamtools", "samtools"]
    h = Header(opts)
    pname = creates_pname("mr")
    h.processname(pname)
    h.setcores(opts.nthreadsAlignment)
    h.setmemory(16)
    h.runtime(96)
    h.holdjid(opts.wait + opts.waitad)
    h.holdjidad(None)
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setnjobs(nSample2Merge)
    h.setconcurrency(nSample2Merge)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get()     
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''if [ -e ALIGN/${sample}/${sample}.merged.bam.sort ]; then rm ALIGN/${sample}/${sample}.merged.bam.sort; fi\n'''
    header += '''if [ -e ./ALIGN/${sample}/tmplist.txt ]; then rm ./ALIGN/${sample}/tmplist.txt; fi\n'''
    header += '''if [ ! -e ./ALIGN/$sample ]; then mkdir ./ALIGN/$sample; fi\n'''
    header += '''for f in $(ls ./ALIGN/$sample/*.sort.bam); do rname=$(realpath ${f}); echo ${rname}; done >> ./ALIGN/${sample}/tmplist.txt\n'''
    header += '''echo "Merging: "; while read p; do echo ${p}; done < ./ALIGN/${sample}/tmplist.txt\n'''
    header += 'bamtools merge -list ./ALIGN/${sample}/tmplist.txt | samtools sort -T ALIGN/${sample}/ ' + '-@ ${}'.format(roadrunner.jobCores) + ' -m 5G - > ALIGN/${sample}/${sample}.merged.tmpa.bam\n'
    header += 'bamtools index -in ALIGN/${sample}/${sample}.merged.tmpa.bam\n'
    header += 'while read p; do rm ${p}; done < ./ALIGN/${sample}/tmplist.txt && rm ./ALIGN/${sample}/tmplist.txt\n'
    header += 'samtools sort -@ ${}'.format(roadrunner.jobCores) + ' -T ALIGN/${sample}/ -m 5G -n -o ALIGN/${sample}/${sample}.merged.bam.sort ALIGN/${sample}/${sample}.merged.tmpa.bam && rm ALIGN/${sample}/${sample}.merged.tmpa.bam\n'
    header += 'bamtools index -in ALIGN/${sample}/${sample}.merged.bam.sort\n'

    # Save script.
    outf.write('%s\n' % header)
    outf.write('''if [ ! -e ./OldOutputLogs ]; then mkdir ./OldOutputLogs; fi\n''')

    # Save list of sample to merge.
    sample2merge = open('./LISTS/sample2merge.txt', 'w')
    [sample2merge.write('%s\n' % sample) for sample in samples if len(sampledict[sample]) > 1]
    sample2merge.close()
    sample2merge = "./LISTS/sample2merge.txt"

    
    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('%s\n' % ('qsub %s '%prname + sample2merge))

    # Save merged 
    mergedSample = open('./LISTS/mergedSamples.txt', 'w')
    for sample in samples:
        if len(sampledict[sample]) > 1: 
            mergedSample.write('%s\t%s\n' % (sample, os.path.join('ALIGN',sample,sample + '.merged.bam.sort')))
            continue
    for line in open(filename):
        sample = line.strip().split()[0]
        if len(sampledict[sample])==1:
            mergedSample.write(line) 
    mergedSample.close()
    return './LISTS/mergedSamples.txt'




# Genrich wrapper
def Stringtie(opts, samples, filename, outcommand, nstep, roadrunner):
    # Check if GFF exists
    import os, sys
    testGff = '.'.join(opts.ref_genome_path.split(".")[0:-1])
    if os.path.exists(testGff + ".gff"):
        myGff = testGff + ".gff"
    elif os.path.exists(testGff + ".gff.gz"):
        myGff = testGff + ".gff.gz"
    elif os.path.exits(opts.gffFile):
        myGff = opts.gffFile
    else:
        sys.exit("No gff file provided for the current genome.\nExit\n")


    prname = "./SCRIPTS/" + nstep + '-Stringtie.sh'
    print 'Setting up pipeline for Stringtie...'
    if not os.path.exists("STRINGTIE"): os.mkdir('STRINGTIE/')
    outf = open(prname, 'w')
    nalignments = len(samples)
    
    # Start create header for SH file.
    dependencies = ["stringtie", "hisat2", "ncurses", "htslib", "samtools"]
    h = Header(opts)
    pname = creates_pname("st")
    h.processname(pname)
    h.setcores(4)
    h.setmemory(32)
    h.runtime(24)
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    if nalignments > opts.nsamples: h.setnjobs(nalignments)
    else: h.setnjobs(opts.nsamples)
    h.setconcurrency(opts.nprocesses)
    list2merge = [sample for sample in samples if len(samples[sample]) > 1]
    if len(list2merge) == 0:
        h.holdjid(opts.wait)
        h.holdjidad(opts.waitad)
    else:
        h.holdjid(opts.wait + opts.waitad)
        h.holdjidad(None)

    opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''if [ ! -z $genrich ]; then export PATH=$PATH:$genrich; fi\n''' 
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' )\n'''
    header += '''read1=($reads)\n\n'''
    header += '''if [ ! -e STRINGTIE/${sample} ]; then mkdir STRINGTIE/${sample}; fi\n\n'''
    header += '''samtools view -hb -L ./LISTS/ctg2keep.bed ${read1} > ${read1}.filt && samtools index ${read1}.filt\n'''
    header += "stringtie -p ${0} -G {1} -x MT -A ./STRINGTIE/$sample/$sample.stringtie.abundances.tab -C ./STRINGTIE/$sample/$sample.stringtie.cov_ref.gtf -o ./STRINGTIE/$sample/$sample.stringtie.out.gtf $read1.filt && rm $read1 $read1.filt\n".format(roadrunner.jobCores, myGff)
    header += '''echo "Done $sample"\n\n''' 
    outf.write(header)

    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('%s\n' % ('qsub %s '%prname + filename))

    print 'Stringtie script ready'
    return 0

