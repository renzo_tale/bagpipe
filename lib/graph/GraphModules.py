########################################################
# This file contains all methods to generate and       #
# process graph genomes                                #
########################################################


import os
from os.path import isfile as Isfile
from lib.Utilities import Header
from lib.Utilities import creates_pname


# Generate graph genome
def graphConstruct(opts, nstep, filename, outcommand, roadrunner):
    import sys
    prname = "./SCRIPTS/" + nstep + '-GraphConstruct.sh'
    print 'Setting up pipeline for graph genome generation...'
    if not os.path.exists("LOCALGRAPH"): os.mkdir('LOCALGRAPH/')
    
    # Start create header for SH file.
    dependencies = ["vg", "tabix"]
    h = Header(opts)
    pname = creates_pname("gc")
    h.processname(pname)
    h.setcores(12)
    h.setmemory(80)
    h.runtime(160)
    h.stderrSet("./LOGS/${}.err".format(roadrunner.jobNameFlag))
    h.stdoutSet("./LOGS/${}.out".format(roadrunner.jobNameFlag))
    h.setnjobs(1)
    h.setconcurrency(1)
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 

    # Check Graph type
    gType = None
    if ".vcf.gz" in opts.graph_genome_path:
        graphPrefix = opts.graph_genome_path.replace('.vcf.gz', '').split("/")[-1]
        fullGraphPrefix = opts.graph_genome_path.replace('.vcf.gz', '')
        fullGraphPath = '/'.join(opts.graph_genome_path.split("/")[0:-1])
        gType = "vcf"
    elif ".vg" in opts.graph_genome_path:
        graphPrefix = opts.graph_genome_path.replace('.vg', '').split("/")[-1]
        fullGraphPrefix = opts.graph_genome_path.replace('.vg', '')
        fullGraphPath = '/'.join(opts.graph_genome_path.split("/")[0:-1])
        gType = "vg"
    elif ".xg" in opts.graph_genome_path:
        graphPrefix = opts.graph_genome_path.replace('.xg', '').split("/")[-1]
        fullGraphPrefix = opts.graph_genome_path.replace('.xg', '')
        fullGraphPath = '/'.join(opts.graph_genome_path.split("/")[0:-1])
        gType = "xg"
    elif os.path.isdir(opts.graph_genome_path):  
        vglist = []
        graphPrefix = "all"
        fullGraphPrefix = opts.graph_genome_path + "/all"
        for root, subdirs, files in os.walk(opts.graph_genome_path): 
            if sum([1 for i in files if ".vg" in i]) > 0:
                vglist += [os.path.join(root, i) for i in files if ".vg" in i]
        for n, vgfile in enumerate(vglist):
            print(' {} - {}'.format(n+1, vgfile) )
        if len(vglist) == 0:
            sys.exit("No vg graphs found in specified folder.\nCheck the path and try again.")
        gType = "folder"
    elif opts.graph_genome_path == "" and opts.ref_genome_path is not None:
        print("Using a linear genome only, without variations.")
        graphPrefix = opts.ref_genome_path.replace('.fna', '').replace('.fasta', '').replace('.fa', '').split("/")[-1]
        fullGraphPrefix = opts.ref_genome_path.replace('.fasta', '').replace('.fa', '')
        fullGraphPath = '/'.join(opts.ref_genome_path.split("/")[0:-1])
        gType = "fasta"

            


    cmd = "export TMPDIR=`pwd`/LOCALGRAPH\n"
    cmd += 'if [ ! -e `pwd`/LOCALGRAPH ]; then mkdir `pwd`/LOCALGRAPH/; fi\n'
    if gType == "vcf":
        cmd += "if [ ! -e {0}.tbi ]; then tabix -p vcf {0}; fi\n".format(opts.graph_genome_path)
        cmd += "if [ ! -e {0}.vg ]; then\n\tvg construct -t ${4} -p -r {1} -v {2} > `pwd`/LOCALGRAPH/{3}.vg\nfi\n".format(fullGraphPrefix, opts.ref_genome_path, opts.graph_genome_path, graphPrefix, roadrunner.jobCores)
        cmd += "if [ ! -e {0}.xg ]; then\n\tvg index -t 1 -x `pwd`/LOCALGRAPH/{1}.xg `pwd`/LOCALGRAPH/{1}.vg\nfi\n".format(fullGraphPrefix, graphPrefix)
        cmd += "if [ ! -e {0}.gcsa  ]; then\n".format(fullGraphPrefix)
        cmd += "\tvg ids -m mapping `pwd`/LOCALGRAPH/{0}.vg > `pwd`/LOCALGRAPH/mapping && cp `pwd`/LOCALGRAPH/mapping `pwd`/LOCALGRAPH/mapping.bck\n".format(graphPrefix)
        cmd += "\tvg prune -t ${1} -u -a -m `pwd`/LOCALGRAPH/mapping `pwd`/LOCALGRAPH/{0}.vg > `pwd`/LOCALGRAPH/{0}.pruned.vg\n".format(graphPrefix, roadrunner.jobCores)
        cmd += "\tvg index -t ${1} -Z 4096 -f mapping -p -g `pwd`/LOCALGRAPH/{0}.gcsa `pwd`/LOCALGRAPH/{0}.pruned.vg && rm `pwd`/LOCALGRAPH/{0}.pruned.vg;\nfi\n".format(graphPrefix, roadrunner.jobCores)
    elif gType == "fasta":
        cmd += "if [ ! -e {0}.vg ]; then\n\tvg construct -t ${4} -p -r {1} > `pwd`/LOCALGRAPH/{3}.vg\nfi\n".format(fullGraphPrefix, opts.ref_genome_path, graphPrefix, roadrunner.jobCores)
        cmd += "if [ ! -e {0}.xg ]; then\n\tvg index -t 1 -x `pwd`/LOCALGRAPH/{1}.xg `pwd`/LOCALGRAPH/{1}.vg\nfi\n".format(fullGraphPrefix, graphPrefix)
        cmd += "if [ ! -e {0}.gcsa  ]; then\n".format(fullGraphPrefix)
        cmd += "\tvg ids -m mapping `pwd`/LOCALGRAPH/{0}.vg > `pwd`/LOCALGRAPH/mapping && cp `pwd`/LOCALGRAPH/mapping `pwd`/LOCALGRAPH/mapping.bck\n".format(graphPrefix)
        cmd += "\tvg prune -t ${1} -u -a -m `pwd`/LOCALGRAPH/mapping `pwd`/LOCALGRAPH/{0}.vg > `pwd`/LOCALGRAPH/{0}.pruned.vg\n".format(graphPrefix, roadrunner.jobCores)
        cmd += "\tvg index -t ${1} -Z 4096 -f mapping -p -g `pwd`/LOCALGRAPH/{0}.gcsa `pwd`/LOCALGRAPH/{0}.pruned.vg && rm `pwd`/LOCALGRAPH/{0}.pruned.vg;\nfi\n".format(graphPrefix, roadrunner.jobCores)
    elif gType == "vg":
        cmd += "if [ ! -e {0}.xg ]; then\n\tvg index -t 1 -x `pwd`/LOCALGRAPH/{1}.xg `pwd`/LOCALGRAPH/{1}.vg\nfi\n".format(fullGraphPrefix, graphPrefix)
        cmd += "if [ ! -e {0}.gcsa  ]; then\n".format(fullGraphPrefix)
        cmd += "\tvg ids -m mapping `pwd`/LOCALGRAPH/{0}.vg > `pwd`/LOCALGRAPH/mapping && cp `pwd`/LOCALGRAPH/mapping `pwd`/LOCALGRAPH/mapping.bck\n".format(graphPrefix)
        cmd += "\tvg prune -t ${} -u -a -m `pwd`/LOCALGRAPH/mapping `pwd`/LOCALGRAPH/{0}.vg > `pwd`/LOCALGRAPH/{0}.pruned.vg\n".format(graphPrefix, roadrunner.jobCores)
        cmd += "\tvg index -t ${} -Z 4096 -f mapping -p -g `pwd`/LOCALGRAPH/{0}.gcsa `pwd`/LOCALGRAPH/{0}.pruned.vg && rm `pwd`/LOCALGRAPH/{0}.pruned.vg;\nfi\n".format(graphPrefix, roadrunner.jobCores)
    elif gType == "folder":
        # Generate ids space for every vg file.
        cmd += "vg ids -j {0}\n".format(' '.join(vglist))
        cmd += "vg ids -m `pwd`/LOCALGRAPH/mapping {0}\nfi\n".format(' '.join(vglist))
        # Generate XG index for all the vg together.
        cmd += "if [ ! -e {0}.xg ]; then\n\tvg index -t 1 -x `pwd`/LOCALGRAPH/{1}.xg {2}\nfi\n".format(fullGraphPrefix, graphPrefix, ' '.join(vglist))
        # Check for gcsa and eventually generate it.
        prunedList = [i.replace(".vg", ".pruned.vg") for i in vglist]
        cmd += "if [ ! -e {0}.gcsa ]; then\n".format(fullGraphPrefix)
        cmd += '''\tfor i in {0}; do\n\t\tbname=`basename -s ".vg" $i`\n\t\tvg prune -t $NSLOTS -u -a -m `pwd`/LOCALGRAPH/mapping $i > `pwd`/LOCALGRAPH/$bname".pruned.vg"\n\t\techo "Done $i"\n\tdone'''.format(' '.join(vglist)) 
        cmd += "\tvg index -t ${0} -Z 4096 -f mapping -p -g `pwd`/LOCALGRAPH/{1}.gcsa {2}\nfi\n".format(roadrunner.jobCores, graphPrefix, ' '.join(prunedList)) 
    elif gType == "xg" and (not Isfile("{0}.gcsa".format(fullGraphPrefix)) or not Isfile("{0}.gcsa.lcp".format(fullGraphPrefix)) or not Isfile("{0}.vg".format(fullGraphPrefix))):
        print("Cannot proceed with pre-built XG index but without pre-built GCSA index.")
        print("Ensure to have the file {0}.vg, {0}.gcsa and {0}.gcsa.lcp are in the same path as {0}.xg and try again.".format(graphPrefix))
        sys.exit("Closed.")  
    elif gType == "xg" and Isfile("{0}.gcsa".format(fullGraphPrefix)) and Isfile("{0}.gcsa.lcp".format(fullGraphPrefix)):
        print("Graph provided:")
        print("vg:\t{}.vg".format(fullGraphPrefix))
        print("xg:\t{}.xg".format(fullGraphPrefix))
        print("gcsa:\t{}.gcsa".format(fullGraphPrefix))
        print("gcsa.lcp:\t{}.gcsa.lcp".format(fullGraphPrefix))
        print("")
    else:
        print("Graph is of no supperted type.")
        sys.exit("Closed.")

    
    cmd += "cp `pwd`/LOCALGRAPH/* {} && rm -r `pwd`/LOCALGRAPH/*\n".format(fullGraphPath)

    # Check whether the indexes need to be generated
    generateIndex = False
    if not Isfile("{0}.xg".format(fullGraphPrefix)) or \
                        not Isfile("{0}.gcsa".format(fullGraphPrefix)):
        generateIndex = True
    
    if generateIndex:
        # Save script.
        outf = open(prname, 'w')
        outf.write(header)  
        outf.write('%s\n' % cmd)

        # Perform actual alignment OR save command line to launch by hand.
        roadrunner.parse(script_name=prname, jobname=pname)
        roadrunner.saveCommand()
        outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))
    
    return "{0}".format(fullGraphPrefix)


# Module to join PE fastq files into single-library interleaved fastq file 
def joinPEfq(opts, samples, filename, outcommand, nstep, roadrunner):
    # Basic details
    dependencies = ["vg", "java", "samtools", "gatk"]
    prname = "./SCRIPTS/" + nstep + '-JoinPEreads.sh'
    procName = "jr"
    outf = open(prname, 'w')
    nalignments = 0
    pr_file = open("./LISTS/pairedReads.txt", "w")
    for line in open(filename):
        sample, reads = line.strip().split()
        reads = reads.split(",")
        if len(reads) > 1:
            nalignments += 1
            pr_file.write(line)
    
    # Start create header for SH file.
    h = Header(opts)
    pname = creates_pname(procName)
    h.processname(pname)
    h.setcores(opts.nthreadsAlignment)
    h.setmemory(4)
    h.runtime(95)
    h.stderrSet("./LOGS/${0}.${1}.err".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.out".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    if nalignments > opts.nsamples: h.setnjobs(nalignments)
    else: h.setnjobs(opts.nsamples)
    h.setconcurrency(opts.nprocesses)
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    opts.waitad += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' | tr ',' ' ' )\n'''
    header += '''read1=($reads)\n'''
    header += '''mypath=$(python -c 'import sys; print "/".join(sys.argv[1].split("/")[0:-1])' $read1)\n'''
    header += '''tName=$(basename $read1)\n'''
    header += '''rName=$(python -c 'import sys; print [sys.argv[1].replace(".fq.gz", ".fastq.gz").replace(".fastq.gz", ""), sys.argv[1].replace(".fq.gz", ".fastq.gz").replace("_1.fastq.gz", "").replace("_R1.fastq.gz", "").replace("_R2.fastq.gz", "").replace("_2.fastq.gz", "")]["_1.fastq.gz" in sys.argv[1].replace(".fq.gz", ".fastq.gz") or "_R1.fastq.gz" in sys.argv[1].replace(".fq.gz", ".fastq.gz")]' $tName)\n'''
    header += '''IFS=', ' read -r -a readsarray <<< "$reads"\n'''
    header += '''R1="${readsarray[0]}"\n'''
    header += '''R2="${readsarray[1]}"\n'''
    header += '''echo $R1\n'''
    header += '''echo $R2\n'''
    outf.write(header)

    # create bam file
    outf.write('gatk --java-options "-Xmx4G" FastqToSam -SM $sample -F1 $R1 -F2 $R2 -O $mypath/$rName".bam" --TMP_DIR $mypath {} && rm $R1 $R2 \n'.format(opts.GATKoptions.get("FastqToSam", "")))
    # Convert to interleaved fastq
    outf.write('samtools sort -@ ${} -n $mypath/$rName".bam" | samtools fastq - | gzip -c > $mypath/$rName".interleaved.fq.gz" && rm $mypath/$rName".bam" \n'.format(roadrunner.jobCores))

    # Create new list
    outlist = open('./LISTS/interleavedlist.txt', 'w')
    tmprows = []
    for line in open(filename):
        sample, reads = line.strip().split()
        reads = reads.split(",")
        if len(reads) > 1:
            newreadname = reads[0].replace(".fq.gz", ".fastq.gz").replace("_1.fastq.gz", "").replace("_R1.fastq.gz", "").replace("_2.fastq.gz", "").replace("_R2.fastq.gz", "") + ".interleaved.fq.gz"
            line = "{}\t{}\n".format(sample, newreadname)
        tmprows.append(line)
    for l in tmprows:
        outlist.write('%s' % (l))
    outlist.close()
    

    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse("./LISTS/pairedReads.txt", script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))

    print 'List of fixed reads: ./LISTS/interleavedlist.txt'
    return './LISTS/interleavedlist.txt'


# Make alignment script
def graphMap(opts, samples, graphName, filename, outcommand, nstep, roadrunner):
    # Basic details
    try:
        envName = opts.customEnv["CondaEnv"]
    except:
        from lib.GridOptions import GridEngine
        GE = GridEngine()
        envName = GE.localEnv["CondaEnv"]

    if opts.aligner == "graphaligner" and envName is not None:
        dependencies = ["vg", "java", "samtools", "CondaEnv"]
    else:
        opts.aligner = "vg"
        dependencies = ["vg", "java", "samtools"]
    prname = "./SCRIPTS/" + nstep + '-GraphMap.sh'
    procName = "gm"
    if not os.path.exists("ALIGN"): os.mkdir('ALIGN/')
    outf = open(prname, 'w')
    nalignments = 0
    for sample in samples:
        nalignments += len(samples[sample])
    
    # Start create header for SH file.
    h = Header(opts)
    pname = creates_pname(procName)
    h.processname(pname)
    h.setcores(opts.nthreadsAlignment)
    if opts.interleave:
        h.setmemory(64)
    else:
        h.setmemory(16)

    # If scattered dataset, use a lower run time
    if opts.scatterSize != 0:
        h.runtime(47)
    else:
        h.runtime(160)

    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    if nalignments > opts.nsamples: h.setnjobs(nalignments)
    else: h.setnjobs(opts.nsamples)
    if int(opts.nprocesses) < 25 and nalignments > 25:
        h.setconcurrency(25)
    else:
        h.setconcurrency(opts.nprocesses)
        

    # Define holding jobs
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    list2merge = [sample for sample in samples if len(samples[sample]) > 1]
    if len(list2merge) == 0:
        opts.waitad += pname + ","
    else:
        opts.wait += opts.waitad + pname + ","
        opts.waitad = ''

    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' | tr ',' ' ' )\n'''
    header += '''rName=`python -c "import sys; tmp = sys.argv[1].replace('.interleaved', '').replace('.fq.gz', '.fastq.gz').replace('.fastq.gz', '').split('/')[-1]; print( ['_'.join( tmp.split('_')[:-1]) if '_' in tmp else tmp][0] )" $reads`\n'''
    header += '''echo $rName\n'''

    # Align reads
    if opts.aligner == "vg":
        header += '''interleaved=`python -c "import sys;print ' '.join(['-if {}'.format(i) if 'interleaved.fq.gz' in i else '-f {}'.format(i) for i in sys.argv[1:] ])" $reads`\n'''
        header += '''echo $interleaved\n'''
        header += '''NCORES_MAP=$((${} - 1))\n'''.format(roadrunner.jobCores)
        outf.write(header)
        outf.write('vg map -R $sample -N $sample -S 0 -u 1 -t $NCORES_MAP -x {0}.xg -g {0}.gcsa $interleaved > ./ALIGN/$sample/$rName.tmp.gam\n'.format(graphName))
    else:
        header += '''interleaved=`python -c "import sys;print ' '.join(['-if {}'.format(i) if 'interleaved.fq.gz' in i else '-f {}'.format(i) for i in sys.argv[1:] ])" $reads`\n'''
        header += '''echo $interleaved\n'''
        header += '''NCORES_MAP=$((${} - 1))\n'''.format(roadrunner.jobCores)
        outf.write(header)
        outf.write("GraphAligner -g {0}.vg -t $NCORES_MAP -f $interleaved -a ./ALIGN/$sample/$rName.tmp.gam".format(graphName))        

    # Get alignment statistic
    outf.write('%s\n' % '\n# Get alignment statistics.')
    outf.write('vg stats -a ./ALIGN/$sample/$rName.tmp.gam > ./ALIGN/$sample/$rName.alignmentStats\n')
    
    outlist = open('./LISTS/alignedlist.txt', 'w')
    tmprows = []
    for line in open(filename):
        sample, reads = line.strip().split()
        if not os.path.exists(os.path.join('./ALIGN/', sample)): os.mkdir('./ALIGN/' + sample)
        reads = reads.split(",")
        rName = '_'.join(reads[0].split("/")[-1].replace(".fastq.gz", "").split("_")[:-1])
        if len(rName) == 0:
            rName = reads[0].split("/")[-1].replace(".fastq.gz", "")
        tmprows.append('%s\t%s\n' % (sample, 'ALIGN/'+sample+'/'+rName+'.tmp.gam'))
    newrows = []
    for row in tmprows:
        if row not in newrows: newrows.append(row)
    for l in newrows:
        outlist.write('%s' % (l))
    outlist.close()
    

    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))

    print 'List of aligned reads: ./LISTS/alignedlist.txt'
    return './LISTS/alignedlist.txt'



# Merge multiple reads per sample
def mergeGam(opts, samples, sampledict, filename, outcommand, nstep, nsamples, roadrunner):

    # check that reads need to be merged; if not close and return previous step file name.
    list2merge = [sample for sample in samples if len(sampledict[sample]) > 1]
    if len(list2merge) == 0:
        return filename
    else:
        nSample2Merge = len(list2merge)


    prname = "./SCRIPTS/" + nstep + '-mergeGam.sh'
    print 'Setting up pipeline to merge aligned reads...'
    if not os.path.exists('./ALIGN/'): os.mkdir('./ALIGN/')
    outf = open(prname, 'w')

    # Start create header for SH file.
    dependencies = ["vg"]
    h = Header(opts)
    pname = creates_pname("mg")
    h.processname(pname)
    h.setcores(opts.nthreadsAlignment)
    h.setmemory(16)
    h.runtime(96)
    h.holdjid(opts.wait + opts.waitad)
    h.holdjidad(None)
    h.stderrSet("./LOGS/${0}.${1}.err".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.out".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setnjobs(nSample2Merge)
    h.setconcurrency(nSample2Merge)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get()     
    header += '''export TMPDIR=`pwd`\n'''
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''if [ -e ALIGN/${sample}/${sample}.gam ]; then rm ALIGN/${sample}/${sample}.gam; fi\n'''
    header += '''if [ -e ./ALIGN/${sample}/tmplist.txt ]; then rm ./ALIGN/${sample}/tmplist.txt; fi\n'''
    header += '''if [ ! -e ./ALIGN/$sample ]; then mkdir ./ALIGN/$sample; fi\n'''
    header += '''for f in $(ls ./ALIGN/$sample/*.tmp.gam); do rname=$(realpath ${f}); echo ${rname}; done >> ./ALIGN/${sample}/tmplist.txt\n'''
    header += '''echo "Merging: "; while read p; do echo ${p}; done < ./ALIGN/${sample}/tmplist.txt\n'''
    header += '''while read p; do cat ${p} && rm ${p}; done < ./ALIGN/${sample}/tmplist.txt > ALIGN/${sample}/${sample}.gam\n'''
    header += 'vg stats -a ALIGN/${sample}/${sample}.gam > ALIGN/${sample}/${sample}.alignmentStats\n'
    header += 'vgv=`vg version -s`\n'
    header += '''if [ `python -c "import sys; print(sys.argv[1].split('-')[0].replace('v','') >= '1.20.0' )" $vgv` == "True" ]; then\n\tvg depth -a ALIGN/${sample}/${sample}.gam > ALIGN/${sample}/${sample}.depthStats\nfi\n'''
    
    # Save script.
    outf.write('%s\n' % header)
    outf.write('''if [ ! -e ./OldOutputLogs ]; then mkdir ./OldOutputLogs; fi\n''')

    # Save list of sample to merge.
    sample2merge = open('./LISTS/sample2merge.txt', 'w')
    [sample2merge.write('%s\n' % sample) for sample in samples if len(sampledict[sample]) > 1]
    sample2merge.close()
    sample2merge = "./LISTS/sample2merge.txt"

    
    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse("./LISTS/sample2merge.txt", script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))

    # Save merged 
    mergedSample = open('./LISTS/mergedSamples.txt', 'w')
    for sample in samples:
        if len(sampledict[sample]) > 1: 
            mergedSample.write('%s\t%s\n' % (sample, os.path.join('ALIGN',sample,sample + '.gam')))
            continue
    for line in open(filename):
        sample = line.strip().split()[0]
        if len(sampledict[sample])==1:
            mergedSample.write(line) 
    mergedSample.close()
    return './LISTS/mergedSamples.txt'



def prefilterGam(opts, samples, sampledict, graphName, filename, outcommand, nstep, nsamples, roadrunner, isatac=False):

    # Module to filter gam alignments
    prname = "./SCRIPTS/" + nstep + '-filterGam.sh'
    print 'Setting up pipeline to pre-filter aligned reads...'
    if not os.path.exists('./FILTER/'): os.mkdir('./FILTER/')
    outf = open(prname, 'w')

    # Start create header for SH file.
    dependencies = ["vg"]
    h = Header(opts)
    pname = creates_pname("fg")
    h.processname(pname)
    h.setcores(opts.nthreadsAlignment)
    h.setmemory(32)
    h.runtime(96)
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    h.stderrSet("./LOGS/${0}.${1}.err".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.out".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    opts.waitad += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setnjobs(len(samples))
    h.setconcurrency(len(samples))
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get()     

    # Define command to filter data
    header += '''export TMPDIR=`pwd`\n'''
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' | tr ',' ' ' )\n'''
    header += '''echo "Filtering ${sample}"\n'''
    header += '''if [ ! -e ./FILTER/$sample ]; then mkdir ./FILTER/$sample; fi\n'''

    if isatac:
        filterVal = '''-r 0.95 -s 2.0 -q 60 -fu'''
    else:
        filterVal = '''-r 0.90 -fu -m 1 -q 15 -D 999'''

    header += '''vg filter $reads {0} -x {1}.xg | vg gamsort -p -t ${2} -i FILTER/$sample/$sample.filtered.gam.gai - > FILTER/$sample/$sample.filtered.gam && rm $reads\n'''.format(filterVal, graphName, roadrunner.jobCores)
    header += '''vg depth -g FILTER/$sample/$sample.filtered.gam {}.xg > FILTER/$sample/$sample.filtered.depth\n'''.format(graphName)
    
    # Save script.
    outf.write('%s\n' % header)


    
    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))

    # Save merged 
    filteredSample = open('./LISTS/filtered.txt', 'w')
    for sample in samples:
        filteredSample.write('%s\t%s\n' % (sample, os.path.join('FILTER',sample,sample + '.filtered.gam')))
    filteredSample.close()
    return './LISTS/filtered.txt'



def gamIndelStats(opts, samples, sampledict, graphName, filename, outcommand, nstep, nsamples, roadrunner):

    # Module to filter gam alignments
    prname = "./SCRIPTS/" + nstep + '-gamToBam.sh'
    print 'Setting up pipeline to save aligned bam file and allelic balance'
    if not os.path.exists('./STATS/'): os.mkdir('./STATS/')
    outf = open(prname, 'w')

    # Start create header for SH file.
    dependencies = ["vg", "samtools", "gatk"]
    h = Header(opts)
    pname = creates_pname("gs")
    h.processname(pname)
    h.setcores(1)
    h.setmemory(16)
    h.runtime(95)
    h.holdjid(opts.wait)
    h.stderrSet("./LOGS/${0}.${1}.err".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.out".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    h.addmailaddr(opts.mailaddr)
    h.setnjobs(len(samples))
    h.setconcurrency(len(samples))
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get()     

    # Define command to filter data
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' | tr ',' ' ' )\n'''
    header += '''echo "Filtering ${sample}"\n'''
    header += '''if [ ! -e ./STATS/$sample ]; then mkdir ./STATS/$sample; fi\n'''
    header += ' '.join(['''vg view -j -a $reads''',
    ' | python {}/analyze_indels.py'.format(os.path.dirname(os.path.realpath(__file__)).replace("/lib", "/misc")),
    ' --positions STATS/$sample/$sample.pos.txt --lengths STATS/$sample/$sample.len.txt --mapqs STATS/$sample/$sample.mapqs.txt\n'
    ])
    # Save script.
    outf.write('%s\n' % header)


    
    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))

    # Save merged 
    filteredSample = open('./LISTS/filtered.txt', 'w')
    for sample in samples:
        filteredSample.write('%s\t%s\n' % (sample, os.path.join('FILTER',sample,sample + '.filtered.gam')))
    filteredSample.close()
    return './LISTS/filtered.txt'


def gamToBam(opts, samples, sampledict, graphName, filename, outcommand, nstep, nsamples, roadrunner):

    # Module to filter gam alignments
    prname = "./SCRIPTS/" + nstep + '-gamToBam.sh'
    print 'Setting up pipeline to save aligned bam file and allelic balance'
    if not os.path.exists('./BAM/'): os.mkdir('./BAM/')
    outf = open(prname, 'w')

    # Start create header for SH file.
    dependencies = ["vg", "samtools", "gatk"]
    h = Header(opts)
    pname = creates_pname("gb")
    h.processname(pname)
    h.setcores(opts.nthreadsAlignment)
    h.setmemory(16)
    h.runtime(96)
    h.holdjid(opts.wait)
    h.stderrSet("./LOGS/${0}.${1}.err".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.out".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    h.addmailaddr(opts.mailaddr)
    h.setnjobs(len(samples))
    h.setconcurrency(len(samples))
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get()     

    # Define command to filter data
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' | tr ',' ' ' )\n'''
    header += '''echo "Filtering ${sample}"\n'''
    header += '''if [ ! -e ./BAM/$sample ]; then mkdir ./BAM/$sample; fi\n'''
    header += '''vg surject -t ${0} -b -N $sample -R $sample -C 9 -i -x {1}.xg $reads > BAM/$sample/$sample.filtered.bam\n'''.format(roadrunner.jobCores, graphName)
    header += '''gatk CollectAllelicCounts -I BAM/$sample/$sample.filtered.bam -R {0} -O BAM/$sample/$sample.filtered.ac.tsv {1} \n'''.format(opts.ref_genome_path, opts.GATKoptions.get("CollectAllelicCounts", "") )
    # Save script.
    outf.write('%s\n' % header)


    
    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))

    # Save merged 
    filteredSample = open('./LISTS/filtered.txt', 'w')
    for sample in samples:
        filteredSample.write('%s\t%s\n' % (sample, os.path.join('FILTER',sample,sample + '.filtered.gam')))
    filteredSample.close()
    return './LISTS/filtered.txt'


def bamToGam(opts, samples, sampledict, graphName, filename, outcommand, nstep, nsamples, roadrunner):

    # Module to filter gam alignments
    prname = "./SCRIPTS/" + nstep + '-bamToGam.sh'
    print 'Setting up pipeline to save aligned bam file and allelic balance'
    if not os.path.exists('./GAM/'): os.mkdir('./GAM/')
    outf = open(prname, 'w')

    # Start create header for SH file.
    dependencies = ["vg", "samtools", "gatk"]
    h = Header(opts)
    pname = creates_pname("gb")
    h.processname(pname)
    h.setcores(opts.nthreadsAlignment)
    h.setmemory(16)
    h.runtime(96)
    h.holdjid(opts.wait)
    h.stderrSet("./LOGS/${0}.${1}.err".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.out".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    h.addmailaddr(opts.mailaddr)
    h.setnjobs(len(samples))
    h.setconcurrency(len(samples))
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get()     

    # Define command to filter data
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' | tr ',' ' ' )\n'''
    header += '''echo "Filtering ${sample}"\n'''
    header += '''if [ ! -e ./GAM/$sample ]; then mkdir ./GAM/$sample; fi\n'''
    header += '''vg inject -t ${0} -x {1}.xg $reads > GAM/$sample/$sample.gam\n'''.format(roadrunner.jobCores, graphName)
    header += '''vg stats -a GAM/$sample/$sample.gam > GAM/$sample/$sample.alignmentStats \n'''
    # Save script.
    outf.write('%s\n' % header)


    
    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))

    # Save merged 
    filteredSample = open('./LISTS/filtered.txt', 'w')
    for sample in samples:
        filteredSample.write('%s\t%s\n' % (sample, os.path.join('FILTER',sample,sample + '.filtered.gam')))
    filteredSample.close()
    return './LISTS/filtered.txt'


# Merge multiple reads per sample
def chunkGamChrom(opts, samples, sampledict, graphName, filename, outcommand, nstep, nsamples, roadrunner):

    # check that reads need to be merged; if not close and return previous step file name.

    prname = "./SCRIPTS/" + nstep + '-chunkGam.sh'
    print 'Setting up pipeline to chunk graph and reads...'
    if not os.path.exists('./CHUNK/'): os.mkdir('./CHUNK/')
    outf = open(prname, 'w')

    # Start create header for SH file.
    dependencies = ["vg"]
    h = Header(opts)
    pname = creates_pname("cg")
    h.processname(pname)
    h.setcores(2)
    h.setmemory(64)
    h.runtime(96)
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    h.stderrSet("./LOGS/${0}.${1}.err".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.out".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setnjobs(len(samples))
    h.setconcurrency(len(samples))
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get()     
    pathList = open("./LISTS/PATHS.txt", "w")
    for line in open(opts.ref_genome_path + ".fai"):
        pathList.write("{}\n".format(line.strip().split()[0]))
    pathList.close()
    header += '''export TMPDIR=`pwd`\n'''
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' | tr ',' ' ' )\n'''
    header += '''if [ ! -e CHUNK/${sample} ]; then mkdir CHUNK/${sample}; fi\n'''
    header += '''vg chunk -x {}.xg'''.format(graphName)
    header += ''' -P ./LISTS/PATHS.txt -C -b CHUNK/${sample}/${sample}_call_chunk '''
    header += '''-t ${} '''.format(roadrunner.jobCores)
    header += '''-E CHUNK/${sample}/${sample}.chunklist -O pg\n'''

    # Save script.
    outf.write('%s\n' % header)
    outf.write('''if [ ! -e ./OldOutputLogs ]; then mkdir ./OldOutputLogs; fi\n''')

    
    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))

    # Save merged 
    chunkedSample = open('./LISTS/chunkedsample.txt', 'w')
    for sample in samples:
        chunkedSample.write('%s\t%s\n' % (sample, os.path.join('CHUNK',sample,sample + '.chunklist')))
        continue
    chunkedSample.close()
    return './LISTS/chunkedsample.txt'


# Merge multiple reads per sample
def chunkGamSize(opts, samples, sampledict, graphName, filename, outcommand, nstep, nsamples, roadrunner):

    # check that reads need to be merged; if not close and return previous step file name.

    prname = "./SCRIPTS/" + nstep + '-chunkGam.sh'
    print 'Setting up pipeline to chunk graph and reads...'
    if not os.path.exists('./CHUNK/'): os.mkdir('./CHUNK/')
    outf = open(prname, 'w')

    # Start create header for SH file.
    dependencies = ["vg"]
    h = Header(opts)
    pname = creates_pname("cg")
    h.processname(pname)
    h.setcores(2)
    h.setmemory(64)
    h.runtime(96)
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    h.stderrSet("./LOGS/${0}.${1}.err".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.out".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setnjobs(len(samples))
    h.setconcurrency(len(samples))
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get()     
    pathList = open("./LISTS/PATHS.txt", "w")
    for line in open(opts.ref_genome_path + ".fai"):
        pathList.write("{}\n".format(line.strip().split()[0]))
    pathList.close()
    header += '''export TMPDIR=`pwd`\n'''
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' | tr ',' ' ' )\n'''
    header += '''if [ ! -e CHUNK/${sample} ]; then mkdir CHUNK/${sample}; fi\n'''
    header += '''vg chunk -x {}.xg '''.format(graphName)
    header += '''-a FILTER/${sample}/${sample}.filtered.gam -P ./LISTS/PATHS.txt -c 50 -g'''
    header += ''' -s {} -o {}'''.format(opts.chunkSize, opts.chunkOvlp)
    header += ''' -b CHUNK/${sample}/${sample}_call_chunk '''
    header += '''-t ${}'''.format(roadrunner.jobCores)
    header += ''' -E CHUNK/${sample}/${sample}.chunklist -O pg -f\n'''
    
    # Save script.
    outf.write('%s\n' % header)
    outf.write('''if [ ! -e ./OldOutputLogs ]; then mkdir ./OldOutputLogs; fi\n''')

    
    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))

    # Save merged 
    chunkedSample = open('./LISTS/chunkedsample.txt', 'w')
    for sample in samples:
        chunkedSample.write('%s\t%s\n' % (sample, os.path.join('CHUNK',sample,sample + '.chunklist')))
        continue
    chunkedSample.close()
    return './LISTS/chunkedsample.txt'

# Merge multiple reads per sample
def chunkDepths(opts, samples, sampledict, graphName, filename, outcommand, nstep, nsamples, roadrunner):

    # check that reads need to be merged; if not close and return previous step file name.

    prname = "./SCRIPTS/" + nstep + '-chunkDp.sh'
    print 'Setting up pipeline to compute chunk depths...'
    if not os.path.exists('./CHUNK/'): os.mkdir('./CHUNK/')
    outf = open(prname, 'w')

    # Start create header for SH file.
    dependencies = ["vg"]
    h = Header(opts)
    pname = creates_pname("cd")
    h.processname(pname)
    h.setcores(2)
    h.setmemory(32)
    h.runtime(23)
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    h.stderrSet("./LOGS/${0}.${1}.err".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.out".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setnjobs(len(samples))
    h.setconcurrency(len(samples))
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get()     
    pathList = open("./LISTS/PATHS.txt", "w")
    for line in open(opts.ref_genome_path + ".fai"):
        pathList.write("{}\n".format(line.strip().split()[0]))
    pathList.close()
    header += '''export TMPDIR=`pwd`\n'''
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''chkFile=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' )\n'''
    header += '''if [ ! -e CHUNK/${sample} ]; then mkdir CHUNK/${sample}; fi\n'''
    header += '''while read p; do\n'''
    header += '''\tchrs=`echo $p | awk '{print $1}'`\n'''
    header += '''\tsbgraph=`echo $p | awk '{print $4}' | python -c "import sys; print(sys.stdin.readline().replace('.gam', '.pg'))"`\n'''
    header += '''\tbname=`python -c "import sys;tmp='.'.join(sys.argv[1].split('.')[0:-1]); print(tmp.split('/')[-1])" $tsbgraph`\n'''
    header += '''\tvg depth -t ${}'''.format(roadrunner.jobCores)
    header += ''' -g ./FILTER/$sample/${sample}.filtered.gam $sbgraph | awk -v chrn=$chrs '{print chrn, $1}' \n'''
    header += '''done < $chkFile > CHUNK/${sample}/${sample}.chunkDepths\n'''
    # Save script.
    outf.write('%s\n' % header)
    outf.write('''if [ ! -e ./OldOutputLogs ]; then mkdir ./OldOutputLogs; fi\n''')

    
    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))

    return None


# Make variant calling script.
def graphAugmentChrom(opts, samples, filename, graphName, outcommand, nstep, roadrunner):

    # Define intervals first
    dependencies = ["vg", "java", "samtools"]
    prname = "./SCRIPTS/{}-GraphAugment.sh".format(nstep)
    procName = "ga"
    if not os.path.exists("ALIGN"): os.mkdir('ALIGN/')
    outf = open(prname, 'w')
    
    # Start create header for SH file.
    h = Header(opts)
    h.processname(None)
    h.setcores(1)
    h.setmemory(64)
    h.runtime(47)
    h.stderrSet("./LOGS/${0}.${1}.${2}.err".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.${2}.out".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    h.holdjid(opts.wait + opts.waitad)
    h.holdjidad(None)
    #opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''export TMPDIR=`pwd`\n'''
    header += '''sample=$1\n'''
    header += '''tgtpath=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $2 | awk '{print $1}' | tr ',' ' ' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $2 | awk '{print $4}' | tr ',' ' ' )\n'''
    header += '''bname=`python -c "import sys;tmp='.'.join(sys.argv[1].split('.')[0:-1]); print(tmp.split('/')[-1])" $reads`\n\n'''
    header += '''if [ ! -e ./CHUNK/$sample ]; then mkdir ./CHUNK/$sample; fi\n'''
    outf.write(header)

    

    # Align reads
    outf.write('if [ -e ./FILTER/$sample/$sample.filtered.depth ]; then\n')
    outf.write('''\tmval=`head -1 ./FILTER/$sample/$sample.filtered.depth | awk '{print $1}' | python -c "import sys; print( int(round( (float(sys.stdin.readline())/10)+1)) )"` \n''')
    outf.write('else\n')
    outf.write('''\tmval=`vg depth -t ${0} -g ./FILTER/$sample/$sample.filtered.gam {1}.xg '''.format(roadrunner.jobCores, graphName)  + '''| awk '{print $1}' | python -c "import sys; print( int(round( (float(sys.stdin.readline())/10)+1)) )"` \n''')
    outf.write('fi\n')
    outf.write('''echo "Value of minimum depth to augment $bname is $mval (-m parameter)"\n''')
    outf.write('if [ -e ./CHUNK/$sample/${bname}.pg ]; then\n')
    outf.write('\tvg augment ./CHUNK/$sample/${bname}.pg ./FILTER/$sample/$sample.filtered.gam -Q 5 -m ${mval} -s -p -C ')
    outf.write('-t ${} '.format(roadrunner.jobCores))
    outf.write('-A ./CHUNK/$sample/${bname}.aug.gam > ./CHUNK/$sample/${bname}.aug.vg\n')
    outf.write('elif [ -e ./CHUNK/$sample/${bname}.vg ]; then\n')
    # outf.write('\tvg augment ./CHUNK/$sample/${bname}.vg ./CHUNK/$sample/${sample}.filtered.gam -Q 5 -m ${mval} -s -p -C -t $NSLOTS -A ./CHUNK/$sample/${bname}.aug.gam > ./CHUNK/$sample/${bname}.aug.vg\n')    
    outf.write('\tvg augment ./CHUNK/$sample/${bname}.vg ./FILTER/$sample/$sample.filtered.gam -Q 5 -m ${mval} -s -p -C ')
    outf.write('-t ${} '.format(roadrunner.jobCores))
    outf.write('-A ./CHUNK/$sample/${bname}.aug.gam > ./CHUNK/$sample/${bname}.aug.vg\n')    
    outf.write('fi\n')
    # outf.write('vg augment ./CHUNK/$sample/${bname}.vg ./CHUNK/$sample/${bname}.gam -Q 5 -m ${mval} -s -p -C -t $NSLOTS -A ./CHUNK/$sample/${bname}.aug.gam > ./CHUNK/$sample/${bname}.aug.vg\n')    
    outf.write('vg index ./CHUNK/$sample/${bname}.aug.vg -x ./CHUNK/$sample/${bname}.aug.xg && rm ./CHUNK/$sample/${bname}.aug.vg\n')
    outf.write('vg pack -x ./CHUNK/$sample/${bname}.aug.xg -g ./CHUNK/$sample/${bname}.aug.gam -o ./CHUNK/$sample/${bname}.aug.pack && rm ./CHUNK/$sample/${bname}.aug.gam\n')
    outf.write('vg snarls -t ${} '.format(roadrunner.jobCores))
    outf.write('./CHUNK/$sample/${bname}.aug.xg > ./CHUNK/$sample/${bname}.aug.snarls\n')

    # Save single scripts.
    commandArray = {}
    for sample in samples:
        pname = creates_pname(procName)
        # Perform actual alignment OR save command line to launch by hand.
        roadrunner.parse(sample, "./CHUNK/{0}/{0}.chunklist".format(sample), script_name=prname, jobname=pname, options="-N {1} -tc 150 -t 1-`wc -l ./CHUNK/{0}/{0}.chunklist".format(sample, pname) + " | awk '{print $1}'`")
        roadrunner.saveCommand()
        commandArray[sample] = {"augment": [pname, roadrunner.commandlist[-1]]}
    print("Script to augment graph is ready.")

    return commandArray



# Make variant calling script.
def graphAugmentSize(opts, samples, filename, graphName, outcommand, nstep, roadrunner):

    # Define intervals first
    dependencies = ["vg", "java", "samtools"]
    prname = "./SCRIPTS/{}-GraphAugment.sh".format(nstep)
    procName = "ga"
    if not os.path.exists("ALIGN"): os.mkdir('ALIGN/')
    outf = open(prname, 'w')
    
    # Start create header for SH file.
    h = Header(opts)
    h.processname(None)
    h.setcores(1)
    h.setmemory(64)
    h.runtime(47)
    h.stderrSet("./LOGS/${0}.${1}.${2}.err".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.${2}.out".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    h.holdjid(opts.wait + opts.waitad)
    h.holdjidad(None)
    #opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''export TMPDIR=`pwd`\n'''
    header += '''sample=$1\n'''
    header += '''tgtpath=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $2 | awk '{print $1}' | tr ',' ' ' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $2 | awk '{print $4}' | tr ',' ' ' )\n'''
    header += '''bname=`python -c "import sys;tmp='.'.join(sys.argv[1].split('.')[0:-1]); print(tmp.split('/')[-1])" $reads`\n\n'''
    header += '''if [ ! -e ./CHUNK/$sample ]; then mkdir ./CHUNK/$sample; fi\n'''
    outf.write(header)

    

    # Align reads
    outf.write('if [ -e ./FILTER/$sample/$sample.filtered.depth ]; then\n')
    outf.write('''\tmval=`head -1 ./FILTER/$sample/$sample.filtered.depth | awk '{print $1}' | python -c "import sys; print( int(round( (float(sys.stdin.readline())/10)+1)) )"` \n''')
    outf.write('else\n')
    outf.write('''\tmval=`vg depth -t ${0} -g ./FILTER/$sample/$sample.filtered.gam {1}.xg '''.format(roadrunner.jobCores, graphName)  + '''| awk '{print $1}' | python -c "import sys; print( int(round( (float(sys.stdin.readline())/10)+1)) )"` \n''')
    outf.write('fi\n')
    outf.write('''echo "Value of minimum depth to augment $bname is $mval (-m parameter)"\n''')
    outf.write('if [ -e ./CHUNK/$sample/${bname}.pg ]; then\n')
    outf.write('\tvg augment ./CHUNK/$sample/${bname}.pg ./CHUNK/$sample/${bname}.gam -Q 5 -m ${mval} -s -p -C ')
    outf.write('-t ${} '.format(roadrunner.jobCores))
    outf.write('-A ./CHUNK/$sample/${bname}.aug.gam > ./CHUNK/$sample/${bname}.aug.vg\n')
    outf.write('elif [ -e ./CHUNK/$sample/${bname}.vg ]; then\n')
    # outf.write('\tvg augment ./CHUNK/$sample/${bname}.vg ./CHUNK/$sample/${sample}.filtered.gam -Q 5 -m ${mval} -s -p -C -t $NSLOTS -A ./CHUNK/$sample/${bname}.aug.gam > ./CHUNK/$sample/${bname}.aug.vg\n')    
    outf.write('\tvg augment ./CHUNK/$sample/${bname}.vg ./CHUNK/$sample/${bname}.gam -Q 5 -m ${mval} -s -p -C ')
    outf.write('-t ${} '.format(roadrunner.jobCores))
    outf.write('-A ./CHUNK/$sample/${bname}.aug.gam > ./CHUNK/$sample/${bname}.aug.vg\n')    
    outf.write('fi\n')
    # outf.write('vg augment ./CHUNK/$sample/${bname}.vg ./CHUNK/$sample/${bname}.gam -Q 5 -m ${mval} -s -p -C -t $NSLOTS -A ./CHUNK/$sample/${bname}.aug.gam > ./CHUNK/$sample/${bname}.aug.vg\n')    
    outf.write('vg index ./CHUNK/$sample/${bname}.aug.vg -x ./CHUNK/$sample/${bname}.aug.xg && rm ./CHUNK/$sample/${bname}.aug.vg\n')
    outf.write('vg pack -x ./CHUNK/$sample/${bname}.aug.xg -g ./CHUNK/$sample/${bname}.aug.gam -o ./CHUNK/$sample/${bname}.aug.pack && rm ./CHUNK/$sample/${bname}.aug.gam\n')
    outf.write('vg snarls -t ${} '.format(roadrunner.jobCores))
    outf.write('./CHUNK/$sample/${bname}.aug.xg > ./CHUNK/$sample/${bname}.aug.snarls\n')

    # Save single scripts.
    commandArray = {}
    for sample in samples:
        pname = creates_pname(procName)
        # Perform actual alignment OR save command line to launch by hand.
        roadrunner.parse(sample, "./CHUNK/{0}/{0}.chunklist".format(sample), script_name=prname, jobname=pname, options="-N {1} -tc 150 -t 1-`wc -l ./CHUNK/{0}/{0}.chunklist".format(sample, pname) + " | awk '{print $1}'`")
        roadrunner.saveCommand()
        commandArray[sample] = {"augment": [pname, roadrunner.commandlist[-1]]}
    print("Script to augment graph is ready.")

    return commandArray



# Make variant calling script.
def graphVariantChrom(commands, opts, samples, filename, outcommand, nstep, roadrunner):
    from lib.GridOptions import GridEngine
    import subprocess as sbp
    localEngine = GridEngine()
    modules = localEngine.modules

    dependencies = ["vg", "java", "samtools", "tabix", "vcftools"]
    prname = "./SCRIPTS/" + nstep + '-GraphVariant.sh'
    procName = "gv"
    if not os.path.exists("VCALL"): os.mkdir('VCALL/')
    outf = open(prname, 'w')
    
    # Start create header for SH file.
    h = Header(opts)
    h.processname(None)
    h.setcores(1)
    h.setmemory(32)
    h.runtime(23)
    h.stderrSet("./LOGS/${0}.${1}.${2}.err".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.${2}.out".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    # opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''export TMPDIR=`pwd`\n'''
    header += '''sample=$1\n'''
    header += '''chroms=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $2 | awk '{print $1}' | tr ',' ' ' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $2 | awk '{print $4}' | tr ',' ' ' )\n'''
    header += '''bname=`python -c "import sys;tmp='.'.join(sys.argv[1].split('.')[0:-1]); print(tmp.split('/')[-1])" $reads`\n\n'''
    header += '''if [ ! -e ./VCALL/$sample ]; then mkdir ./VCALL/$sample; fi\n\n'''
    outf.write(header)

    if not os.path.exists(opts.ref_genome_path + ".fai"):
        p = sbp.Popen("module load {0}\nsamtools faidx {1}".format(modules.get("samtools", "roslin/samtools"), opts.ref_genome_path), shell= True)
        p.wait()
    ofLength = open("./LISTS/lengths.txt", "w")
    for line in open(opts.ref_genome_path + ".fai"):
        chrom, length = line.strip().split()[0:2]
        ofLength.write("{}\t{}\n".format(chrom, length))
    ofLength.close()

    # Align reads
    outf.write('vg call ./CHUNK/$sample/${bname}.aug.xg -r ./CHUNK/$sample/${bname}.aug.snarls -s $sample -k ./CHUNK/$sample/${bname}.aug.pack ')
    outf.write('-t ${} '.format(roadrunner.jobCores))
    outf.write('-d {} '.format(opts.ploidy) )
    outf.write('-p $chroms | vcf-sort | bgzip -c > ./VCALL/$sample/${bname}.vcf.gz\n')
    outf.write('tabix -p vcf ./VCALL/$sample/${bname}.vcf.gz #&& rm ./CHUNK/$sample/${bname}.*\n')


    # Define intervals first
    for sample in samples:
        # Perform actual alignment OR save command line to launch by hand.
        pname = creates_pname(procName)
        lockerName = commands[sample]["augment"][0]
        roadrunner.parse(sample, "./CHUNK/{0}/{0}.chunklist".format(sample), script_name=prname, jobname=pname, options="-N {1} -hold_jid_ad {2} -tc 150 -t 1-`wc -l ./CHUNK/{0}/{0}.chunklist".format(sample, pname, lockerName) + " | awk '{print $1}'`")
        roadrunner.saveCommand()
        commands[sample].update({"call": [pname, roadrunner.commandlist[-1]]}) 
        # outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))
    print("Script to call variants from a graph is ready.")

    return commands



# Make variant calling script.
def graphVariantSize(commands, opts, samples, filename, outcommand, nstep, roadrunner):
    from lib.GridOptions import GridEngine
    import subprocess as sbp
    localEngine = GridEngine()
    modules = localEngine.modules

    dependencies = ["vg", "java", "samtools", "tabix", "vcftools"]
    prname = "./SCRIPTS/" + nstep + '-GraphVariant.sh'
    procName = "gv"
    if not os.path.exists("VCALL"): os.mkdir('VCALL/')
    outf = open(prname, 'w')
    
    # Start create header for SH file.
    h = Header(opts)
    h.processname(None)
    h.setcores(1)
    h.setmemory(32)
    h.runtime(23)
    h.stderrSet("./LOGS/${0}.${1}.${2}.err".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.${2}.out".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    # opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''export TMPDIR=`pwd`\n'''
    header += '''sample=$1\n'''
    header += '''chroms=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $2 | awk '{print $1}' | tr ',' ' ' )\n'''
    header += '''bpi=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $2 | awk '{print $2}' | tr ',' ' ' )\n'''
    header += '''bpe=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $2 | awk '{print $3}' | tr ',' ' ' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $2 | awk '{print $4}' | tr ',' ' ' )\n'''
    header += '''clength=$( awk -v var=$chroms '$1==var {print $2}' ./LISTS/lengths.txt )\n'''
    header += '''bname=`python -c "import sys;tmp='.'.join(sys.argv[1].split('.')[0:-1]); print(tmp.split('/')[-1])" $reads`\n\n'''
    header += '''if [ ! -e ./VCALL/$sample ]; then mkdir ./VCALL/$sample; fi\n\n'''
    outf.write(header)

    if not os.path.exists(opts.ref_genome_path + ".fai"):
        p = sbp.Popen("module load {0}\nsamtools faidx {1}".format(modules.get("samtools", "roslin/samtools"), opts.ref_genome_path), shell= True)
        p.wait()
    ofLength = open("./LISTS/lengths.txt", "w")
    for line in open(opts.ref_genome_path + ".fai"):
        chrom, length = line.strip().split()[0:2]
        ofLength.write("{}\t{}\n".format(chrom, length))
    ofLength.close()

    # Align reads
    outf.write('vg call ./CHUNK/$sample/${bname}.aug.xg -r ./CHUNK/$sample/${bname}.aug.snarls -s $sample -k ./CHUNK/$sample/${bname}.aug.pack ')
    outf.write('-t ${} '.format(roadrunner.jobCores))
    outf.write('-d {} '.format(opts.ploidy) )
    outf.write('-p $chroms -l $clength -o $bpi | vcf-sort | bgzip -c > ./VCALL/$sample/${bname}.vcf.gz\n')
    outf.write('tabix -p vcf ./VCALL/$sample/${bname}.vcf.gz #&& rm ./CHUNK/$sample/${bname}.*\n')


    # Define intervals first
    for sample in samples:
        # Perform actual alignment OR save command line to launch by hand.
        pname = creates_pname(procName)
        lockerName = commands[sample]["augment"][0]
        roadrunner.parse(sample, "./CHUNK/{0}/{0}.chunklist".format(sample), script_name=prname, jobname=pname, options="-N {1} -hold_jid_ad {2} -tc 150 -t 1-`wc -l ./CHUNK/{0}/{0}.chunklist".format(sample, pname, lockerName) + " | awk '{print $1}'`")
        roadrunner.saveCommand()
        commands[sample].update({"call": [pname, roadrunner.commandlist[-1]]}) 
        # outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))
    print("Script to call variants from a graph is ready.")

    return commands



# Make variant calling script.
def graphAugment_all(opts, samples, filename, graphName, outcommand, nstep, roadrunner):

    # Define intervals first
    dependencies = ["vg", "java", "samtools"]
    prname = "./SCRIPTS/{}-GraphAugment.sh".format(nstep)
    procName = "ga"
    if not os.path.exists("ALIGN"): os.mkdir('ALIGN/')
    outf = open(prname, 'w')
    
    # Start create header for SH file.
    h = Header(opts)
    pname = creates_pname(procName)
    h.processname(pname)
    h.setcores(1)
    h.setmemory(128)
    h.runtime(160)
    h.stderrSet("./LOGS/${0}.${1}.${2}.err".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.${2}.out".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    opts.wait = opts.wait + opts.waitad
    opts.waitad = ""
    h.holdjid(opts.wait)
    h.holdjidad(None)
    opts.waitad += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setnjobs(len(samples))
    h.setconcurrency(len(samples))
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''export TMPDIR=`pwd`\n'''
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''if [ ! -e ./FILTER/$sample ]; then mkdir ./FILTER/$sample; fi\n'''
    outf.write(header)

    

    # Align reads
    outf.write('if [ -e ./FILTER/$sample/$sample.filtered.depth ]; then\n')
    outf.write('''\tmval=`head -1 ./FILTER/$sample/$sample.filtered.depth | awk '{print $1}' | python -c "import sys; print( int(round( (float(sys.stdin.readline())/10)+1)) )"` \n''')
    outf.write('else\n')
    outf.write('''\tmval=`vg depth -t ${} '''.format(roadrunner.jobCores))
    outf.write('''-g ./FILTER/$sample/$sample.filtered.gam {}.xg '''.format(graphName)  + '''| awk '{print $1}' | python -c "import sys; print( int(round( (float(sys.stdin.readline())/10)+1)) )"` \n''')
    outf.write('fi\n')
    outf.write('''echo "Value of minimum depth to augment the graph is $mval (-m parameter)"\n''')
    outf.write('vg augment {}.vg '.format(graphName)  + 
                'FILTER/$sample/$sample.filtered.gam -Q 5 -m ${mval} -p -C ' +
                '-t ${} '.format(roadrunner.jobCores) + 
                '-A ./FILTER/$sample/${sample}.aug.gam > ./FILTER/$sample/${sample}.aug.vg\n')
    
    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))
    print("Script to augment graph is ready.")

    return 0


def GraphAugmentedIndex(opts, samples, filename, graphName, outcommand, nstep, roadrunner):
    # Define intervals first
    dependencies = ["vg", "java", "samtools"]
    prname = "./SCRIPTS/{}-GraphAugIdx.sh".format(nstep)
    procName = "gx"
    if not os.path.exists("ALIGN"): os.mkdir('ALIGN/')
    outf = open(prname, 'w')
    
    # Start create header for SH file.
    h = Header(opts)
    pname = creates_pname(procName)
    h.processname(pname)
    h.setcores(1)
    h.setmemory(128)
    h.runtime(96)
    h.stderrSet("./LOGS/${0}.${1}.${2}.err".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.${2}.out".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    opts.waitad += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setnjobs(len(samples))
    h.setconcurrency(len(samples))
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''export TMPDIR=`pwd`\n'''
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''if [ ! -e ./FILTER/$sample ]; then mkdir ./FILTER/$sample; fi\n'''
    outf.write(header)

    

    # Align reads
    outf.write('vg index ./FILTER/$sample/${sample}.aug.vg -x ./FILTER/$sample/${sample}.aug.xg\n')
    
    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))
    print("Script to augment graph indexing is ready.")

    return 0


def GraphPack(opts, samples, filename, graphName, outcommand, nstep, roadrunner):
    # Define intervals first
    dependencies = ["vg", "java", "samtools"]
    prname = "./SCRIPTS/{}-GraphPack.sh".format(nstep)
    procName = "gp"
    if not os.path.exists("ALIGN"): os.mkdir('ALIGN/')
    outf = open(prname, 'w')
    
    # Start create header for SH file.
    h = Header(opts)
    pname = creates_pname(procName)
    h.processname(pname)
    h.setcores(1)
    h.setmemory(128)
    h.runtime(47)
    h.stderrSet("./LOGS/${0}.${1}.${2}.err".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.${2}.out".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    opts.waitad += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setnjobs(len(samples))
    h.setconcurrency(len(samples))
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''export TMPDIR=`pwd`\n'''
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''if [ ! -e ./FILTER/$sample ]; then mkdir ./FILTER/$sample; fi\n'''
    outf.write(header)

    

    # Align reads
    outf.write('vg pack -x ./FILTER/$sample/${sample}.aug.xg -g ./FILTER/$sample/${sample}.aug.gam -o ./FILTER/$sample/${sample}.aug.pack && rm ./FILTER/$sample/${sample}.aug.gam\n')
    
    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))
    print("Script to augmented graph packing is ready.")

    return 0

def GraphAugSnarls(opts, samples, filename, graphName, outcommand, nstep, roadrunner):
    # Define intervals first
    dependencies = ["vg", "java", "samtools"]
    prname = "./SCRIPTS/{}-GraphAugSnarls.sh".format(nstep)
    procName = "gs"
    if not os.path.exists("ALIGN"): os.mkdir('ALIGN/')
    outf = open(prname, 'w')
    
    # Start create header for SH file.
    h = Header(opts)
    pname = creates_pname(procName)
    h.processname(pname)
    h.setcores(1)
    h.setmemory(128)
    h.runtime(47)
    h.stderrSet("./LOGS/${0}.${1}.${2}.err".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.${2}.out".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    opts.waitad += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setnjobs(len(samples))
    h.setconcurrency(len(samples))
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''export TMPDIR=`pwd`\n'''
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''if [ ! -e ./FILTER/$sample ]; then mkdir ./FILTER/$sample; fi\n'''
    outf.write(header)

    

    # Align reads
    outf.write('vg snarls -t ${} '.format(roadrunner.jobCores) + './FILTER/$sample/${sample}.aug.xg > ./FILTER/$sample/${sample}.aug.snarls\n')

    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))
    print("Script to compute augmented graph snarls is ready.")

    return 0



# Make variant calling script.
def graphVariant_all(opts, samples, filename, outcommand, nstep, roadrunner):
    from lib.GridOptions import GridEngine
    import subprocess as sbp
    localEngine = GridEngine()
    modules = localEngine.modules

    dependencies = ["vg", "java", "samtools", "tabix", "vcftools"]
    prname = "./SCRIPTS/" + nstep + '-GraphVariant.sh'
    procName = "gv"
    if not os.path.exists("VCALL"): os.mkdir('VCALL/')
    outf = open(prname, 'w')
    
    # Start create header for SH file.
    h = Header(opts)
    pname = creates_pname(procName)
    h.processname(pname)
    h.setcores(opts.nthreadsAlignment)
    h.setmemory(6)
    h.runtime(160)
    h.stderrSet("./LOGS/${0}.${1}.${2}.err".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.${2}.out".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''export TMPDIR=`pwd`\n'''
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''if [ ! -e ./VCALL/$sample ]; then mkdir ./VCALL/$sample; fi\n\n'''
    outf.write(header)

    if not os.path.exists(opts.ref_genome_path + ".fai"):
        p = sbp.Popen("module load {0}\nsamtools faidx {1}".format(modules.get("samtools", "roslin/samtools"), opts.ref_genome_path), shell= True)
        p.wait()
    
    ofLength = open("./LISTS/lengths.txt", "w")
    for line in open(opts.ref_genome_path + ".fai"):
        chrom, length = line.strip().split()[0:2]
        ofLength.write("{}\t{}\n".format(chrom, length))
    ofLength.close()

    # Align reads
    
    outf.write('if [ -e ./FILTER/$sample/${sample}.aug.snarls ]; then\n' )
    outf.write('\tvg call ./FILTER/$sample/${sample}.aug.xg -r ./FILTER/$sample/${sample}.aug.snarls -s $sample -k ./FILTER/$sample/${sample}.aug.pack ')
    outf.write('-t ${} '.format(roadrunner.jobCores))
    outf.write('-d {} '.format(opts.ploidy) )
    outf.write('| vcf-sort | bgzip -c > ./VCALL/$sample/${sample}.vcf.gz\n')
    outf.write('else\n')
    outf.write('\tvg call ./FILTER/$sample/${sample}.aug.xg -s $sample -k ./FILTER/$sample/${sample}.aug.pack ')
    outf.write('-t ${} '.format(roadrunner.jobCores))
    outf.write('-d {} '.format(opts.ploidy) )
    outf.write('| vcf-sort | bgzip -c > ./VCALL/$sample/${sample}.vcf.gz\nfi\n')
    outf.write('tabix -p vcf ./VCALL/$sample/${sample}.vcf.gz && rm ./FILTER/$sample/${sample}.aug.*\n')

    # Define intervals first
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))
    print("Script to call variants from a graph is ready.")

    return 0

def gatherVCFsChrom(commands, opts, samples, filename, outcommand, nstep, roadrunner):

    prname = "./SCRIPTS/" + nstep + '-gatherGraphVCF.sh'

    lockers = ','.join([commands[sample]["augment"][0] for sample in samples] + [commands[sample]["call"][0] for sample in samples])

    # Start create header for SH file.
    dependencies = ["java", "vcftools", "tabix", "bcftools", "R"]
    h = Header(opts)
    #h.processname("vjoin")
    pname = creates_pname("gg")
    h.processname(pname)
    h.setcores(opts.nthreads)
    h.setmemory(8)
    h.runtime(8)
    h.setnjobs(len(samples))
    h.setconcurrency(len(samples))
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    h.addmailaddr(opts.mailaddr)
    h.holdjid(lockers)
    h.holdjidad(None)
    opts.wait += pname + ","
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    print "Creating script to combine single-interval VCFs."
    if not os.path.exists("VCFS"): os.mkdir('VCFS/')
    header += '''sample=`head -n ${}'''.format(roadrunner.jobArrayN) + ''' $1 | tail -1 | awk '{print $1}'`\n'''
    header += '''chunks="CHUNK/${sample}/${sample}.chunklist"\n\n'''
    header += '''if [ ! -e ./VCFS/$sample ]; then mkdir ./VCFS/$sample; fi\n'''
    outf = open(prname, 'w')
    outf.write(header)

    # Align reads
    header += '''export TMPDIR=`pwd`\n'''
    outf.write("while read p; do\n")
    outf.write("\tfname=`echo $p | awk '{print $4}'`\n")
    outf.write('''\tbname=`python -c "import sys; print('.',join(sys.argv[1].split(".")[0:-1]))" ${fname}`\n''')
    outf.write('\techo ./VCALL/$sample/${bname}.vcf.gz\n')
    outf.write('done < VCALL/$sample/${sample}.chunklist > VCALL/$sample/${sample}.vcflist\n\n')

    outf.write('''vcf-concat -f VCALL/$sample/${sample}.vcflist | bgzip -c > ./VCFS/$sample/$sample.vcf.gz && tabix -p vcf ./VCFS/$sample/$sample.vcf.gz\n''')
    outf.write('''echo "Variants joined."\n''')
    outf.close()
    print 'Script for joint VCF creation is ready.'
    print 'Joint VCF file can be found in ./jVCF'

    oflist = open("./LISTS/vcalledlist.txt", "w")
    for sample in samples:
        oflist.write("{0}\t./VCFS/{0}/{0}.vcf.gz\n".format(sample))
    oflist.close()


    # Writing command to run.
    
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    cmd = 'qsub {} {}'.format(prname, filename)
    outcommand.write('%s\n' % (cmd))
    print("Script to gather chunked variants is ready.")

    return './jVCF/{}.sorted.vcf.gz'.format(opts.outfile)



def gatherVCFsSize(commands, opts, samples, filename, outcommand, nstep, roadrunner):

    prname = "./SCRIPTS/" + nstep + '-gatherGraphVCF.sh'

    lockers = ','.join([commands[sample]["augment"][0] for sample in samples] + [commands[sample]["call"][0] for sample in samples])

    # Start create header for SH file.
    dependencies = ["java", "vcftools", "tabix", "bcftools", "R"]
    h = Header(opts)
    #h.processname("vjoin")
    pname = creates_pname("gg")
    h.processname(pname)
    h.setcores(opts.nthreads)
    h.setmemory(8)
    h.runtime(8)
    h.setnjobs(len(samples))
    h.setconcurrency(len(samples))
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    h.addmailaddr(opts.mailaddr)
    h.holdjid(lockers)
    h.holdjidad(None)
    opts.wait += pname + ","
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    print "Creating script to combine single-interval VCFs."
    if not os.path.exists("VCFS"): os.mkdir('VCFS/')
    header += '''sample=`head -n ${}'''.format(roadrunner.jobArrayN) + ''' $1 | tail -1 | awk '{print $1}'`\n'''
    header += '''chunks="CHUNK/${sample}/${sample}.chunklist"\n\n'''
    header += '''if [ ! -e ./VCFS/$sample ]; then mkdir ./VCFS/$sample; fi\n'''
    outf = open(prname, 'w')
    outf.write(header)

    # Align reads
    header += '''export TMPDIR=`pwd`\n'''
    outf.write("Rscript {}/FixBedFile.R".format(os.path.dirname(os.path.realpath(__file__)).replace("/lib", "/misc")) + " CHUNK/$sample/${sample}.chunklist\n")
    outf.write("while read p; do\n")
    outf.write("\tchrname=`echo $p | awk '{print $1}'`\n")
    outf.write("\tbpi=`echo $p | awk '{print $2}'`\n")
    outf.write("\tbpe=`echo $p | awk '{print $3}'`\n")
    outf.write("\tfname=`echo $p | awk '{print $4}'`\n")
    outf.write('\tbname=`basename -s ".vcf.gz" ${fname}`\n')
    outf.write("\tbcftools view $fname -t ${chrname}:${bpi}-${bpe} | bgzip -c > ./VCALL/$sample/${bname}.clipped.vcf.gz && tabix -p vcf ./VCALL/$sample/${bname}.clipped.vcf.gz\n")
    outf.write('\techo ./VCALL/$sample/${bname}.clipped.vcf.gz\n')
    outf.write('done < VCALL/$sample/${sample}.bed > VCALL/$sample/${sample}.vcflist\n\n')

    outf.write('''vcf-concat -f VCALL/$sample/${sample}.vcflist | bgzip -c > ./VCFS/$sample/$sample.vcf.gz && tabix -p vcf ./VCFS/$sample/$sample.vcf.gz\n''')
    outf.write('''echo "Variants joined."\n''')
    outf.close()
    print 'Script for joint VCF creation is ready.'
    print 'Joint VCF file can be found in ./jVCF'

    oflist = open("./LISTS/vcalledlist.txt", "w")
    for sample in samples:
        oflist.write("{0}\t./VCFS/{0}/{0}.vcf.gz\n".format(sample))
    oflist.close()


    # Writing command to run.
    
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    cmd = 'qsub {} {}'.format(prname, filename)
    outcommand.write('%s\n' % (cmd))
    print("Script to gather chunked variants is ready.")

    return './jVCF/{}.sorted.vcf.gz'.format(opts.outfile)




# Make variant calling script.
def graphGenotype(commands, opts, samples, filename, outcommand, nstep, roadrunner):
    from lib.GridOptions import GridEngine
    import subprocess as sbp
    localEngine = GridEngine()
    modules = localEngine.modules

    dependencies = ["vg", "java", "samtools", "tabix", "vcftools"]
    prname = "./SCRIPTS/" + nstep + '-GraphGenotype.sh'
    procName = "gt"
    if not os.path.exists("VCALL"): os.mkdir('VCALL/')
    outf = open(prname, 'w')
    
    # Start create header for SH file.
    h = Header(opts)
    h.processname(None)
    h.setcores(1)
    h.setmemory(32)
    h.runtime(23)
    h.stderrSet("./LOGS/${0}.${1}.${2}.err".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.${2}.out".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    # opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''export TMPDIR=`pwd`\n'''
    header += '''sample=$1\n'''
    header += '''chroms=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $2 | awk '{print $1}' | tr ',' ' ' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $2 | awk '{print $4}' | tr ',' ' ' )\n'''
    header += '''bname=`python -c "import sys;tmp='.'.join(sys.argv[1].split('.')[0:-1]); print(tmp.split('/')[-1])" $reads`\n\n'''
    header += '''if [ ! -e ./VCALL/$sample ]; then mkdir ./VCALL/$sample; fi\n\n'''
    outf.write(header)

    if not os.path.exists(opts.ref_genome_path + ".fai"):
        p = sbp.Popen("module load {0}\nsamtools faidx {1}".format(modules.get("samtools", "roslin/samtools"), opts.ref_genome_path), shell= True)
        p.wait()
    ofLength = open("./LISTS/lengths.txt", "w")
    for line in open(opts.ref_genome_path + ".fai"):
        chrom, length = line.strip().split()[0:2]
        ofLength.write("{}\t{}\n".format(chrom, length))
    ofLength.close()

    # Align reads
    outf.write('vg index -p -d CHUNK/$sample/${bname}.gam.idx -N ./CHUNK/$sample/${bname}.gam && rm ./CHUNK/$sample/${bname}.gam')    
    outf.write('vg genotype -p -s $sample -v ./CHUNK/$sample/${bname}.pg CHUNK/$sample/${bname}.gam.idx')
    outf.write(' | vcf-sort | bgzip -c > ./VCALL/$sample/${bname}.vcf.gz\n')
    outf.write('tabix -p vcf ./VCALL/$sample/${bname}.vcf.gz #&& rm ./CHUNK/$sample/${bname}.*\n')


    # Define intervals first
    for sample in samples:
        # Perform actual alignment OR save command line to launch by hand.
        pname = creates_pname(procName)
        lockerName = commands[sample]["augment"][0]
        roadrunner.parse(sample, "./CHUNK/{0}/{0}.chunklist".format(sample), script_name=prname, jobname=pname, options="-N {1} -hold_jid_ad {2} -tc 150 -t 1-`wc -l ./CHUNK/{0}/{0}.chunklist".format(sample, pname, lockerName) + " | awk '{print $1}'`")
        roadrunner.saveCommand()
        commands[sample].update({"call": [pname, roadrunner.commandlist[-1]]}) 
        # outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))
    print("Script to call variants from a graph is ready.")

    return commands

