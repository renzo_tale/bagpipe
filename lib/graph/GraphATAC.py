########################################################
# This file contains all methods to generate and       #
# process graph genomes                                #
########################################################


import os
from os.path import isfile as Isfile
from lib.Utilities import Header
from lib.Utilities import creates_pname

# Merge multiple reads per sample
def chunkGamATAC(opts, samples, sampledict, graphName, filename, outcommand, nstep, nsamples, roadrunner):

    # check that reads need to be merged; if not close and return previous step file name.

    prname = "./SCRIPTS/" + nstep + '-chunkAtac.sh'
    print 'Setting up pipeline to chunk graph and reads...'
    if not os.path.exists('./CHUNK/'): os.mkdir('./CHUNK/')
    outf = open(prname, 'w')

    # Start create header for SH file.
    dependencies = ["vg"]
    h = Header(opts)
    pname = creates_pname("ca")
    h.processname(pname)
    h.setcores(2)
    h.setmemory(64)
    h.runtime(96)
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    h.stderrSet("./LOGS/${0}.${1}.err".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.out".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setnjobs(len(samples))
    h.setconcurrency(len(samples))
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get()     
    if not os.path.exists("./LISTS/PATHS.txt"):
        pathList = open("./LISTS/PATHS.txt", "w")
        for line in open(opts.ref_genome_path + ".fai"):
            pathList.write("{}\n".format(line.strip().split()[0]))
        pathList.close()
    header += '''export TMPDIR=`pwd`\n'''
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' | tr ',' ' ' )\n'''
    header += '''if [ ! -e CHUNK/${sample} ]; then mkdir CHUNK/${sample}; fi\n'''
    # header += '''vg chunk -x {}.xg '''.format(graphName)
    # header += '''-a FILTER/${sample}/${sample}.filtered.gam -g'''
    # header += ''' -P ./LISTS/PATHS.txt -C -b CHUNK/${sample}/${sample}_call_chunk '''
    # header += '''-t ${} '''.format(roadrunner.jobCores)
    # header += '''-E CHUNK/${sample}/${sample}.chunklist\n'''
    header += '''bname=`basename -s ".gam" $reads`\n'''
    header += '''chromosomes=`python -c "import sys;print(','.join([line.strip() for line in open(sys.argv[1])]))" $2`\n'''
    header += '''echo "$sample"\n'''
    header += '''echo "$reads"\n'''
    header += '''echo "$chromosomes"\n\n'''
    header += '''vg view -aj $reads > CHUNK/${sample}/${bname}.json\n'''
    header += '''echo "Done CHUNK/${sample}/${bname}.json"\n'''
    header += '''graph_peak_caller split_vg_json_reads_into_chromosomes $chromosomes ${bname}.json ''' + opts.GPC_graph + ''' && rm CHUNK/${sample}/${bname}.json\n'''
    header += '''echo "Done $sample""\n'''


    # Save script.
    outf.write('%s\n' % header)
    outf.write('''if [ ! -e ./OldOutputLogs ]; then mkdir ./OldOutputLogs; fi\n''')

    
    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))

    # Save merged 
    chunkedSample = open('./LISTS/chunkedsample.txt', 'w')
    for sample in samples:
        chunkedSample.write('%s\t%s\n' % (sample, os.path.join('CHUNK',sample,sample + '.chunklist')))
        continue
    chunkedSample.close()
    return './LISTS/chunkedsample.txt'


def graphVgJson(opts, samples, sampledict, graphName, filename, outcommand, nstep, nsamples, roadrunner):

    # check that reads need to be merged; if not close and return previous step file name.
    if not os.path.exists(graphName + ".vg"):
        print( "Warning! {}.vg not found.".format( graphName.split("/")[-1] ) )
        print( "Prior running the software check whether the vg file is in place." )


    prname = "./SCRIPTS/" + nstep + '-vg2json.sh'
    print 'Setting up pipeline to convert graph to json...'
    outf = open(prname, 'w')

    # Start create header for SH file.
    dependencies = ["vg", "GraphPeakCallerEnv"]
    h = Header(opts)
    pname = creates_pname("vj")
    h.processname(pname)
    h.setcores(1)
    h.setmemory(128)
    h.runtime(96)
    h.holdjid(opts.wait + opts.waitad)
    h.holdjidad(None)
    h.stderrSet("./LOGS/${0}.${1}.err".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.out".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    if opts.chunking == 1:
        h.setnjobs(len(samples))
        h.setconcurrency(len(samples))
    else:
        h.setnjobs(1)
        h.setconcurrency(1)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get()     
    header += '''export TMPDIR=`pwd`\n'''
    header += '''GPCGRAPH={}\n'''.format(opts.GPC_graph)
    if opts.chunking == 1:
        header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
        header += '''chkFile=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' )\n'''
        header += '''while read p; do\n'''
        header += '''\tchrfull=$( echo $p | awk '{print $1}' | tr ',' ' ' )\n'''
        header += '''\tchrname=$( echo $p | awk '{print $1}' | tr ',' ' ' | tr '.' '_' )\n'''
        header += '''\treads=$( echo $p | awk '{print $4}' | tr ',' ' ' )\n'''
        header += '''\tbname=`python -c "import sys;tmp='.'.join(sys.argv[1].split('.')[0:-1]); print(tmp.split('/')[-1])" $reads`\n\n'''
        header += '''\techo "vg view -Vj ./CHUNK/$sample/${bname}.vg > ./CHUNK/$sample/${chrname}.json"\n'''
        header += '''\tvg view -Vj ./CHUNK/$sample/${bname}.vg > ${GPCGRAPH}/${chrname}.json\n'''
        header += '''\tvg stats -r ./CHUNK/$sample/${bname}.vg | awk '{print $2}' > ${GPCGRAPH}/node_range_${chrname}.txt\n'''
        header += '''\tgraph_peak_caller create_ob_graph ./CHUNK/$sample/${chrname}.json -o ${GPCGRAPH}/$chrname && mv ${GPCGRAPH}/${chrname} ${GPCGRAPH}/${chrname}.nobg\n'''
        header += '''\tgraph_peak_caller find_linear_path -g ${GPCGRAPH}/${chrname}.nobg ${GPCGRAPH}/${chrname}.json $chrfull ${GPCGRAPH}/${chrname}_linear_pathv2.interval && rm ./CHUNK/$sample/${bname}.vg\n'''
        header += '''done < $chkFile\n'''
    else:
        header += '''\tvg view -Vj {}.vg > ${GPCGRAPH}/all.json\n'''.format(graphName)
        header += '''\tvg stats -r {}.vg | awk '{print $2}' > ./LOCALGRAPH/node_range_all.txt\n'''.format(graphName)
        header += '''\tgraph_peak_caller create_ob_graph ./LOCALGRAPH/all.json -o ${GPCGRAPH}/all && mv ${GPCGRAPH}/all ${GPCGRAPH}/all.nobg\n'''
        header += '''\tgraph_peak_caller find_linear_path -g ${GPCGRAPH}/all.nobg ${GPCGRAPH}/all.json ${GPCGRAPH}/all_linear_pathv2.interval\n'''


    # Save script.
    outf.write('%s\n' % header)
    outf.write('''if [ ! -e ./OldOutputLogs ]; then mkdir ./OldOutputLogs; fi\n''')

    
    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))
    print("Script to convert vg to database is ready.")

    return 0

def graphMapJson(opts, samples, sampledict, graphName, filename, outcommand, nstep, nsamples, roadrunner):

    prname = "./SCRIPTS/" + nstep + '-gam2json.sh'
    print 'Setting up pipeline to convert alignments to json...'
    outf = open(prname, 'w')

    # Start create header for SH file.
    dependencies = ["vg"]
    h = Header(opts)
    pname = creates_pname("gj")
    h.processname(pname)
    h.setcores(1)
    h.setmemory(16)
    h.runtime(96)
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    h.stderrSet("./LOGS/${0}.${1}.err".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.out".format(roadrunner.jobNameFlag, roadrunner.taskIdFlag))
    opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    if opts.chunking == 1:
        h.setnjobs(len(samples))
        h.setconcurrency(len(samples))
    else:
        h.setnjobs(1)
        h.setconcurrency(1)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get()      
    if not os.path.exists("./LISTS/PATHS.txt"):
        pathList = open("./LISTS/PATHS.txt", "w")
        for line in open(opts.ref_genome_path + ".fai"):
            pathList.write("{}\n".format(line.strip().split()[0]))
        pathList.close()
    header += '''export TMPDIR=`pwd`\n'''
    header += '''sample=$( head -${}'''.format(roadrunner.jobArrayN) + ''' $1 | tail -1 | awk '{print $1}' )\n'''
    # if opts.chunking == 1:
    #     header += '''chkFile=$( head -${}'''.format(roadrunner.jobArrayN) + ''' $1 | tail -1 | awk '{print $2}' )\n'''
    #     header += '''while read p; do\n'''
    #     header += '''\tchrfull=$( head -${}'''.format(roadrunner.jobArrayN) + ''' $2 | tail -1 | awk '{print $1}' | tr ',' ' ' )\n'''
    #     header += '''\tchrname=$( echo $p | awk '{print $1}' | tr ',' ' ' | tr '.' '_' )\n'''
    #     header += '''\treads=$( echo $p | awk '{print $4}' | tr ',' ' ' )\n'''
    #     header += '''\tbname=`python -c "import sys;tmp='.'.join(sys.argv[1].split('.')[0:-1]); print(tmp.split('/')[-1])" $reads`\n\n'''
    #     header += '''\tvg view -aj ./CHUNK/$sample/${bname}.gam > CHUNK/${sample}/${chrname}_filtered.json && rm ./CHUNK/$sample/${bname}.gam\n'''
    #     header += '''done < $chkFile\n'''
    # else:
    #     header += '''\tvg view -aj ./FILTER/$sample/${sample}.filtered.gam > FILTER/${sample}/${sample}_filtered.json\n'''
    header += '''\tvg view -aj ./FILTER/$sample/${sample}.filtered.gam > FILTER/${sample}/${sample}_filtered.json\n'''   

    # Save script.
    outf.write('%s\n' % header)
    outf.write('''if [ ! -e ./OldOutputLogs ]; then mkdir ./OldOutputLogs; fi\n''')

    
    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))

    print("Script to convert gam to json is ready.")
    return './LISTS/samplejson.txt'


def graphPeaks(opts, samples, sampledict, graphName, filename, outcommand, nstep, nsamples, roadrunner):

    prname = "./SCRIPTS/" + nstep + '-graphPeaksCaller.sh'
    print 'Setting up pipeline to call peaks from alignments...'
    outf = open(prname, 'w')

    # Start create header for SH file.
    dependencies = ["vg", "GraphPeakCallerEnv"]
    h = Header(opts)
    procName = "jp"
    # h.processname(pname)
    h.setcores(1)
    h.setmemory(64)
    h.runtime(96)
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    h.stderrSet("./LOGS/${0}.${1}.${2}.err".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/${0}.${1}.${2}.out".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag, roadrunner.taskIdFlag))
    # opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    # h.setnjobs(len(samples))
    # h.setconcurrency(len(samples))
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get()     
    pathList = open("./LISTS/PATHS.txt", "w")
    for line in open(opts.ref_genome_path + ".fai"):
        pathList.write("{}\n".format(line.strip().split()[0]))
    pathList.close()
    header += '''export TMPDIR=`pwd`\n'''
    header += '''sample=$1\n'''
    header += '''GPCGRAPH={}\n'''.format(opts.GPC_graph)
    if opts.chunking == 1:
        header += '''chrfull=$( head -${}'''.format(roadrunner.jobArrayN) + ''' $2 | tail -1 | awk '{print $1}' | tr ',' ' ' )\n'''
        header += '''chrname=$( head -${}'''.format(roadrunner.jobArrayN) + ''' $2 | tail -1 | awk '{print $1}' | tr ',' ' ' | tr '.' '_' )\n'''
        header += '''reads=$( head -${}'''.format(roadrunner.jobArrayN) + ''' $2 | tail -1 | awk '{print $4}' | tr ',' ' ' )\n'''
        header += '''bname=`python -c "import sys;tmp='.'.join(sys.argv[1].split('.')[0:-1]); print(tmp.split('/')[-1])" $reads`\n\n'''
        header += '''echo "Sorting"\n'''
        # Check if atac-seq or chip-seq
        if not opts.atac:
            header += '''fsize=`graph_peak_caller estimate_shift {} ./CHUNK/$sample CHUNK/${sample}/${sample}.sorted 5 100`\n'''  # Estimate insert size
            header += '''atac="-a False"\n'''
        else:
            header += '''fsize=150\n'''  # Estimate insert size
            header += '''atac="-a True"\n'''
        # Actual processing
        header += '''unique_reads=$(pcregrep -o1 '"sequence": "([ACGTNacgtn]{20,})"' CHUNK/${sample}/${chrname}_filtered.json | sort | uniq | wc -l)\n'''
        header += '''genomesize=`awk 'BEGIN{gsize=0}; {gsize+=$2}; END{print gsize}' ''' + '{}.fai ` \n'.format(opts.ref_genome_path)
        header += '''graph_peak_caller callpeaks -g ./CHUNK/${sample}/${chrname}.nobg -f $fsize -s CHUNK/${sample}/${chrname}_filtered.json -f $fsize -p True -u $unique_reads -G $genomesize $atac\n'''
        
        # Save script.
        outf.write('%s\n' % header)
        outf.write('''if [ ! -e ./OldOutputLogs ]; then mkdir ./OldOutputLogs; fi\n''')

        # Single sample
        commandArray = {}
        for sample in samples:
            pname = creates_pname(procName)
            # Perform actual alignment OR save command line to launch by hand.
            roadrunner.parse(sample, "./CHUNK/{0}/{0}.chunklist".format(sample), script_name=prname, jobname=pname, options="-N {1} -tc 150 -t 1-`wc -l ./CHUNK/{0}/{0}.chunklist".format(sample, pname) + " | awk '{print $1}'`")
            roadrunner.saveCommand()
            commandArray[sample] = {"peakcaller": [pname, roadrunner.commandlist[-1]]}    
    else:
        # Check if atac-seq or chip-seq
        if not opts.atac:
            header += '''fsize=`graph_peak_caller estimate_shift {} ./CHUNK/$sample CHUNK/${sample}/${sample}.sorted 5 100`\n'''  # Estimate insert size
            header += '''atac="-a False"\n'''
        else:
            header += '''fsize=150\n'''  # Estimate insert size
            header += '''atac="-a True"\n'''
        # Actual processing
        header += '''unique_reads=$(pcregrep -o1 '"sequence": "([ACGTNacgtn]{20,})"' FILTER/${sample}/${sample}_filtered.json | sort | uniq | wc -l)\n'''
        header += '''genomesize=`awk 'BEGIN{gsize=0}; {gsize+=$2}; END{print gsize}' ''' + '{}.fai ` \n'.format(opts.ref_genome_path)
        header += '''graph_peak_caller callpeaks -g ./LOCALGRAPH/all.nobg -f $fsize -s FILTER/${sample}/${sample}_filtered.json -f $fsize -p True -u $unique_reads -G $genomesize $atac\n'''
        
        # Save script.
        outf.write('%s\n' % header)
        outf.write('''if [ ! -e ./OldOutputLogs ]; then mkdir ./OldOutputLogs; fi\n''')

        # Single sample
        commandArray = {}
        for sample in samples:
            pname = creates_pname(procName)
            # Perform actual alignment OR save command line to launch by hand.
            roadrunner.parse(sample, "./CHUNK/{0}/{0}.chunklist".format(sample), script_name=prname, jobname=pname, options="-N {1} -tc 150 -t 1-`wc -l ./CHUNK/{0}/{0}.chunklist".format(sample, pname) + " | awk '{print $1}'`")
            roadrunner.saveCommand()
            commandArray[sample] = {"peakcaller": [pname, roadrunner.commandlist[-1]]}  

    print("Script to call graph peaks is ready.")
    return commandArray
