########################################################
# This file contains all methods from alignment to     #
# Base Quality Re-Calibration                          #
########################################################


import os
from lib.Utilities import Header
from lib.Utilities import creates_pname
from lib.Utilities import areFile

# Make alignment script
def alignment(opts, samples, filename, outcommand, nstep, roadrunner):
    if opts.aligner == "bwa":
        prname = "./SCRIPTS/" + nstep + '-BWA_pipeline.sh'
        command = 'bwa mem -a -M'
        procName = "al"
        dependencies = ["bwa", "java", "samtools", "gatk"]
        print 'Setting up pipeline for BWA...'
    elif opts.aligner == "minimap2":
        prname = "./SCRIPTS/" + nstep + '-Minimap2_pipeline.sh'
        command = 'minimap2 -ax sr'
        procName = "m2"
        dependencies = ["minimap2", "java", "samtools", "gatk"]
        print 'Setting up pipeline for Minimap2...'


    if not os.path.exists("ALIGN"): os.mkdir('ALIGN/')
    outf = open(prname, 'w')
    nalignments = 0
    for sample in samples:
        nalignments += len(samples[sample])
    
    # Start create header for SH file.
    h = Header(opts)
    pname = creates_pname(procName)
    h.processname(pname)
    h.setcores(opts.nthreadsAlignment)
    h.setmemory(8)
    if opts.scatterSize != 0:
        h.runtime(47)
    else:
        h.runtime(160)
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    if nalignments > opts.nsamples: h.setnjobs(nalignments)
    else: h.setnjobs(opts.nsamples)
    h.setconcurrency(opts.nprocesses)
    h.holdjid(opts.wait)
    opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' | tr ',' ' ' )\n'''
    header += '''read1=($reads)\n'''
    header += '''rName=$(basename $read1)\n'''
    header += '''rName=$(python -c 'import sys; print [sys.argv[1].replace(".fq.gz", ".fastq.gz").replace(".fastq.gz", ""), sys.argv[1].replace(".fq.gz", ".fastq.gz").replace("_1.fastq.gz", "").replace("_R1.fastq.gz", "").replace("_R2.fastq.gz", "").replace("_2.fastq.gz", "")]["_1.fastq.gz" in sys.argv[1].replace(".fq.gz", ".fastq.gz") or "_R1.fastq.gz" in sys.argv[1].replace(".fq.gz", ".fastq.gz")]' $rName)\n'''

    outf.write(header)

    

    # Align reads
    outf.write('%s\n' % ' '.join([command, '-t ${}'.format(roadrunner.jobCores), opts.ref_genome_path, 
                                        '$reads',
                                        '| samtools view - -b -S -h -o ./ALIGN/$sample/$rName.tmp.bam']))
    
    # Validate bam file.
    outf.write('%s\n' % ' '.join([opts.path_to_GATK, 'ValidateSamFile', 
                                    '-I ./ALIGN/$sample/$rName.tmp.bam',
                                    '--TMP_DIR ./ALIGN/$sample',
                                    '--MODE SUMMARY',
                                    '-O', './ALIGN/$sample/$rName.validate']))

    # Get alignment statistic
    outf.write('%s\n' % '\n# Get alignment statistics.')
    outf.write('%s\n' % ' '.join([opts.path_to_GATK, 'FlagStat', 
                                    '-I ./ALIGN/$sample/$rName.tmp.bam']))
    outf.write('%s\n' % '\n# Get alignment statistics.\nsamtools flagstat ./ALIGN/$sample/$rName.tmp.bam > ./ALIGN/$sample/$rName.alignmentStats && rm $reads')
    outf.write('''samtools view ./ALIGN/$sample/$rName.tmp.bam | awk 'length($10)"M" == $6 {print}' | wc -l | awk '{print "Perfectly aligned: ", $1}' >>  ./ALIGN/$sample/$rName.alignmentStats\n''')

    outlist = open('./LISTS/alignedlist.txt', 'w')
    tmprows = []
    for line in open(filename):
        sample, reads = line.strip().split()
        if not os.path.exists(os.path.join('./ALIGN/', sample)): os.mkdir('./ALIGN/' + sample)
        reads = reads.split(",")
        rName = '_'.join(reads[0].split("/")[-1].replace(".fastq.gz", "").split("_")[:-1])
        if len(rName) == 0:
            rName = reads[0].split("/")[-1].replace(".fastq.gz", "")
        tmprows.append('%s\t%s\n' % (sample, 'ALIGN/'+sample+'/'+rName+'.tmp.bam'))
    newrows = []
    for row in tmprows:
        if row not in newrows: newrows.append(row)
    for l in newrows:
        outlist.write('%s' % (l))
    outlist.close()
    

    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('%s\n' % ('qsub %s '%prname + filename))

    print 'List of aligned reads: ./LISTS/alignedlist.txt'
    return './LISTS/alignedlist.txt'


# Make alignment script
def BWAlignment(opts, samples, filename, outcommand, nstep, roadrunner):
    prname = "./SCRIPTS/" + nstep + '-BWA_pipeline.sh'
    print 'Setting up pipeline for BWA...'
    if not os.path.exists("ALIGN"): os.mkdir('ALIGN/')
    outf = open(prname, 'w')
    nalignments = 0
    for sample in samples:
        nalignments += len(samples[sample])
    
    # Start create header for SH file.
    dependencies = ["bwa", "java", "samtools", "gatk"]
    h = Header(opts)
    pname = creates_pname("al")
    h.processname(pname)
    h.setcores(opts.nthreadsAlignment)
    h.setmemory(6)
    h.runtime(160)
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    if nalignments > opts.nsamples: h.setnjobs(nalignments)
    else: h.setnjobs(opts.nsamples)
    h.setconcurrency(opts.nprocesses)
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    opts.waitad += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' | tr ',' ' ' )\n'''
    header += '''read1=($reads)\n'''
    header += '''rName=$(basename $read1)\n'''
    header += '''rName=$(python -c 'import sys; print [sys.argv[1].replace(".fq.gz", ".fastq.gz").replace(".fastq.gz", ""), sys.argv[1].replace(".fq.gz", ".fastq.gz").replace("_1.fastq.gz", "").replace("_R1.fastq.gz", "").replace("_R2.fastq.gz", "").replace("_2.fastq.gz", "")]["_1.fastq.gz" in sys.argv[1].replace(".fq.gz", ".fastq.gz") or "_R1.fastq.gz" in sys.argv[1].replace(".fq.gz", ".fastq.gz")]' $rName)\n'''

    outf.write(header)

    bwacommand = 'bwa mem -a -M'

    # Align reads
    outf.write('%s\n' % ' '.join([bwacommand, '-t ${}'.format(roadrunner.jobCores), opts.ref_genome_path, 
                                        '$reads',
                                        '| samtools view - -b -S -h -o ./ALIGN/$sample/$rName.tmp.bam']))
    
    # Validate bam file.
    outf.write('%s\n' % ' '.join(['gatk --java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0}"'.format(roadrunner.jobCores),
                                    'ValidateSamFile', 
                                    '-I ./ALIGN/$sample/$rName.tmp.bam',
                                    '--TMP_DIR ./ALIGN/$sample',
                                    '--MODE SUMMARY',
                                    '-O', './ALIGN/$sample/$rName.validate']))

    # Get alignment statistic
    outf.write('%s\n' % '\n# Get alignment statistics.')
    outf.write('%s\n' % ' '.join(['gatk FlagStat', 
                                    '-I ./ALIGN/$sample/$rName.tmp.bam']))
    outf.write('%s\n' % '\n# Get alignment statistics.\nsamtools flagstat ./ALIGN/$sample/$rName.tmp.bam > ./ALIGN/$sample/$rName.alignmentStats && rm $reads')

    outlist = open('./LISTS/alignedlist.txt', 'w')
    tmprows = []
    for line in open(filename):
        sample, reads = line.strip().split()
        if not os.path.exists(os.path.join('./ALIGN/', sample)): os.mkdir('./ALIGN/' + sample)
        reads = reads.split(",")
        rName = '_'.join(reads[0].split("/")[-1].replace(".fastq.gz", "").split("_")[:-1])
        if len(rName) == 0:
            rName = reads[0].split("/")[-1].replace(".fastq.gz", "")
        tmprows.append('%s\t%s\n' % (sample, 'ALIGN/'+sample+'/'+rName+'.tmp.bam'))
    newrows = []
    for row in tmprows:
        if row not in newrows: newrows.append(row)
    for l in newrows:
        outlist.write('%s' % (l))
    outlist.close()
    

    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('%s\n' % ('qsub %s '%prname + filename))

    print 'List of aligned reads: ./LISTS/alignedlist.txt'
    return './LISTS/alignedlist.txt'


# Make alignment script
def Mmap2Alignment(opts, samples, filename, outcommand, nstep, roadrunner):
    prname = "./SCRIPTS/" + nstep + '-Minimap2_pipeline.sh'
    print 'Setting up pipeline for Minimap2...'
    if not os.path.exists("ALIGN"): os.mkdir('ALIGN/')
    outf = open(prname, 'w')
    nalignments = 0
    for sample in samples:
        nalignments += len(samples[sample])
    
    # Start create header for SH file.
    dependencies = ["minimap2", "java", "samtools", "gatk"]
    h = Header(opts)
    pname = creates_pname("m2")
    h.processname(pname)
    h.setcores(opts.nthreadsAlignment)
    h.setmemory(6)
    h.runtime(160)
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    if nalignments > opts.nsamples: h.setnjobs(nalignments)
    else: h.setnjobs(opts.nsamples)
    h.setconcurrency(opts.nprocesses)
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    opts.waitad += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' | tr ',' ' ' )\n'''
    header += '''read1=($reads)\n'''
    header += '''rName=$(basename $read1)\n'''
    header += '''rName=$(python -c 'import sys; print [sys.argv[1].replace(".fq.gz", ".fastq.gz").replace(".fastq.gz", ""), sys.argv[1].replace(".fq.gz", ".fastq.gz").replace("_1.fastq.gz", "").replace("_R1.fastq.gz", "").replace("_R2.fastq.gz", "").replace("_2.fastq.gz", "")]["_1.fastq.gz" in sys.argv[1].replace(".fq.gz", ".fastq.gz") or "_R1.fastq.gz" in sys.argv[1].replace(".fq.gz", ".fastq.gz")]' $rName)\n'''

    outf.write(header)

    minimap2command = 'minimap2 -ax sr'

    # Align reads
    outf.write('%s\n' % ' '.join([minimap2command, '-t ${}'.format(roadrunner.jobCores), opts.ref_genome_path, 
                                        '$reads',
                                        '| samtools view - -b -S -h -o ./ALIGN/$sample/$rName.tmp.bam']))
    
    # Validate bam file.
    outf.write('%s\n' % ' '.join(['gatk --java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0}"'.format(roadrunner.jobCores),
                                    'ValidateSamFile', 
                                    '-I ./ALIGN/$sample/$rName.tmp.bam',
                                    '--TMP_DIR ./ALIGN/$sample',
                                    '--MODE SUMMARY',
                                    '-O', './ALIGN/$sample/$rName.validate']))

    # Get alignment statistic
    outf.write('%s\n' % '\n# Get alignment statistics.')
    outf.write('%s\n' % ' '.join(['gatk FlagStat', 
                                    '-I ./ALIGN/$sample/$rName.tmp.bam']))
    outf.write('%s\n' % '\n# Get alignment statistics.\nsamtools flagstat ./ALIGN/$sample/$rName.tmp.bam > ./ALIGN/$sample/$rName.alignmentStats && rm $reads')

    outlist = open('./LISTS/alignedlist.txt', 'w')
    tmprows = []
    for line in open(filename):
        sample, reads = line.strip().split()
        if not os.path.exists(os.path.join('./ALIGN/', sample)): os.mkdir('./ALIGN/' + sample)
        reads = reads.split(",")
        rName = '_'.join(reads[0].split("/")[-1].replace(".fastq.gz", "").split("_")[:-1])
        if len(rName) == 0:
            rName = reads[0].split("/")[-1].replace(".fastq.gz", "")
        tmprows.append('%s\t%s\n' % (sample, 'ALIGN/'+sample+'/'+rName+'.tmp.bam'))
    newrows = []
    for row in tmprows:
        if row not in newrows: newrows.append(row)
    for l in newrows:
        outlist.write('%s' % (l))
    outlist.close()
    

    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('%s\n' % ('qsub %s '%prname + filename))

    print 'List of aligned reads: ./LISTS/alignedlist.txt'
    return './LISTS/alignedlist.txt'



# Merge multiple reads per sample
def merge_reads(opts, samples, sampledict, filename, outcommand, nstep, nsamples, roadrunner):

    # check that reads need to be merged; if not close and return previous step file name.
    list2merge = [sample for sample in samples if len(sampledict[sample]) > 1]
    if len(list2merge) == 0:
        return filename
    else:
        nSample2Merge = len(list2merge)


    prname = "./SCRIPTS/" + nstep + '-mergeReads.sh'
    print 'Setting up pipeline to merge aligned reads...'
    if not os.path.exists('./ALIGN/'): os.mkdir('./ALIGN/')
    outf = open(prname, 'w')

    # Start create header for SH file.
    dependencies = ["bamtools", "samtools"]
    h = Header(opts)
    pname = creates_pname("mr")
    h.processname(pname)
    h.setcores(opts.nthreadsAlignment)
    h.setmemory(16)
    h.runtime(96)
    h.holdjid(opts.wait + opts.waitad)
    h.holdjidad(None)
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setnjobs(nSample2Merge)
    h.setconcurrency(nSample2Merge)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get()     
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''if [ -e ALIGN/${sample}/${sample}.merged.bam.sort ]; then rm ALIGN/${sample}/${sample}.merged.bam.sort; fi\n'''
    header += '''if [ -e ./ALIGN/${sample}/tmplist.txt ]; then rm ./ALIGN/${sample}/tmplist.txt; fi\n'''
    header += '''if [ ! -e ./ALIGN/$sample ]; then mkdir ./ALIGN/$sample; fi\n'''
    header += '''for f in $(ls ./ALIGN/$sample/*.bam.sort); do rname=$(realpath ${f}); echo ${rname}; done >> ./ALIGN/${sample}/tmplist.txt\n'''
    header += '''echo "Merging: "; while read p; do echo ${p}; done < ./ALIGN/${sample}/tmplist.txt\n'''
    header += 'bamtools merge -list ./ALIGN/${sample}/tmplist.txt -out ALIGN/${sample}/${sample}.merged.tmpa.bam\n'
    header += 'while read p; do rm ${p}; done < ./ALIGN/${sample}/tmplist.txt && rm ./ALIGN/${sample}/tmplist.txt\n'
    header += 'samtools sort -T ALIGN/${sample}/ -@ ${NSLOTS} -m 10G -o ALIGN/${sample}/${sample}.merged.bam.sort ALIGN/${sample}/${sample}.merged.tmpa.bam  && rm ALIGN/${sample}/${sample}.merged.tmpa.bam\n'
    header += 'bamtools index -in ALIGN/${sample}/${sample}.merged.bam.sort\n'
    header += '%s\n' % '\n# Get alignment statistics.\nsamtools flagstat ./ALIGN/${sample}/${sample}.merged.bam.sort > ./ALIGN/${sample}/${sample}.alignmentStats' 
    header += '''samtools view ./ALIGN/${sample}/${sample}.merged.bam.sort | awk 'length($10)"M" == $6 {print}' | wc -l | awk '{print "Perfectly aligned: ", $1}' >> ./ALIGN/${sample}/${sample}.alignmentStats\n''' 

    # Save script.
    outf.write('%s\n' % header)
    outf.write('''if [ ! -e ./OldOutputLogs ]; then mkdir ./OldOutputLogs; fi\n''')

    # Save list of sample to merge.
    sample2merge = open('./LISTS/sample2merge.txt', 'w')
    [sample2merge.write('%s\n' % sample) for sample in samples if len(sampledict[sample]) > 1]
    sample2merge.close()
    sample2merge = "./LISTS/sample2merge.txt"

    
    # Perform actual alignment OR save command line to launch by hand.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('%s\n' % ('qsub %s '%prname + sample2merge))

    # Save merged 
    mergedSample = open('./LISTS/mergedSamples.txt', 'w')
    for sample in samples:
        if len(sampledict[sample]) > 1: 
            mergedSample.write('%s\t%s\n' % (sample, os.path.join('ALIGN',sample,sample + '.merged.bam.sort')))
            continue
    for line in open(filename):
        sample = line.strip().split()[0]
        if len(sampledict[sample])==1:
            mergedSample.write(line) 
    mergedSample.close()
    return './LISTS/mergedSamples.txt'



# Make read processor script
def process_reads(opts, samples, sampleDict, filename, outcommand, nstep, nsamples, roadrunner):

    prname = "./SCRIPTS/" + nstep + '-ProcessReads.sh'

    print 'Setting up pipeline to process aligned reads...'
    if not os.path.exists('./ALIGN/'): os.mkdir('./ALIGN/')
    outf = open(prname, 'w')
    
    # Number of alignments to process
    nalignments = 0
    for line in open(filename):
        nalignments += 1

    
    # Start create header for SH file.
    dependencies = ["java", "samtools", "gatk"]
    h = Header(opts)
    pname = creates_pname("pr")
    h.processname(pname)
    h.setcores(opts.nthreads)
    
    # Define memory configuration.
    minmemory_tot = 32
    h.setmemory(minmemory_tot/int(opts.nthreads))
    h.runtime(48)
    h.setnjobs(nalignments)
    h.setconcurrency(opts.nprocesses)
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))

    # Control if next step is merging. If not, use array locker.
    list2merge = [sample for sample in samples if len(sampleDict[sample]) > 1]
    if len(list2merge) == 0:
        opts.waitad += pname + ","
    else:
        opts.wait += pname + ","

    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' | tr ',' ' ' )\n\n'''
    header += '''rName=$(basename -s ".tmp.bam" $reads)\n'''
    header += '''if [ ! -e ./ALIGN/$sample ]; then mkdir ./ALIGN/$sample; fi\n'''

    outf.write(header) 

    # Add read group and sort bam file
    outf.write('%s\n' % ' '.join(['gatk --java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0} -Xmx{1}g"'.format(roadrunner.jobCores, minmemory_tot - 4),
                                    'AddOrReplaceReadGroups', 
                                    '-I $reads',
                                    '-O ALIGN/${sample}/${rName}.bam',
                                    '--SORT_ORDER coordinate',
                                    '--TMP_DIR ./ALIGN/$sample', 
                                    '--MAX_RECORDS_IN_RAM 2000000',
                                    '--RGLB lib1', '--RGPU unit1', '--RGPL illumina', '--RGSM $sample', opts.GATKoptions.get("AddOrReplaceReadGroups", "") ,
                                    "&& rm $reads"]))


    # Eventually save unmapped reads
    if opts.saveUnmap.lower() == "y":
        outf.write('%s\n' % 'samtools view -b -f 4 ALIGN/${sample}/${rName}.bam > ./ALIGN/${sample}/${rName}.unmapped.bam')


    # Drop zero mapping quality reads
    outf.write('%s\n' % ' '.join(['gatk --java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0} -Xmx{1}g"'.format(roadrunner.jobCores, minmemory_tot - 4) ,
                                    'PrintReads',
                                    '-R', opts.ref_genome_path,
                                    '-I ./ALIGN/${sample}/${rName}.bam',
                                    '--read-filter MappingQualityNotZeroReadFilter',
                                    '--read-filter MappedReadFilter',
                                    '-O ALIGN/${sample}/${rName}.bam.sort', opts.GATKoptions.get("PrintReads", ""),
                                    '&& rm ./ALIGN/$sample/${rName}.bam']))


    # Validate bam file.
    outf.write('%s\n' % ' '.join(['gatk --java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0} -Xmx{1}g"'.format(roadrunner.jobCores, minmemory_tot - 4) ,
                                    'ValidateSamFile', 
                                    '-I ./ALIGN/${sample}/${rName}.bam.sort',
                                    '--TMP_DIR ./ALIGN/$sample',
                                    '--MODE SUMMARY',
                                    '-O', './ALIGN/${sample}/${rName}.validate', opts.GATKoptions.get("ValidateSamFile", "")]))

    # Save header.
    outf.write('%s\n' % ' '.join(['samtools view -H ', 
                                        'ALIGN/${sample}/${rName}.bam.sort >> ALIGN/${sample}/HEADER_${rName}.txt']))


    outlist = open('./LISTS/processedlist.txt', 'w')
    tmprows = []
    for line in open(filename):
        sample, read = line.strip().split()
        if not os.path.exists(os.path.join('./ALIGN/', sample)): os.mkdir('./ALIGN/' + sample)
        rName = read.split("/")[-1].replace(".tmp.bam", "")
        tmprows.append('%s\t%s\n' % (sample, 'ALIGN/'+sample+'/'+rName+'.bam.sort'))
    newrows = []
    for row in tmprows:
        if row not in newrows: 
            newrows.append(row)
    for l in newrows:
        outlist.write('%s' % (l))
    outlist.close()


    # Merge files.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('%s\n' % ('qsub %s '%prname + filename))
    
    print 'List of processed and aligned reads: ./LISTS/processedlist.txt'
    
    return './LISTS/processedlist.txt'



# Make de-duplicates removal script
def MarkDup(opts, samples, sampleDict, filename, outcommand, nstep, nsamples, prgFolder, roadrunner):

    prname = "./SCRIPTS/" + nstep + '-Dedup_pipeline.sh'
    nalignments = 0
    for sample in sampleDict:
        nalignments += 1
    # Start create header for SH file.
    dependencies = ["java", "samtools", "gatk"]
    h = Header(opts)
    pname = creates_pname("md")
    h.processname(pname)
    h.setcores(opts.nthreads)
    minmemory_tot = 32
    h.setmemory(minmemory_tot/int(opts.nthreads))
    h.runtime(72)
    h.setnjobs(nalignments)
    h.setconcurrency(opts.nprocesses)
    list2merge = [sample for sample in samples if len(sampleDict[sample]) > 1]
    if len(list2merge) == 0:
        h.holdjid(opts.wait)
        h.holdjidad(opts.waitad)
    else:
        opts.wait = opts.wait + opts.waitad
        opts.waitad = ""
        h.holdjid(opts.wait)
        h.holdjidad(opts.waitad)

    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    if opts.BQSR == "n" and areFile(opts.dbsnpf):
        opts.wait += pname + ","
    else:
        opts.waitad += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' )\n\n'''
    header += '''rName=$(basename -s ".bam.sort" $reads)\n'''
    header += '''if [ !  -e ./DEDUP ]; then mkdir ./DEDUP; fi\n'''
    header += '''if [ ! -e ./DEDUP/$sample ]; then mkdir ./DEDUP/$sample; fi\n'''

    outf = open(prname,'w')
    outf.write(header)
    print 'Creating de-dup script.'
    if not os.path.exists('./DEDUP/'): os.mkdir('./DEDUP/')
        
    # Re-implement with GATK 4 MarkDuplicates
    outf.write('%s\n' % ' '.join(['gatk --java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0} -Xmx{1}g"'.format(roadrunner.jobCores, minmemory_tot -  4), 
                                'MarkDuplicates -I $reads', 
                                '-O DEDUP/$sample/${sample}.dedup.bam -M DEDUP/$sample/marked_dup_metrics_${sample}.txt',
                                '--TMP_DIR ./DEDUP/$sample', 
                                "-CREATE_INDEX true", opts.GATKoptions.get("MarkDuplicates", ""),
                                '&& rm $reads']))
    
    # Validate bam file.
    outf.write('%s\n' % ' '.join(['gatk ValidateSamFile', 
                                    '--java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0} -Xmx{1}g"'.format(roadrunner.jobCores, minmemory_tot -  4), 
                                    '-I ./DEDUP/$sample/${sample}.dedup.bam',
                                    '--{} ./DEDUP/$sample'.format("TMP_DIR"),
                                    '--MODE SUMMARY', 
                                    '-O', './DEDUP/$sample/${sample}.validate', opts.GATKoptions.get("ValidateSamFile", "")]))

    # Get coverage informations
    outf.write('%s\n' % ' '.join(['if [ -e DEDUP/$sample ]; then\nsamtools depth -a',
                                'DEDUP/$sample/${sample}.dedup.bam', ">",
                                "DEDUP/$sample/${sample}.dedup.depthStats", 
                                "&& python", prgFolder + "/misc/AverageDepth.py DEDUP/$sample/${sample}.dedup.depthStats > DEDUP/$sample/${sample}.dedup.avrgDepth",
                                " && rm DEDUP/$sample/${sample}.dedup.depthStats; fi"]))
                                
    outf.close()

    outlist = open('./LISTS/deduppedlist.txt', 'w')
    for line in open(filename):
        sample, reads = line.strip().split()
        if not os.path.exists(os.path.join('./DEDUP/', sample)): os.mkdir('./DEDUP/' + sample)
        outlist.write('%s\t%s\n' % (sample, ('./DEDUP/'+sample+'/'+sample+'.dedup.bam')))
    outlist.close()

    # Launch or save the qsub command
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('%s\n' % ('qsub %s '%prname + filename))

    print 'Outputs from deduplication are listed in ./LISTS/deduppedlist.txt'
    return './LISTS/deduppedlist.txt'




# Make Base Quality ReCalibration script
def BQSR(opts, samples, filename, outcommand, nstep, nsamples, roadrunner):

    import os
    nalignments = 0
    for sample in samples:
        nalignments += 1
    prname = "./SCRIPTS/" + nstep + '-BaseQualityReCal_pipeline.sh'
    dbsnpf = opts.dbsnpf.split(',')
    dbsnpf = ' '.join(['--known-sites '+f for f in dbsnpf])

    pname = creates_pname("bq")
    if opts.clustermode != "local":
        # Start create header for SH file.
        dependencies = ["java", "gatk"]
        h = Header(opts)
        h.processname(pname)
        h.setcores(opts.nthreads)
        minmemory_tot = 32
        h.setmemory(minmemory_tot/int(opts.nthreads))
        h.runtime(48)
        h.setnjobs(nalignments)
        h.setconcurrency(opts.nprocesses)
        h.holdjid(opts.wait)
        h.holdjidad(opts.waitad)
        h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
        h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
        opts.wait += opts.waitad + pname + ","
        opts.waitad = ""
        h.addmailaddr(opts.mailaddr)
        h.setprj(opts.prj)
        for i in dependencies:
            h.add_dependency(i)
        header = h.get() 
    else:
        header = "#!/bin/bash\n\n"
        header = "SGE_TASK_ID=$2"
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' )\n\n'''
    header += '''if [ !  -e ./BQSR ]; then mkdir ./BQSR; fi\n'''
    header += '''if [ ! -e ./BQSR/$sample ]; then\n\tmkdir ./BQSR/$sample\nelse\n\trm -r ./BQSR/$sample/*\nfi\n'''
    


    print 'Base Quality ReCalibration.'    

    print 'Creating BQSR script.'
    outf = open(prname, 'w')
    outf.write(header)

    ### Indel Realignment.
    if not os.path.exists('./BQSR/'): os.mkdir('./BQSR/')
    


    # Identify target sequences.
    outf.write('%s\n' % ' '.join(['gatk --java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0} -Xmx{1}g"'.format(roadrunner.jobCores, minmemory_tot -  4),
                                    'BaseRecalibrator', 
                                    '-R ' + opts.ref_genome_path,
                                    '-I $reads', dbsnpf,
                                    '-O BQSR/$sample/recalibration_report_$sample.table', 
                                    opts.GATKoptions.get("BaseRecalibrator", "")]))
    
    # Perform realingment.
    outf.write('%s\n' % ' '.join(['gatk --java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0} -Xmx{1}g"'.format(roadrunner.jobCores, minmemory_tot -  4),
                                    'ApplyBQSR', 
                                    '-R ' + opts.ref_genome_path, 
                                    '-I $reads',
                                    '--bqsr-recal-file', './BQSR/$sample/recalibration_report_$sample.table', 
                                    '-O', './BQSR/$sample/$sample.bqsr.bam', opts.GATKoptions.get("ApplyBQSR", "")]))
    
    # Validate bam file.
    outf.write('%s\n' % ' '.join(['gatk --java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0} -Xmx{1}g"'.format(roadrunner.jobCores, minmemory_tot -  4),
                                    'ValidateSamFile', 
                                    '-I ./BQSR/$sample/$sample.bqsr.bam',
                                    '--TMP_DIR ./BQSR/$sample',
                                    '--MODE SUMMARY', 
                                    '-O', './BQSR/$sample/$sample.validate', 
                                    opts.GATKoptions.get("ValidateSamFile", ""),
                                    '&& rm $reads']))

    outf.close()



    print 'Base quality recalibration script ready.'
    outlist = open('./LISTS/recalibratedlist.txt', 'w')
    for line in open(filename):
        sample, reads = line.strip().split()
        if not os.path.exists(os.path.join('./BQSR/', sample)): os.mkdir('./BQSR/' + sample)
        outlist.write('%s\t%s\n' % (sample, ('./BQSR/'+sample+'/'+sample+'.bqsr.bam')))
    outlist.close()

    # Launch or save the qsub command
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    cmd = roadrunner.saveCommand()
    roadrunner.commandNtasks[cmd] = nalignments
    try: outcommand.write('%s\n' % (cmd))
    except: outcommand.write('%s\n' % (cmd[0]))
    print 'Outputs from Base Quality ReCalibration are listed in ./LISTS/recalibratedlist.txt'
    return './LISTS/recalibratedlist.txt'



# Make Base Quality ReCalibration script
def sumalign(opts, outcommand, nstep, prgFolder, roadrunner):

    prname = "./SCRIPTS/" + nstep + '-SummarizeAlignmentStats.sh'

    # Start create header for SH file.
    h = Header(opts)
    pname = creates_pname("as")
    h.processname(pname)
    h.setcores(1)
    minmemory_tot = 32
    h.setmemory(minmemory_tot/int(opts.nthreads))
    h.runtime(6)
    h.holdjid(opts.wait + opts.waitad)
    h.holdjidad(None)
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setconcurrency(1)
    h.setprj(opts.prj)
    header = h.get() 


    print 'summarize alignment statistics.'    

    print 'Creating Alignment Statistics script.'
    outf = open(prname, 'w')
    outf.write(header)

    # Identify target sequences.
    outf.write('%s\n' % ' '.join(['python', prgFolder + '/misc/SummarizeAlignmentStats.py',
                                    '>', "./AlignmentStatistics.csv"]))
    outf.close()



    # Launch or save the qsub command
    roadrunner.parse(script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    cmd = 'qsub {}'.format(prname)
    outcommand.write('%s\n' % (cmd))

    print 'Once alignment will be completed, you will find summary statistics in AlignmentStatistics.csv'
    return 0



# Make Base Quality ReCalibration script
def sumdepth(opts, outcommand, nstep, prgFolder, roadrunner):

    prname = "./SCRIPTS/" + nstep + '-SummarizeDepth.sh'

    # Start create header for SH file.
    h = Header(opts)
    pname = creates_pname("ds")
    h.processname(pname)
    h.setcores(1)
    minmemory_tot = 32
    h.setmemory(minmemory_tot/int(opts.nthreads))
    h.runtime(6)
    h.holdjid(opts.wait + opts.waitad)
    h.holdjidad(None)
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setconcurrency(1)
    h.setprj(opts.prj)
    header = h.get() 


    print 'Creating Depth Statistics script.'
    outf = open(prname, 'w')
    outf.write(header)

    # Identify target sequences.
    try: 
        chrs = ','.join(opts.chromosomes)
    except:
        chrs = "all"
    if chrs == "": chrs = "all"

    # Write actual command
    outf.write('%s\n' % ' '.join(['python', prgFolder + '/misc/SummarizeDepth.py',
                                    chrs,
                                    '>', "./DepthStatistics.csv"]))
    outf.close()



    # Launch or save the qsub command
    roadrunner.parse(script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    cmd = 'qsub {}'.format(prname)
    outcommand.write('%s\n' % (cmd))

    print 'Summary statistics in DepthStatistics.csv'
    return 0

