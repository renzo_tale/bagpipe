########################################################
# This file contains all methods working on vcf data,  #
# from variant calling to variant annotation           #
########################################################


import os
from lib.Utilities import Header, creates_pname, GetKaryotypes, intervals



# Make variant calling script.
def HCall(opts, samples, filename, outcommand, nstep, nsamples, roadrunner):

    prname = "./SCRIPTS/" + nstep + '-HaplotypeCaller_pipeline.sh'
    nalignments = 0
    for sample in samples:
        nalignments += 1
    # Start create header for SH file.
    dependencies = ["java", "gatk"]
    h = Header(opts)
    #h.processname("vcaller")
    pname = creates_pname("hc")
    h.processname(pname)
    h.setcores(opts.nthreads)
    minmemory_tot = 32
    h.setmemory(minmemory_tot/int(opts.nthreads))
    h.setnjobs(nalignments)
    h.setconcurrency(opts.nprocesses)
    h.runtime(500)
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    h.holdjid(opts.wait + opts.waitad)
    #opts.wait = "vcaller"
    opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 

    #intervals = opts.ref_genome_path.replace('.fasta', '.intervals').replace('.fa', '.intervals').replace('.fna', '.intervals')
    # Define remaining steps.
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' )\n\n'''
    if opts.sexcheck == "y":
        header += '''male=$(grep -w $sample ./LISTS/males.txt | wc -l | awk '{print $1}')\n'''
    else:
        header += '''male=0\n'''
    header += '''if [ ! -e ./VCALL ]; then mkdir ./VCALL; fi\n'''
    header += '''if [ ! -e ./VCALL/$sample ]; then mkdir ./VCALL/$sample; fi\n'''
    print 'Creating Variant calling script.'
    outf = open(prname, 'w')
    outf.write(header)

    if not os.path.exists('./VCALL/'): os.mkdir('./VCALL/')
    

    # Create command line for GATK HaplotypeCaller
    if opts.chromosomes == "all":
        command_line = ['gatk --java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0} -Xmx{1}g"'.format(roadrunner.jobCores, minmemory_tot - 4),
                        'HaplotypeCaller', 
                        '-R ' + opts.ref_genome_path, '-I $reads', 
                        '--ERC GVCF -A AlleleFraction', "--native-pair-hmm-threads ${}".format(roadrunner.jobCores),
                        "--sample-ploidy {}".format(opts.ploidy),
                        '-O', './VCALL/$sample/$sample.diploid.raw.g.vcf.gz', opts.GATKoptions.get("HaplotypeCaller", ""),
                        '\n']
        # Identify variants for provided sample.
        outf.write('{}\n'.format(' '.join(command_line)))
    else:
        command_line = ['gatk --java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0} -Xmx{1}g"'.format(roadrunner.jobCores, minmemory_tot - 4),
                        'HaplotypeCaller', 
                        '-R ' + opts.ref_genome_path, '-I $reads', 
                        '--ERC GVCF -A AlleleFraction', "--native-pair-hmm-threads ${}".format(roadrunner.jobCores),
                        "--sample-ploidy {}".format(opts.ploidy),
                        '-O', './VCALL/$sample/$sample.diploid.raw.g.vcf.gz',
                        ' '.join(["-L "+i for i in opts.chromosomes if i.isdigit()]), opts.GATKoptions.get("HaplotypeCaller", ""), 
                        '\n']
        # Identify variants for provided sample.
        outf.write('{}\n'.format(' '.join(command_line)))

        # Sex specific and mithocondrion
        if opts.sexcheck.lower() == "y":
            if "X" in opts.chromosomes or "Y" in opts.chromosomes or "MT" in opts.chromosomes:
                outf.write('if [ $male == 1 ];\nthen\n')
                if "X" in opts.chromosomes:
                    command_line = ['gatk --java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0} -Xmx{1}g"'.format(roadrunner.jobCores, minmemory_tot - 4),
                                    'HaplotypeCaller', 
                                    '-R ' + opts.ref_genome_path, '-I $reads', 
                                    '--ERC GVCF -A AlleleFraction', "--native-pair-hmm-threads ${}".format(roadrunner.jobCores),
                                    '-O', './VCALL/$sample/$sample.X.raw.g.vcf.gz',
                                    '-L X', "--sample-ploidy {}".format(opts.ploidy / 2), opts.GATKoptions.get("HaplotypeCaller", ""),
                                    '\n']
                    outf.write('\t{}\n'.format(' '.join(command_line)))
                if "Y" in opts.chromosomes:
                    command_line = ['gatk --java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0} -Xmx{1}g"'.format(roadrunner.jobCores, minmemory_tot - 4),
                                    'HaplotypeCaller', 
                                    '-R ' + opts.ref_genome_path, '-I $reads', 
                                    '--ERC GVCF -A AlleleFraction', "--native-pair-hmm-threads ${}".format(roadrunner.jobCores),
                                    '-O', './VCALL/$sample/$sample.Y.raw.g.vcf.gz',
                                    '-L Y', "--sample-ploidy {}".format(opts.ploidy / 2), opts.GATKoptions.get("HaplotypeCaller", ""),
                                    '\n']
                    outf.write('\t{}\n'.format(' '.join(command_line)))
                if "MT" in opts.chromosomes:
                    command_line = ['gatk --java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0} -Xmx{1}g"'.format(roadrunner.jobCores, minmemory_tot - 4),
                                    'HaplotypeCaller', 
                                    '-R ' + opts.ref_genome_path, '-I $reads', 
                                    '--ERC GVCF -A AlleleFraction', "--native-pair-hmm-threads ${}".format(roadrunner.jobCores),
                                    '-O', './VCALL/$sample/$sample.MT.raw.g.vcf.gz',
                                    '-L MT', "--sample-ploidy {}".format(opts.ploidy / 2), opts.GATKoptions.get("HaplotypeCaller", ""),
                                    '\n']
                    outf.write('\t{}\n'.format(' '.join(command_line)))
                outf.write('else\n')
                if "X" in opts.chromosomes:
                    command_line = ['gatk --java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0} -Xmx{1}g"'.format(roadrunner.jobCores, minmemory_tot - 4),
                                'HaplotypeCaller', 
                                '-R ' + opts.ref_genome_path, '-I $reads', 
                                '--ERC GVCF -A AlleleFraction', "--native-pair-hmm-threads ${}".format(roadrunner.jobCores),
                                '-O', './VCALL/$sample/$sample.X.raw.g.vcf.gz',
                                '-L X', "--sample-ploidy {}".format(opts.ploidy), opts.GATKoptions.get("HaplotypeCaller", ""),
                                '\n']
                    outf.write('\t{}\n'.format(' '.join(command_line)))
                if "Y" in opts.chromosomes:
                    command_line = ['gatk --java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0} -Xmx{1}g"'.format(roadrunner.jobCores, minmemory_tot - 4),
                                'HaplotypeCaller', 
                                '-R ' + opts.ref_genome_path, '-I $reads', 
                                '--ERC GVCF -A AlleleFraction', "--native-pair-hmm-threads ${}".format(roadrunner.jobCores),
                                '-O', './VCALL/$sample/$sample.Y.raw.g.vcf.gz',
                                '-L Y', "--sample-ploidy {}".format(opts.ploidy / 2), opts.GATKoptions.get("HaplotypeCaller", ""),
                                '\n']
                    outf.write('\t{}\n'.format(' '.join(command_line)))
                if "MT" in opts.chromosomes:
                    command_line = ['gatk --java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0} -Xmx{1}g"'.format(roadrunner.jobCores, minmemory_tot - 4),
                                'HaplotypeCaller', 
                                '-R ' + opts.ref_genome_path, '-I $reads', 
                                '--ERC GVCF -A AlleleFraction', "--native-pair-hmm-threads ${}".format(roadrunner.jobCores),
                                '-O', './VCALL/$sample/$sample.MT.raw.g.vcf.gz',
                                '-L MT', "--sample-ploidy {}".format(opts.ploidy / 2), opts.GATKoptions.get("HaplotypeCaller", ""),
                                '\n']
                    outf.write('\t{}\n'.format(' '.join(command_line)))
                outf.write("fi\n") 
        xcludedItvs = ["-XL "+i for i in opts.chromosomes if i.isdigit()]
        if opts.sexcheck.lower() == "y":
            if "X" in opts.chromosomes:  xcludedItvs += ["-XL X"]
            if "Y" in opts.chromosomes:  xcludedItvs += ["-XL Y"]
            if "MT" in opts.chromosomes: xcludedItvs += ["-XL MT"]
        command_line = ['gatk --java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0} -Xmx{1}g"'.format(roadrunner.jobCores, minmemory_tot - 4),
                        'HaplotypeCaller',  
                        '-R ' + opts.ref_genome_path, '-I $reads', 
                        '--ERC GVCF -A AlleleFraction', "--native-pair-hmm-threads ${}".format(roadrunner.jobCores),
                        "--sample-ploidy {}".format(opts.ploidy),
                        '-O', './VCALL/$sample/$sample.otherCtgs.raw.g.vcf.gz',
                        ' '.join(xcludedItvs), opts.GATKoptions.get("HaplotypeCaller", ""), 
                        '&& rm $reads\n']
        # Identify variants for provided sample.
        outf.write('{}\n'.format(' '.join(command_line))) 


    outf.close()


    # Writing file list.
    print 'Variant call script ready.'
    outlist = open('./LISTS/vcalledlist.txt', 'w')
    for sample in samples:
        if not os.path.exists(os.path.join('./VCALL/', sample)): os.mkdir('./VCALL/' + sample)
        outlist.write('%s\t%s\n' % (sample, ('./VCALL/'+sample+'/'+sample+'.diploid.raw.g.vcf.gz')))
    outlist.close()

    # Run or save qsub command line.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    cmd = 'qsub {} {}'.format(prname, filename)
    outcommand.write('%s\n' % (cmd))

    print 'Outputs from Variant Calling are listed in ./LISTS/vcalledlist.txt'
    return './LISTS/vcalledlist.txt'




# Make variant calling script.
def HCall_sct(opts, samples, filename, outcommand, nstep, nsamples, roadrunner):

    # Define intervals first
    if opts.chunkSize == 0:
        opts.chunkSize = 10000000
    itvlist = intervals(opts)
    print("Created intervals.")
    nsets = sum([1 for itv in open("./LISTS/Intervals.txt")])
    nconcurrent = nsets
    if nsets > 150:
        nconcurrent = 150
    

    # Start create header for SH file.
    prname = "./SCRIPTS/" + nstep + '-HaplotypeCaller_pipeline.sh'
    dependencies = ["java", "gatk"]
    h = Header(opts)
    #h.processname("vcaller")
    pname = creates_pname("hc")
    h.processname(pname)
    h.setcores(opts.nthreads)
    minmemory_tot = 32
    h.setmemory(minmemory_tot/int(opts.nthreads))
    h.setnjobs(nsets)
    h.setconcurrency(nconcurrent)
    h.runtime(24)
    h.stderrSet("./LOGS/{}.$JOB_ID.$TASK_ID.err".format(pname))
    h.stdoutSet("./LOGS/{}.$JOB_ID.$TASK_ID.out".format(pname))
    h.holdjid(opts.wait + opts.waitad)
    opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 



    #intervals = opts.ref_genome_path.replace('.fasta', '.intervals').replace('.fa', '.intervals').replace('.fna', '.intervals')
    # Define remaining steps.
    header += '''sample=$3\n'''
    header += '''ctg=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''bpi=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' )\n'''
    header += '''bpe=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $3}' )\n'''
    header += '''itv=${ctg}:${bpi}-${bpe}\n'''
    header += '''reads=$( grep -w $sample $2 | awk '{print $2}' )\n\n'''
    header += '''if [ ! -e ./VCALL ]; then mkdir ./VCALL; fi\n'''
    header += '''if [ ! -e ./VCALL/$sample ]; then mkdir ./VCALL/$sample; fi\n\n'''
    print 'Creating Variant calling script.'
    outf = open(prname, 'w')
    outf.write(header)

    if not os.path.exists('./VCALL/'): os.mkdir('./VCALL/')
    

    # Create command line for GATK HaplotypeCaller
    cmd = '''ploidy={}\n'''.format(opts.ploidy)
    if opts.sexcheck == "y":
        cmd += '''male=$(grep -w $sample ./LISTS/males.txt | wc -l | awk '{print $1}')\n\n'''
    else:
        cmd += '''male=0\n\n'''

    cmd += '''if [ $ctg == "Y" ] || [ $ctg == "MT" ] || [ $ctg == "y" ] || [ $ctg == "Mt" ] || [ $ctg == "mt" ]; then ploidy=$((ploidy / 2));fi\n'''
    cmd += '''if [ $ctg == "X" ] && [ $male == 1 ]; then ploidy=$((ploidy / 2));fi\n'''    
    cmd += '''if [ $ploidy -lt 1 ]; then ploidy=1;fi\n'''

    outf.write('{}\n'.format(''.join(cmd)))

    command_line = ['gatk --java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0} -Xmx{1}g"'.format(roadrunner.jobCores, minmemory_tot - 4),
                    'HaplotypeCaller', 
                    '-R ' + opts.ref_genome_path, '-I $reads', 
                    '--ERC GVCF', "--native-pair-hmm-threads ${}".format(roadrunner.jobCores),
                    "--sample-ploidy $ploidy -A AlleleFraction",
                    '-O', './VCALL/$sample/$sample.$ctg.$bpi.$bpe.raw.g.vcf.gz',
                    "-L $itv", opts.GATKoptions.get("HaplotypeCaller", ""), 
                    '\n']
    # Identify variants for provided sample.
    outf.write('{}\n'.format(' '.join(command_line)))
    outf.close()


    # Writing file list.
    print 'Variant call script ready.'
    outlist = open('./LISTS/calledsamples.txt', 'w')
    for sample in samples:
        if not os.path.exists(os.path.join('./VCALL/', sample)): os.mkdir('./VCALL/' + sample)
        outtmplist = open("./VCALL/{0}/{0}.gvcflist".format(sample), "w")
        for line in open(itvlist):
            ctg, bpi, bpe = line.strip().split()
            outtmplist.write('./VCALL/{0}/{0}.{1}.{2}.{3}.raw.g.vcf.gz\n'.format(sample, ctg, bpi, bpe))
        outtmplist.close()
        outlist.write("{0}\t./VCALL/{0}/{0}.gvcflist\n".format(sample))
        roadrunner.parse('./LISTS/Intervals.txt', filename, sample, script_name=prname, jobname=pname)
        roadrunner.saveCommand()
        outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))
    outlist.close()

    # Run or save qsub command line.

    print 'Outputs from Variant Calling are listed in ./LISTS/calledsamples.txt'
    return './LISTS/calledsamples.txt'



def gatherGVCFs(samples, filename, outcommand, nstep, opts, roadrunner):

    prname = "./SCRIPTS/" + nstep + '-gatherGVCF.sh'
    
    # Start create header for SH file.
    dependencies = ["java", "vcftools", "tabix"]
    h = Header(opts)
    #h.processname("vjoin")
    pname = creates_pname("gg")
    h.processname(pname)
    h.setcores(opts.nthreads)
    h.setmemory(48)
    h.runtime(24)
    h.setnjobs(len(samples))
    h.setconcurrency(len(samples))
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    h.addmailaddr(opts.mailaddr)
    h.holdjid(opts.wait + opts.waitad)
    opts.wait += pname + ","
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    print "Creating script to combine single-interval VCFs."
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''flist=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' )\n'''
    header += '''male=$(grep -w $sample ./LISTS/males.txt | wc -l | awk '{print $1}')\n'''
    header += '''if [ $male == 1 ]; then\n\tawk '$0!~".X." && $0!~".Y." && toupper($0)!~".MT"{print}' $flist > ${flist}.new\n\tawk '$0~".X." || $0~".Y." || toupper($0)~".MT"{print}' $flist > ${flist}.hap\n\ttmp=${flist}.new\n\ttmp2=${flist}.hap\n\tflist=$tmp\nfi\n''' 
    header += '''if [ $male == 0 ]; then\n\tawk '$0!~".Y." && toupper($0)!~".MT"{print}' $flist > ${flist}.new\n\tawk '$0~".Y." || toupper($0)~".MT"{print}' $flist > ${flist}.hap\n\ttmp=${flist}.new\n\ttmp2=${flist}.hap\n\tflist=$tmp\nfi\n''' 
    header += '''vcf-concat -f $flist | bgzip -c > ./VCALL/$sample/$sample.diploid.raw.g.vcf.gz && tabix -p vcf ./VCALL/$sample/$sample.diploid.raw.g.vcf.gz\n'''
    header += '''vcf-concat -f ${tmp2} | bgzip -c > ./VCALL/$sample/$sample.haploid.raw.g.vcf.gz && tabix -p vcf ./VCALL/$sample/$sample.haploid.raw.g.vcf.gz\n'''
    header += '''while read i; do rm $i ${i}.tbi; done < $flist\n'''
    header += '''while read i; do rm $i ${i}.tbi; done< ${tmp2}\n'''
    header += '''echo "Variants joined."\n'''
    outf = open(prname, 'w')
    outf.write(header)
    outf.close()
    print 'Script for joint VCF creation is ready.'
    print 'Joint VCF file can be found in ./jVCF'

    oflist = open("./LISTS/vcalledlist.txt", "w")
    for sample in samples:
        oflist.write("{0}\t./VCALL/{0}/{0}.diploid.raw.g.vcf.gz\n".format(sample))
    oflist.close()


    # Writing command to run.
    
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    cmd = 'qsub {} {}'.format(prname, filename)
    outcommand.write('%s\n' % (cmd))

    return './jVCF/{}.sorted.vcf.gz'.format(opts.outfile)



def gatherVCFs(samples, filename, outcommand, nstep, opts, roadrunner):

    prname = "./SCRIPTS/" + nstep + '-gatherVCF.sh'
    
    # Start create header for SH file.
    dependencies = ["java", "vcftools", "tabix"]
    h = Header(opts)
    #h.processname("vjoin")
    pname = creates_pname("gg")
    h.processname(pname)
    h.setcores(opts.nthreads)
    h.setmemory(48)
    h.runtime(24)
    h.setnjobs(len(samples))
    h.setconcurrency(len(samples))
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    h.addmailaddr(opts.mailaddr)
    h.holdjid(opts.wait + opts.waitad)
    opts.wait += pname + ","
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    print "Creating script to combine single-interval VCFs."
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''flist=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' )\n'''
    header += '''male=$(grep -w $sample ./LISTS/males.txt | wc -l | awk '{print $1}')\n'''
    header += '''if [ $male == 1 ]; then\n\tawk '$0!~".X." && $0!~".Y." && toupper($0)!~".MT"{print}' $flist > ${flist}.new\n\tawk '$0~".X." || $0~".Y." || toupper($0)~".MT"{print}' $flist > ${flist}.hap\n\ttmp=${flist}.new\n\ttmp2=${flist}.hap\n\tflist=$tmp\nfi\n''' 
    header += '''if [ $male == 0 ]; then\n\tawk '$0!~".Y." && toupper($0)!~".MT"{print}' $flist > ${flist}.new\n\tawk '$0~".Y." || toupper($0)~".MT"{print}' $flist > ${flist}.hap\n\ttmp=${flist}.new\n\ttmp2=${flist}.hap\n\tflist=$tmp\nfi\n''' 
    header += '''vcf-concat -f $flist | bgzip -c > ./VCALL/$sample/$sample.diploid.raw.vcf.gz && tabix -p vcf ./VCALL/$sample/$sample.diploid.raw.vcf.gz\n'''
    header += '''vcf-concat -f ${tmp2} | bgzip -c > ./VCALL/$sample/$sample.haploid.raw.vcf.gz && tabix -p vcf ./VCALL/$sample/$sample.haploid.raw.vcf.gz\n'''
    header += '''while read i; do rm $i ${i}.tbi; done < $flist\n'''
    header += '''while read i; do rm $i ${i}.tbi; done< ${tmp2}\n'''
    header += '''echo "Variants joined."\n'''
    outf = open(prname, 'w')
    outf.write(header)
    outf.close()
    print 'Script for joint VCF creation is ready.'
    print 'Joint VCF file can be found in ./jVCF'

    oflist = open("./LISTS/vcalledlist.txt", "w")
    for sample in samples:
        oflist.write("{0}\t./VCALL/{0}/{0}.diploid.raw.vcf.gz\n".format(sample))
    oflist.close()


    # Writing command to run.
    
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    cmd = 'qsub {} {}'.format(prname, filename)
    outcommand.write('%s\n' % (cmd))

    return './jVCF/{}.sorted.vcf.gz'.format(opts.outfile)





def PCall():

    return None




