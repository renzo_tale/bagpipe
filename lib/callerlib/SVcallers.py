########################################################
# This file contains all methods working on vcf data,  #
# from variant calling to variant annotation           #
########################################################


import os
from lib.Utilities import Header, creates_pname, GetKaryotypes, intervals



# Make GRIDSSV script
def GRIDSS(opts, samples, filename, outcommand, nstep, nsamples, roadrunner):

    import os

    prname = "./SCRIPTS/" + nstep + '-GRIDSS_pipeline.sh'
    nalignments = 0
    for sample in samples:
        nalignments += 1
    # Start create header for SH file.
    dependencies = ["java", "R3.5", "bwa", "samtools"]
    h = Header(opts)
    pname = creates_pname("sv")
    h.processname(pname)
    nthreads = (int(opts.nthreadsAlignment) / 2)
    if nthreads < 1: nthreads = 1
    h.setcores(nthreads)
    minmemory_tot = 64
    h.setmemory(minmemory_tot)
    h.runtime(48)
    h.setnjobs(nalignments)
    h.setconcurrency(opts.nprocesses)
    h.holdjid(opts.wait)
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''INPUT=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' )\n'''
    header += '''TMPN=${INPUT/.bam/.sv.vcf}\n'''
    header += '''OUTPUT=${TMPN/BQSR/GRIDSS_SV}\n'''
    header += '''ASSEMBLY=${OUTPUT/.sv.vcf/.gridss.assembly.bam}\n'''
    header += '''REFERENCE={}\n\n'''.format(opts.ref_genome_path)
    header += '''GRIDSS_JAR={}\n\n'''.format(opts.gridss)

    header += '''if [ !  -e ./GRIDSS_SV ]; then mkdir ./GRIDSS_SV; fi\n'''
    header += '''if [ ! -e ./GRIDSS_SV/$sample ]; then mkdir ./GRIDSS_SV/$sample; fi\n\n'''

    print 'GRIDSSV structural variants detection.'    

    print 'Creating GRIDSSV script.'
    outf = open(prname, 'w')
    outf.write(header)

    ### Indel Realignment.
    if not os.path.exists('./GRIDSS_SV/'): os.mkdir('./GRIDSS_SV/')
    
    # Identify target sequences.
    outf.write('%s\n' % ' '.join(["java -ea -Xmx" + str(minmemory_tot * nthreads -  24) + "g", 
                                    '''-Dreference_fasta="$REFERENCE" \
	-Dsamjdk.create_index=true \
	-Dsamjdk.use_async_io_read_samtools=true \
	-Dsamjdk.use_async_io_write_samtools=true \
	-Dsamjdk.use_async_io_write_tribble=true \
	-Dsamjdk.buffer_size=4194304 \
	-Dgridss.gridss.output_to_temp_file=true \
	-cp $GRIDSS_JAR gridss.CallVariants \
	TMP_DIR=./GRIDSS_SV/$sample \
	WORKING_DIR=./GRIDSS_SV/$sample \
    WORKER_THREADS=${0} \
	REFERENCE_SEQUENCE="$REFERENCE" \
	INPUT="$INPUT" \
	OUTPUT="$OUTPUT" \
	ASSEMBLY="$ASSEMBLY" \
    2>&1 | tee -a LOGS/gridss.${1}.log\n'''.format(roadrunner.jobCores, roadrunner.jobArrayN)]))

    outf.close()



    print 'GRIDSS script ready.'

    # Launch or save the qsub command
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    cmd = 'qsub {} {}'.format(prname, filename)
    outcommand.write('%s\n' % (cmd))

    return 0




# Make Delly script
def delly(opts, samples, filename, outcommand, nstep, nsamples, roadrunner):

    import os

    prname = "./SCRIPTS/" + nstep + '-DELLY_pipeline.sh'
    nalignments = 0
    for sample in samples:
        nalignments += 1
    # Start create header for SH file.
    dependencies = ["delly"]
    h = Header(opts)
    pname = creates_pname("sv")
    h.processname(pname)
    nthreads = 1
    h.setcores(nthreads)
    minmemory_tot = 128
    h.setmemory(minmemory_tot)
    h.runtime(96)
    h.setnjobs(nalignments)
    h.setconcurrency(opts.nprocesses)
    h.holdjid(opts.wait)
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''INPUT=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' )\n'''
    header += '''TMPN=${INPUT/.bam/.sv.bcf}\n'''
    header += '''OUTPUT=${TMPN/BQSR/DELLY_SV}\n'''
    header += '''REFERENCE={}\n\n'''.format(opts.ref_genome_path)

    header += '''if [ !  -e ./DELLY_SV ]; then mkdir ./DELLY_SV; fi\n'''
    header += '''if [ ! -e ./DELLY_SV/$sample ]; then mkdir ./DELLY_SV/$sample; fi\n\n'''

    print 'DELLY structural variants detection.'    

    print 'Creating DELLY script.'
    outf = open(prname, 'w')
    outf.write(header)

    ### Indel Realignment.
    if not os.path.exists('./DELLY_SV/'): os.mkdir('./DELLY_SV/')
    
    # Identify target sequences.
    outf.write('''delly call -g $REFERENCE -o $OUTPUT $INPUT \n'''.format(roadrunner.jobCores, roadrunner.jobArrayN))

    outf.close()


    print 'DELLY script ready.'

    # Launch or save the qsub command
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    cmd = 'qsub {} {}'.format(prname, filename)
    outcommand.write('%s\n' % (cmd))

    return 0
