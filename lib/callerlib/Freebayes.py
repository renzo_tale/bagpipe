########################################################
# This file contains all methods working on vcf data,  #
# from variant calling to variant annotation           #
########################################################


import os
from lib.Utilities import Header, creates_pname, GetKaryotypes, intervals


# Make variant calling script.
def FreebayesCall(opts, samples, filename, outcommand, nstep, nsamples, roadrunner):

    prname = "./SCRIPTS/" + nstep + '-FreeBayes_pipeline.sh'
    nalignments = 0
    for sample in samples:
        nalignments += 1
    # Start create header for SH file.
    dependencies = ["tabix", "bamtools", "gcc5", "cmake", "samtools", "htslib","freebayes"]
    h = Header(opts)
    #h.processname("vcaller")
    pname = creates_pname("fb")
    h.processname(pname)
    h.setcores(opts.nthreads)
    minmemory_tot = 48
    h.setmemory(minmemory_tot)
    h.setnjobs(nalignments)
    h.setconcurrency(opts.nprocesses)
    h.runtime(500)
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    h.holdjid(opts.wait + opts.waitad)
    opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 

    if opts.vcaller == "freebayes-m":
        flag = "--report-monomorphic"
    elif opts.vcaller == "freebayes-g":
        flag = "--gvcf"
    else:
        flag = ""

    #intervals = opts.ref_genome_path.replace('.fasta', '.intervals').replace('.fa', '.intervals').replace('.fna', '.intervals')
    # Define remaining steps.
    header += '''sample=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' )\n\n'''.format(roadrunner.jobArrayN)
    if opts.sexcheck == "y":
        header += '''male=$(grep -w $sample ./LISTS/males.txt | wc -l | awk '{print $1}')\n'''
    else:
        header += '''male=0\n'''
    header += '''if [ ! -e ./VCALL ]; then mkdir ./VCALL; fi\n'''
    header += '''if [ ! -e ./VCALL/$sample ]; then mkdir ./VCALL/$sample; fi\n'''
    header += '''parallelFbayes=`whereis freebayes | awk '{print $2}' | sed 's+/bin+/script+g'`\nexport PATH=$PATH:$parallelFbayes'''
    header += '''export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$fbayespath/../\n'''
    print 'Creating Variant calling script.'
    outf = open(prname, 'w')
    outf.write(header)



    if not os.path.exists('./VCALL/'): os.mkdir('./VCALL/')
    

    # Create command line for GATK HaplotypeCaller
    if opts.chromosomes == "all":
        command_line = 'if [ ${} -eq 1 ]; then\n\tfreebayes {} -f {} -p {} $reads --use-best-n-alleles {} | bgzip -c > ./VCALL/$sample/$sample.diploid.raw.g.vcf.gz'.format(roadrunner.jobCores, flag, opts.ref_genome_path, opts.ploidy, int(opts.ploidy))
        command_line += 'else\n\t${fbayespath}' + '/../scripts/freebayes-parallel <(fasta_generate_regions.py {}.fai 5000) ${} {} --use-best-n-alleles {} -f {} -p {} $reads | bgzip -c > ./VCALL/$sample/$sample.diploid.raw.vcf.gz\nfi\n'.format(opts.ref_genome_path, roadrunner.jobCores, flag, int(opts.ploidy), opts.ref_genome_path, opts.ploidy)
        # Identify variants for provided sample.
        outf.write('{}\n'.format(command_line))
    else:
        command_line = 'if [ ${} -eq 1 ]; then\n\tfreebayes {} -f {} -p {} --use-best-n-alleles {} '.format(roadrunner.jobCores, flag, opts.ref_genome_path, opts.ploidy, opts.ploidy) + ' '.join(["-r "+i for i in opts.chromosomes if i.isdigit()]) + ' $reads | bgzip -c > ./VCALL/$sample/$sample.diploid.raw.g.vcf.gz\n'
        command_line += 'else\n\tfreebayes-parallel <(fasta_generate_regions.py {}.fai 5000) ${} {} --use-best-n-alleles {} -f {} -p {} $reads | bgzip -c > ./VCALL/$sample/$sample.diploid.raw.vcf.gz\nfi\n'.format(opts.ref_genome_path, roadrunner.jobCores, flag, int(opts.ploidy), opts.ref_genome_path, opts.ploidy)
        # Identify variants for provided sample.
        outf.write('{}\n'.format(' '.join(command_line)))

        # Sex specific and mithocondrion
        if opts.sexcheck.lower() == "y":
            if "X" in opts.chromosomes or "Y" in opts.chromosomes or "MT" in opts.chromosomes:
                outf.write('if [ $male == 1 ];\nthen\n')
                if "X" in opts.chromosomes:
                    command_line = '\tif [ ${} -eq 1 ]; then\n\t\tfreebayes'.format(roadrunner.jobCores)
                    command_line += ' {} -f {} -p {} --use-best-n-alleles {} -r X $reads | bgzip -c > ./VCALL/$sample/$sample.X.raw.vcf.gz\n'.format(flag, opts.ref_genome_path, opts.ploidy / 2, opts.ploidy)
                    command_line += '\telse\n\t\tfreebayes-parallel <(fasta_generate_regions.py {}.fai 5000) ${} {} -r X --use-best-n-alleles {} -f {} -p {} $reads | bgzip -c > ./VCALL/$sample/$sample.diploid.raw.vcf.gz\n\tfi\n'.format(opts.ref_genome_path, roadrunner.jobCores, flag, int(opts.ploidy), opts.ref_genome_path, opts.ploidy / 2)
                    outf.write('\t{}\n'.format(' '.join(command_line)))
                if "Y" in opts.chromosomes:
                    command_line = '\tif [ ${} -eq 1 ]; then\n\t\tfreebayes'.format(roadrunner.jobCores) 
                    command_line += ' {} -f {} -p {} --use-best-n-alleles {} -r Y $reads | bgzip -c > ./VCALL/$sample/$sample.Y.raw.vcf.gz\n'.format(flag, opts.ref_genome_path, int(opts.ploidy / 2), int(opts.ploidy))
                    command_line += '\telse\n\t\tfreebayes-parallel <(fasta_generate_regions.py {}.fai 5000) ${} {} -r Y --use-best-n-alleles {} -f {} -p {} $reads | bgzip -c > ./VCALL/$sample/$sample.diploid.raw.vcf.gz\n\tfi\n'.format(opts.ref_genome_path, roadrunner.jobCores, flag, int(opts.ploidy), opts.ref_genome_path, opts.ploidy / 2)
                    outf.write('\t{}\n'.format(' '.join(command_line)))
                if "MT" in opts.chromosomes:
                    command_line = '\tif [ ${} -eq 1 ]; then\n\tfreebayes'.format(roadrunner.jobCores) 
                    command_line += ' {} -f {} -p {} --use-best-n-alleles {} -r MT $reads | bgzip -c > ./VCALL/$sample/$sample.MT.raw.vcf.gz\n'.format(flag, opts.ref_genome_path, int(opts.ploidy / 2), int(opts.ploidy))
                    command_line += '\telse\n\t\tfreebayes-parallel <(fasta_generate_regions.py {}.fai 5000) ${} {} -r MT --use-best-n-alleles {} -f {} -p {} $reads | bgzip -c > ./VCALL/$sample/$sample.diploid.raw.vcf.gz\n\tfi\n'.format(opts.ref_genome_path, roadrunner.jobCores, flag, int(opts.ploidy), opts.ref_genome_path, opts.ploidy / 2)
                    outf.write('\t{}\n'.format(' '.join(command_line)))
                outf.write('else\n')
                if "X" in opts.chromosomes:
                    command_line = '\tif [ ${} -eq 1 ]; then\n\t\tfreebayes'.format(roadrunner.jobCores) 
                    command_line += ' {} -f {} -p {} --use-best-n-alleles {} -r X $reads | bgzip -c > ./VCALL/$sample/$sample.X.raw.vcf.gz\n'.format(flag, opts.ref_genome_path, int(opts.ploidy), int(opts.ploidy))
                    command_line += '\telse\n\t\tfreebayes-parallel <(fasta_generate_regions.py {}.fai 5000) ${} {} -r X --use-best-n-alleles {} -f {} -p {} $reads | bgzip -c > ./VCALL/$sample/$sample.diploid.raw.vcf.gz\n\tfi\n'.format(opts.ref_genome_path, roadrunner.jobCores, flag, int(opts.ploidy), opts.ref_genome_path, opts.ploidy)
                    outf.write('\t{}\n'.format(' '.join(command_line)))
                if "Y" in opts.chromosomes:
                    command_line = '\tif [ ${} -eq 1 ]; then\n\t\tfreebayes'.format(roadrunner.jobCores) 
                    command_line += ' {} -f {} -p {} --use-best-n-alleles {} -r Y $reads | bgzip -c > ./VCALL/$sample/$sample.Y.raw.vcf.gz\n'.format(flag, opts.ref_genome_path, int(opts.ploidy / 2), int(opts.ploidy))
                    command_line += '\telse\n\t\tfreebayes-parallel <(fasta_generate_regions.py {}.fai 5000) ${} {} -r Y --use-best-n-alleles {} -f {} -p {} $reads | bgzip -c > ./VCALL/$sample/$sample.diploid.raw.vcf.gz\n\tfi\n'.format(opts.ref_genome_path, roadrunner.jobCores, flag, int(opts.ploidy), opts.ref_genome_path, opts.ploidy / 2)
                    outf.write('\t{}\n'.format(' '.join(command_line)))
                if "MT" in opts.chromosomes:
                    command_line = '\tif [ ${} -eq 1 ]; then\n\t\tfreebayes'.format(roadrunner.jobCores)
                    command_line += ' {} -f {} -p {} --use-best-n-alleles {} -r MT $reads | bgzip -c > ./VCALL/$sample/$sample.MT.raw.vcf.gz\n'.format(flag, opts.ref_genome_path, int(opts.ploidy / 2), int(opts.ploidy))
                    command_line += '\telse\n\t\tfreebayes-parallel <(fasta_generate_regions.py {}.fai 5000) ${} {} -r MT --use-best-n-alleles {} -f {} -p {} $reads | bgzip -c > ./VCALL/$sample/$sample.diploid.raw.vcf.gz\n\tfi\n'.format(opts.ref_genome_path, roadrunner.jobCores, flag, int(opts.ploidy), opts.ref_genome_path, opts.ploidy / 2)
                    outf.write('\t{}\n'.format(' '.join(command_line)))
                outf.write("fi\n")
        allCTGs = [line.strip().split()[0] for line in open("{}.fai".format(opts.ref_genome_path))]
        remainingCTGs = ' '.join(["-r {}".format(i) for i in allCTGs if i not in map(str, opts.chromosomes)])
        if opts.sexcheck.lower() == 'n':
            if "X" in opts.chromosomes: remainingCTGs += " -r X"
            if "Y" in opts.chromosomes: remainingCTGs += " -r Y"
            if "MT" in opts.chromosomes: remainingCTGs += " -r MT"
        command_line = 'if [ ${} -eq 1 ]; then\n\tfreebayes'.format(roadrunner.jobCores) + ' {} -f {} -p {} --use-best-n-alleles {} '.format(flag, opts.ref_genome_path, opts.ploidy, int(opts.ploidy)) + remainingCTGs + ' $reads | bgzip -c > ./VCALL/$sample/$sample.otherCtgs.raw.g.vcf.gz\n'
        command_line += 'else\n\t freebayes-parallel <(fasta_generate_regions.py {}.fai 5000) ${} {} {} --use-best-n-alleles {} -f {} -p {} $reads | bgzip -c > ./VCALL/$sample/$sample.diploid.raw.vcf.gz\nfi\n'.format(opts.ref_genome_path, roadrunner.jobCores, flag, remainingCTGs, int(opts.ploidy), opts.ref_genome_path, opts.ploidy)
        # Identify variants for provided sample.
        outf.write('{}\n'.format(' '.join(command_line)))

    outf.close()


    # Writing file list.
    print 'Variant call script ready.'
    outlist = open('./LISTS/vcalledlist.txt', 'w')
    for sample in samples:
        if not os.path.exists(os.path.join('./VCALL/', sample)): os.mkdir('./VCALL/' + sample)
        outlist.write('%s\t%s\n' % (sample, ('./VCALL/'+sample+'/'+sample+'.diploid.raw.vcf.gz')))
    outlist.close()

    # Run or save qsub command line.
    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    cmd = 'qsub {} {}'.format(prname, filename)
    outcommand.write('%s\n' % (cmd))

    print 'Outputs from Variant Calling are listed in ./LISTS/vcalledlist.txt'
    return './LISTS/vcalledlist.txt'



def FBCall_sct(opts, samples, filename, outcommand, nstep, nsamples, roadrunner):

    # Define intervals first
    if opts.chunkSize == 0:
        opts.chunkSize = 10000000
    itvlist = intervals(opts)
    print("Created intervals.")
    nsets = sum([1 for itv in open("./LISTS/Intervals.txt")])
    nconcurrent = nsets
    if nsets > 150:
        nconcurrent = 150
    

    # Start create header for SH file.
    prname = "./SCRIPTS/" + nstep + '-freebayes_pipeline.sh'
    dependencies = ["java","freebayes", "gcc", "tabix"]
    h = Header(opts = opts)
    #h.processname("vcaller")
    pname = creates_pname("fb")
    h.processname(pname)
    h.setcores(opts.nthreads)
    minmemory_tot = 32
    h.setmemory(minmemory_tot/int(opts.nthreads))
    h.setnjobs(nsets)
    h.setconcurrency(nconcurrent)
    h.runtime(24)
    h.stderrSet("./LOGS/{}.$JOB_ID.$TASK_ID.err".format(pname))
    h.stdoutSet("./LOGS/{}.$JOB_ID.$TASK_ID.out".format(pname))
    h.holdjid(opts.wait + opts.waitad)
    opts.wait += pname + ","
    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 


    if opts.vcaller == "freebayes-m":
        flag = "--report-monomorphic"
    elif opts.vcaller == "freebayes-g":
        flag = "--gvcf"
    else:
        flag = ""


    #intervals = opts.ref_genome_path.replace('.fasta', '.intervals').replace('.fa', '.intervals').replace('.fna', '.intervals')
    # Define remaining steps.
    header += '''sample=$3\n'''
    header += '''ctg=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $1}' )\n'''
    header += '''bpi=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $2}' )\n'''
    header += '''bpe=$( sed "${} q;d"'''.format(roadrunner.jobArrayN) + ''' $1 | awk '{print $3}' )\n'''
    header += '''itv=${ctg}:${bpi}-${bpe}\n'''
    header += '''reads=$( grep -w $sample $2 | awk '{print $2}' )\n\n'''
    header += '''if [ ! -e ./VCALL ]; then mkdir ./VCALL; fi\n'''
    header += '''if [ ! -e ./VCALL/$sample ]; then mkdir ./VCALL/$sample; fi\n\n'''
    print 'Creating Variant calling script.'
    outf = open(prname, 'w')
    outf.write(header)

    if not os.path.exists('./VCALL/'): os.mkdir('./VCALL/')
    

    # Create command line for GATK HaplotypeCaller
    cmd = '''ploidy={}\n'''.format(opts.ploidy)
    if opts.sexcheck == "y":
        cmd += '''male=$(grep -w $sample ./LISTS/males.txt | wc -l | awk '{print $1}')\n\n'''
    else:
        cmd += '''male=0\n\n'''

    cmd += '''if [ $ctg == "Y" ] || [ $ctg == "MT" ] || [ $ctg == "y" ] || [ $ctg == "Mt" ] || [ $ctg == "mt" ]; then ploidy=$((ploidy / 2));fi\n'''
    cmd += '''if [ $ctg == "X" ] && [ $male == 1 ]; then ploidy=$((ploidy / 2));fi\n'''
    cmd += '''if [ $ploidy -lt 1 ]; then ploidy=1;fi\n'''

    cmd += 'freebayes {0} --use-best-n-alleles $ploidy -f {1} -p $ploidy -r $itv $reads | bgzip -c > ./VCALL/$sample/$sample.$ctg.$bpi.$bpe.raw.g.vcf.gz && tabix -p vcf ./VCALL/$sample/$sample.$ctg.$bpi.$bpe.raw.g.vcf.gz\n'.format(flag, opts.ref_genome_path)

    outf.write('{}\n'.format(cmd))
    outf.close()


    # Writing file list.
    print 'Variant call script ready.'
    outlist = open('./LISTS/calledsamples.txt', 'w')
    for sample in samples:
        if not os.path.exists(os.path.join('./VCALL/', sample)): os.mkdir('./VCALL/' + sample)
        outtmplist = open("./VCALL/{0}/{0}.gvcflist".format(sample), "w")
        for line in open(itvlist):
            ctg, bpi, bpe = line.strip().split()
            outtmplist.write('./VCALL/{0}/{0}.{1}.{2}.{3}.raw.g.vcf.gz\n'.format(sample, ctg, bpi, bpe))
        outtmplist.close()
        outlist.write("{0}\t./VCALL/{0}/{0}.gvcflist\n".format(sample))
        roadrunner.parse('./LISTS/Intervals.txt', filename, sample, script_name=prname, jobname=pname)
        roadrunner.saveCommand()
        outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))
    outlist.close()

    # Run or save qsub command line.

    print 'Outputs from Variant Calling are listed in ./LISTS/calledsamples.txt'
    return './LISTS/calledsamples.txt'

