# Define the pipelines called by bagpipe.py


# Graph WGS running method
def runGraphPipeline(opts, sample_dict, sample_list, filename, roadrunner, outcommand, nstep, nsamples, prgFolder):
    if opts.ploidy != 1 and opts.ploidy != 2:
        import sys
        sys.exit("ERROR! Graph pipeline only support 1/2 as ploidies.\nUse a linear genome pipeline instead.\n")

    # Get graph-specific libraries
    from lib.wgslib.AlignmentMethods import sumdepth
    from graph.GraphModules import graphConstruct, joinPEfq, graphMap, mergeGam
    from graph.GraphModules import gatherVCFsChrom, gatherVCFsSize, prefilterGam, graphAugmentChrom, graphAugmentSize
    from graph.GraphModules import graphVariantChrom, graphVariantSize, chunkGamChrom, chunkGamSize
    from graph.GraphModules import graphAugment_all, graphVariant_all, chunkDepths
    from graph.GraphModules import GraphAugmentedIndex, GraphPack, GraphAugSnarls
    # Generate graph genome
    mygraph = graphConstruct(opts, next(nstep), filename, outcommand, roadrunner)

    # Generate interleaved reads
    if opts.interleave or opts.aligner.lower() == "graphaligner": 
        filename = joinPEfq(opts, sample_dict, filename, outcommand, next(nstep), roadrunner)

    # Map reads to graph genome
    filename = graphMap(opts, sample_dict, mygraph, filename, outcommand, next(nstep), roadrunner)

    # Combine GAM files
    if len([sample for sample in sample_list if len(sample_dict[sample]) > 1]) != 0:
        filename = mergeGam(opts, sample_list, sample_dict, filename, outcommand, next(nstep), nsamples, roadrunner)

    # Pre-filter gam files
    filename = prefilterGam(opts, sample_list, sample_dict, mygraph, filename, outcommand, next(nstep), nsamples, roadrunner)

    if opts.chunking == 1:
        # Chunking GAM and graph files
        if opts.chunkSize==0:
            filename = chunkGamChrom(opts, sample_list, sample_dict, mygraph, filename, outcommand, next(nstep), nsamples, roadrunner)
            chunkDepths(opts, sample_list, sample_dict, mygraph, filename, outcommand, next(nstep), nsamples, roadrunner)
            sumdepth(opts, outcommand, next(nstep), prgFolder, roadrunner)
            commands = graphAugmentChrom(opts, sample_list, filename, mygraph, outcommand, next(nstep), roadrunner)
            commands = graphVariantChrom(commands, opts, sample_list, filename, outcommand, next(nstep), roadrunner)
            # Writing sample-locked callers.
            for sample in sample_list:
                outcommand.write('{}\n'.format( commands[sample]["augment"][1] ))
                outcommand.write('{}\n'.format( commands[sample]["call"][1] ))
            gatherVCFsChrom(commands, opts, sample_dict, filename, outcommand, next(nstep), roadrunner)
        else:
            filename = chunkGamSize(opts, sample_list, sample_dict, mygraph, filename, outcommand, next(nstep), nsamples, roadrunner)
            chunkDepths(opts, sample_list, sample_dict, mygraph, filename, outcommand, next(nstep), nsamples, roadrunner)
            sumdepth(opts, outcommand, next(nstep), prgFolder, roadrunner)
            commands = graphAugmentSize(opts, sample_list, filename, mygraph, outcommand, next(nstep), roadrunner)
            commands = graphVariantSize(commands, opts, sample_list, filename, outcommand, next(nstep), roadrunner)
            # Writing sample-locked callers.
            for sample in sample_list:
                outcommand.write('{}\n'.format( commands[sample]["augment"][1] ))
                outcommand.write('{}\n'.format( commands[sample]["call"][1] ))
            gatherVCFsSize(commands, opts, sample_dict, filename, outcommand, next(nstep), roadrunner)

    else:
        # Do graph augmentation
        graphAugment_all(opts, sample_dict, filename, mygraph, outcommand, next(nstep), roadrunner)
        # Augmented indexing
        GraphAugmentedIndex(opts, sample_dict, filename, mygraph, outcommand, next(nstep), roadrunner)
        # Augmented packing
        GraphPack(opts, sample_dict, filename, mygraph, outcommand, next(nstep), roadrunner)
        # Augmented indexing
        GraphAugSnarls(opts, sample_dict, filename, mygraph, outcommand, next(nstep), roadrunner)
        # Call variants from augmented graph
        graphVariant_all(opts, sample_dict, filename, outcommand, next(nstep), roadrunner)
    

    print "Completed graph variant call."
    return 0


# ATAC-seq running method
def runAtacPipeline(opts, sample_dict, sample_list, filename, roadrunner, outcommand, nstep, nsamples):
    print("Creating ATAC-seq pipeline.")
    from lib.epilib.ATAC import index_refs_ATAC, trimgalore_ATAC, FastQC_ATAC
    from lib.epilib.ATAC import AlignATAC, merge_reads_ATAC, Genrich

    # Trim data
    filename = trimgalore_ATAC(opts, sample_list, sample_dict, filename, outcommand, next(nstep), nsamples, roadrunner)   

    # Run FastQC
    FastQC_ATAC(opts, sample_list, sample_dict, filename, outcommand, next(nstep), nsamples, roadrunner)         

    # Check indexed reference genome.
    index_refs_ATAC(opts, outcommand, nstep, roadrunner)

    # Run Bowtie2  alignment
    filename = AlignATAC(opts, sample_dict, filename, outcommand, next(nstep), roadrunner)

    # Merge reads
    if len([sample for sample in sample_list if len(sample_dict[sample]) > 1]) != 0:
        filename = merge_reads_ATAC(opts, sample_list, sample_dict, filename, outcommand, next(nstep), nsamples, roadrunner)

    # Run Genrich
    Genrich(opts, sample_dict, filename, outcommand, next(nstep), roadrunner)
    
    print "Completed ATAC-seq pipeline."
    return 0


def runRRBSpipeline(opts, sample_dict, sample_list, filename, roadrunner, outcommand, nstep, nsamples):
    print("Creating RRBS pipeline.")
    from lib.epilib.RRBS import index_refs_RRBS, trimgalore_RRBS, FastQC_RRBS
    from lib.epilib.RRBS import AlignRRBS, merge_reads_RRBS, MethylXtract, RRBScoverage

    # Run FastQC
    FastQC_RRBS(opts, sample_list, sample_dict, filename, outcommand, next(nstep), nsamples, roadrunner)      

    # Trim data
    filename = trimgalore_RRBS(opts, sample_list, sample_dict, filename, outcommand, next(nstep), nsamples, roadrunner)      

    # Check indexed reference genome.
    index_refs_RRBS(opts, outcommand, nstep, roadrunner)

    # Run Bowtie2  alignment
    filename = AlignRRBS(opts, sample_dict, filename, outcommand, next(nstep), roadrunner)

    # Merge reads
    if len([sample for sample in sample_list if len(sample_dict[sample]) > 1]) != 0:
        filename = merge_reads_RRBS(opts, sample_list, sample_dict, filename, outcommand, next(nstep), nsamples, roadrunner)

    # Calculate genome coverage
    RRBScoverage(opts, sample_list, filename, outcommand, next(nstep), roadrunner)

    # Run Genrich
    MethylXtract(opts, sample_dict, filename, outcommand, next(nstep), roadrunner)
    
    print "Completed RRBS-seq pipeline."
    return 0




# ATAC-seq running method
def runRnaPipeline(opts, sample_dict, sample_list, filename, roadrunner, outcommand, nstep, nsamples):
    print("Creating RNA-seq pipeline.")
    from lib.epilib.RNA import index_refs_RNA, trimgalore_RNA, FastQC_RNA
    from lib.epilib.RNA import AlignRNA, merge_reads_RNA, Stringtie

    # Trim data
    filename = trimgalore_RNA(opts, sample_list, sample_dict, filename, outcommand, next(nstep), nsamples, roadrunner)   

    # Run FastQC
    FastQC_RNA(opts, sample_list, sample_dict, filename, outcommand, next(nstep), nsamples, roadrunner)         

    # Check indexed reference genome.
    index_refs_RNA(opts, outcommand, nstep, roadrunner)

    # Run Bowtie2  alignment
    filename = AlignRNA(opts, sample_dict, filename, outcommand, next(nstep), roadrunner)

    # Merge reads
    if len([sample for sample in sample_list if len(sample_dict[sample]) > 1]) != 0:
        filename = merge_reads_RNA(opts, sample_list, sample_dict, filename, outcommand, next(nstep), nsamples, roadrunner)

    # Run Genrich
    Stringtie(opts, sample_dict, filename, outcommand, next(nstep), roadrunner)
    
    print "Completed ATAC-seq pipeline."
    return 0




def runWGSpipeline(opts, sample_dict, sample_list, filename, roadrunner, outcommand, nstep, nsamples, prgFolder):
    #############################
    ####  Process raw fastq  ####
    #############################
    from Utilities import areFile
    from lib.reflib.ReferencesIndexingMethods import index_refs
    from lib.rawlib.StageOut import stagingOut
    from lib.rawlib.FastQpreprocess import FastQC, trimgalore, Trimmomatic, PigZ
    from lib.wgslib.AlignmentMethods import BWAlignment, Mmap2Alignment, merge_reads, process_reads
    from lib.wgslib.AlignmentMethods import MarkDup, BQSR, sumalign, sumdepth
    from lib.callerlib.Freebayes import FBCall_sct, FreebayesCall
    from lib.callerlib.HaplotypeCaller import HCall, HCall_sct
    from lib.callerlib.VariantMethods import gatherVCFs, gatherGVCFs
    from lib.callerlib.SVcallers import GRIDSS, delly

    # Check indexed reference genome.
    index_refs(opts, outcommand, nstep, roadrunner)
    
    # FastQC analysis on all reads
    if opts.fqc == 'y':
        FastQC(opts, sample_list, sample_dict, filename, outcommand, nstep, nsamples, roadrunner)


    # Parallel gzip with PigZ
    if opts.PigZ == 'y': 
        filename = PigZ(opts, sample_list, next(nstep), filename, outcommand, nsamples, roadrunner)


    # Trimmomatic.
    if opts.trimmomatic == 'y': 
        filename = Trimmomatic(opts, sample_list, sample_dict, filename, outcommand, next(nstep), nsamples, roadrunner)
        if opts.PigZ == 'y': 
            filename = PigZ(opts, sample_list, next(nstep), filename, outcommand, nsamples, roadrunner)
    if opts.trimgalore == 'y': 
        filename = trimgalore(opts, sample_list, sample_dict, filename, outcommand, next(nstep), nsamples, roadrunner)        
        if opts.PigZ == 'y': 
            filename = PigZ(opts, sample_list, next(nstep), filename, outcommand, nsamples, roadrunner)



    #############################
    ####  Align and process  ####
    #############################

    # BWA alignment.
    if opts.aligner.lower() == "bwa":
        filename = BWAlignment(opts, sample_dict, filename, outcommand, next(nstep), roadrunner)
    else:
        filename = Mmap2Alignment(opts, sample_dict, filename, outcommand, next(nstep), roadrunner)


    # Reads processing.
    filename = process_reads(opts, sample_list, sample_dict, filename, outcommand, next(nstep), nsamples, roadrunner)


    # Merge reads
    if len([sample for sample in sample_list if len(sample_dict[sample]) > 1]) != 0:
        filename = merge_reads(opts, sample_list, sample_dict, filename, outcommand, next(nstep), nsamples, roadrunner)


    # Dedup. 
    filename = MarkDup(opts, sample_list, sample_dict, filename, outcommand, next(nstep), nsamples, prgFolder, roadrunner)


    # Base Quality ReCalibration.
    if opts.BQSR == 'y' and areFile(opts.dbsnpf):
        filename = BQSR(opts, sample_list, filename, outcommand, next(nstep), nsamples, roadrunner)


    # Summarize alignment statistics.
    sumalign(opts, outcommand, next(nstep), prgFolder, roadrunner)


    # Define the read depth
    sumdepth(opts, outcommand, next(nstep), prgFolder, roadrunner)


    # Call SV using GRIDSSV
    if opts.getsv == 'y':
        if opts.gridss != "":
            GRIDSS(opts, sample_list, filename, outcommand, next(nstep), nsamples, roadrunner)
        if  opts.delly == "y" or opts.delly == 1 or opts.delly == True or opts.delly == "1":
            delly(opts, sample_list, filename, outcommand, next(nstep), nsamples, roadrunner)


    # Stageout if requested.
    if 1 in opts.stageoutstep and opts.stageoutpath is not None:
        stagingOut(1, outcommand, next(nstep), nsamples, opts, roadrunner)


    #############################
    #####   Call variants   #####
    #############################

    # Variant calling.
    if opts.vcaller.lower() == "haplotypecaller":
        # Run HaplotypeCaller
        if not opts.chunking:
            filename = HCall(opts, sample_list, filename, outcommand, next(nstep), nsamples, roadrunner)
        else:
            # Scattered GVCF calling
            filename = HCall_sct(opts, sample_list, filename, outcommand, next(nstep), nsamples, roadrunner)
            # Gather all GVCFs into one GVCFs per sample
            filename = gatherGVCFs(sample_list, filename, outcommand, next(nstep), opts, roadrunner)
            
    elif "freebayes" in opts.vcaller.lower():
        # freebayes variant caller
        if not opts.chunking:
            filename = FreebayesCall(opts, sample_list, filename, outcommand, next(nstep), nsamples, roadrunner)
        else:
            # Scattered GVCF calling
            filename = FBCall_sct(opts, sample_list, filename, outcommand, next(nstep), nsamples, roadrunner)
            # Gather all GVCFs into one GVCFs per sample
            if opts.vcaller.lower() == "freebayes-g":
                filename = gatherGVCFs(sample_list, filename, outcommand, next(nstep), opts, roadrunner)
            else:
                filename = gatherVCFs(sample_list, filename, outcommand, next(nstep), opts, roadrunner)

    # Stageout if requested.
    if 2 in opts.stageoutstep and opts.stageoutpath is not None:
        stagingOut(2, outcommand, next(nstep), nsamples, opts, roadrunner)
    return 0


# Graph ATAC running method
def runAtacGraphPipeline(opts, sample_dict, sample_list, filename, roadrunner, outcommand, nstep, nsamples):
    if opts.ploidy != 1 and opts.ploidy != 2:
        import sys
        sys.exit("ERROR! Graph pipeline only support 1/2 as ploidies.\nUse a linear genome pipeline instead.\n")

    # Get graph-specific libraries
    from graph.GraphModules import graphConstruct, joinPEfq, graphMap, mergeGam
    from graph.GraphModules import gatherVCFsChrom, gatherVCFsSize, prefilterGam
    from graph.GraphATAC import graphVgJson, graphPeaks, graphMapJson, chunkGamATAC
    from lib.epilib.ATAC import trimgalore_ATAC, FastQC_ATAC

    # Generate graph genome
    mygraph = graphConstruct(opts, next(nstep), filename, outcommand, roadrunner)

    # Trim data
    filename = trimgalore_ATAC(opts, sample_list, sample_dict, filename, outcommand, next(nstep), nsamples, roadrunner)   

    # Map reads to graph genome
    filename = graphMap(opts, sample_dict, mygraph, filename, outcommand, next(nstep), roadrunner)

    # Combine GAM files
    if len([sample for sample in sample_list if len(sample_dict[sample]) > 1]) != 0:
        filename = mergeGam(opts, sample_list, sample_dict, filename, outcommand, next(nstep), nsamples, roadrunner)

    # Pre-filter gam files
    filename = prefilterGam(opts, sample_list, sample_dict, mygraph, filename, outcommand, next(nstep), nsamples, roadrunner, isatac=True)


    if opts.chunking == 1:
        # Chunking GAM and graph files
        # Chunk graph and gam
        filename = chunkGamATAC(opts, sample_list, sample_dict, mygraph, filename, outcommand, next(nstep), nsamples, roadrunner)

        # Converting VG and Gam to Json
        # graphVgJson(opts, sample_list, sample_dict, mygraph, filename, outcommand, next(nstep), nsamples, roadrunner)
        # graphMapJson(opts, sample_list, sample_dict, mygraph, filename, outcommand, next(nstep), nsamples, roadrunner)

        # Do graph augmentation
        commands = graphPeaks(opts, sample_list, sample_dict, mygraph, filename, outcommand, next(nstep), nsamples, roadrunner)
        for sample in sample_list:
            outcommand.write('{}\n'.format( commands[sample]["peakcaller"][1] ))
    else:
        # Converting VG and Gam to Json
        # graphVgJson(opts, sample_list, sample_dict, mygraph, filename, outcommand, next(nstep), nsamples, roadrunner)
        graphMapJson(opts, sample_list, sample_dict, mygraph, filename, outcommand, next(nstep), nsamples, roadrunner)

        # Do graph augmentation
        commands = graphPeaks(opts, sample_list, sample_dict, mygraph, filename, outcommand, next(nstep), nsamples, roadrunner)
        for sample in sample_list:
            outcommand.write('{}\n'.format( commands[sample]["peakcaller"][1] ))


    print "Completed graph variant call."
    return 0

