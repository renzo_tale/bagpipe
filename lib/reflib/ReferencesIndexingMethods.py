########################################################
# This file contains the method to index references    #
# genome and VCF files with known variants             #
########################################################


import os
from os.path import isfile as Isfile
from numpy import repeat as rp
from lib.Utilities import Header
from lib.Utilities import creates_pname



# Function to index the reference files (genome and vcf of variants).
def index_refs(opts, outcommand, steps, roadrunner):

    # Start create header for SH file.
    dependencies = ["java", "bwa", "samtools", "picard", "gatk"]
    h = Header(opts)
    prname = "./SCRIPTS/" + next(steps) + "-Indexer.sh"
    pname = creates_pname("gi")
    h.processname(pname)
    h.setcores(1)
    minmemory_tot = 32
    h.setmemory(minmemory_tot/int(opts.nthreads))
    h.runtime(18)
    h.stderrSet("./LOGS/{}.err".format(pname))
    h.stdoutSet("./LOGS/{}.out".format(pname))
    h.setprj(opts.prj)
    #opts.wait = "genomeindex"
    h.addmailaddr(opts.mailaddr)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 

    # BWA indexing
    createB = False
    createP = False
    createS = False
    createI = False
    createD = False


    if Isfile(opts.ref_genome_path + '.amb') == False or\
        Isfile(opts.ref_genome_path + '.ann') == False or\
        Isfile(opts.ref_genome_path + '.bwt') == False or\
        Isfile(opts.ref_genome_path + '.pac') == False or\
        Isfile(opts.ref_genome_path + '.sa') == False: 
        
        createB = True
  

    # Picard indexing
    if not os.path.isfile(opts.ref_genome_path.replace('.fasta',".dict").replace('.fa','.dict')) or not os.path.isfile(opts.ref_genome_path.replace('.fasta',".intervals").replace('.fa','.intervals')):
        createP = True

    # Samtools indexing        
    if os.path.isfile(opts.ref_genome_path + '.fai') == False:
        createS = True
       

    # DBsnp indexing
    if opts.dbsnpf is not None and opts.dbsnpf != "":
        dbsnpf = opts.dbsnpf.split(',')
        dbsnpindexer = ''
        for f in dbsnpf:
            if not os.path.isfile(f + '.tbi'):
                createD = True
                dbsnpindexer += ' '.join(['gatk --java-options "-Xmx' + str(minmemory_tot -  6) + 'g"',
                                        'IndexFeatureFile', 
                                        '-F', f, opts.GATKoptions.get("IndexFeatureFile", ""), '\n'])
        

    # indels indexing
    if opts.indelsf is not None and opts.indelsf != "":
        indelsf = opts.indelsf.split(',')
        indelindexer = ''
        for f in indelsf:
            if not os.path.isfile(f + '.tbi'):
                createI = True
                indelindexer += ' '.join(['gatk --java-options "-Xmx' + str(minmemory_tot -  6) + 'g"',
                                        'IndexFeaturesFile', 
                                        '-F', f, opts.GATKoptions.get("IndexFeatureFile", ""), '\n'])
                

    if createB or createP or createS:
        opts.wait += pname + ","
        outf = open(prname,'w')
        outf.write(header)
        if createS: outf.write('%s\n' % ('samtools faidx ' + opts.ref_genome_path))
        if createB: outf.write('%s\n' % ('bwa index -a bwtsw ' + opts.ref_genome_path))
        if createP: 
            outf.write('%s\n' % ' '.join(['gatk --java-options "-Xmx' + str(minmemory_tot -  6) + 'g"',
                                        'CreateSequenceDictionary -R', opts.ref_genome_path, 
                                        '-O', opts.ref_genome_path.replace('.fasta', '.dict').replace('.fa', '.dict'), 
                                        opts.GATKoptions.get("CreateSequenceDictionary", "")]))
            outf.write('%s\n' % ' '.join(['gatk --java-options "-Xmx' + str(minmemory_tot -  6) + 'g"',
                                        'ScatterIntervalsByNs -R', opts.ref_genome_path, "-OT BOTH", 
                                        '-O', opts.ref_genome_path.replace('.fasta', '.intervals').replace('.fa', '.intervals'), 
                                        opts.GATKoptions.get("CreateSequenceDictionary", "")]))
        if createD: outf.write('%s\n' % dbsnpindexer)
        if createI: outf.write('%s\n' % indelindexer)
        roadrunner.parse(script_name=prname, jobname=pname)
        roadrunner.saveCommand()
        cmd = "qsub %s"
        outcommand.write('%s\n' % cmd%prname)

    return None


