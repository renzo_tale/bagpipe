########################################################
# This file contains the master of puppets method,     #
# that allows interactive submission and creation      #
# of scripts.                                          #
########################################################


class masterOfPuppets:
    import os
    from os.path import isfile as Isfile
    from Utilities import areFile, counter, settings
    from Utilities import autosubmit

    def __init__(self, roadrunner, options):
        self.commands = roadrunner.commandlist
        self.scriptNames = []
        self.inputfiles = []
        self.needAddition = []
        for command in self.commands:
            command = command.split()
            self.scriptNames.append(command[1])
            try:
                self.inputfiles.append(command[2])
                self.needAddition.append(True)
            except:
                self.inputfiles.append([""])
                self.needAddition.append(False)

        self.roadrunner = roadrunner
        self.roadrunner.reset()
        self.commandperformed = []
        self.fileprocessed = []
    
    def start(self):
        print "This is the master of puppets."
        print "With this, you will be able to run single commands interactively."
        print "What do you want to do?"
        for n, choice in enumerate(self.commands):
            print "{:02}) {}".format(n, choice)
        print "{:02}) {}".format(n + 1, "quit")
        choice = int(raw_input('\nMy choice (number): '))
        while choice != n+1:
            ok = False
            while not ok:
                
                if self.needAddition[choice] == False and self.commands[choice] not in self.commandperformed:
                    print "Submitting {}".format(self.needAddition[choice])
                    self.roadrunner.saveCommand('qsub {}'.format(self.commands[choice]))
                    self.roadrunner.run(1)
                    self.roadrunner.reset()
                    self.commandperformed.append(self.commands[choice])
                    continue
                elif self.needAddition[choice] == True and self.commands[choice] not in self.commandperformed:
                    fileChosen = ""
                    while fileChosen == "":
                        print "Which file to process?"
                        for n, choice in enumerate(self.inputfiles):
                            print "{:02}) {}".format(n, choice)
                        print "{:02}) {}".format(n + 1, "quit")
                        secondChoice = int(raw_input("Your choice: "))
                        try:
                            fileChosen = self.inputfiles[secondChoice]
                            if fileChosen in self.fileprocessed:
                                fileChosen = ""
                                print "File already processed."
                                continue
                            print "Submitting {}".format(self.needAddition[choice])
                            self.roadrunner.saveCommand('qsub {} {}'.format(self.commands[choice], fileChosen))
                            self.roadrunner.run(1)
                            self.roadrunner.reset()
                            self.commandperformed.append(self.commands[choice])
                            self.fileprocessed.append(fileChosen)
                            continue
                        except:
                            print "Not a valid choice."
                            fileChosen = ""
                for n, choice in enumerate(self.commands):
                    print "{:02}) {}".format(n, choice)
                try:
                    print "Perform: {}".format(self.commands[choice])
                    ok = True
                except:
                    print "Wrong choice; try again."
                    choice = raw_input('\nMy choice (number): ')
            if choice == (len(self.commands) - 1):
                print "Closing master of puppets."
            else:
                self.roadrunner.runSingle(choice)
        return 0
        

        
            