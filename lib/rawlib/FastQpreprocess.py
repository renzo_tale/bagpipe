########################################################
# This file contains all methods working on fastq data #
# including trimming, fastqc and pigz                  #
########################################################

import os 
from os.path import isfile as Isfile
from numpy import repeat as rp
from lib.Utilities import Header
from lib.Utilities import creates_pname


# Convert a bam to unmapped FastQ files
def bamToFastq(sample, bamfile, opts, roadrunner):
    import os
    if not os.path.exists('./SCRIPTS/bamToFq.sh'):
        outsh = open('./SCRIPTS/bamToFq.sh', "w")
        
        # Start create header for SH file.
        h = Header(opts)
        h.processname("b2fq")
        h.setcores(1)
        h.setmemory(32)
        h.runtime(48)
        h.stderrSet("./LOGS/${}.${}.err".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag) )
        h.stdoutSet("./LOGS/${}.${}.out".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag) )
        h.addmailaddr(None)
        h.setprj(opts.prj)
        dependencies = ["java", "gatk"]
        for i in dependencies:
            h.add_dependency(i)
        if opts.scatterSize == -1:
            sctSize = 100000000
        else:
            sctSize = opts.scatterSize
        header = h.get() 
        header += '''sample=$1\n'''
        header += '''reads=$2\n'''
        #header += '''gatk={}\n'''.format(opts.path_to_GATK)
        #header += '''reads=`python -c "import os, sys; print(' '.join(sorted([i for i in os.listdir(sys.argv[1]) if '.fq' in i or '.fastq' in i and '.md5' not in i])))" $path` \n'''
        header += '''rName=$(basename -s ".bam" $reads)\n'''
        header += '''rPath=$(dirname $reads)\n'''
    # Check whether apply scatter-gather approach to alignment
        header += '''\tgatk --java-options "-Xmx4G" SamToFastq -I ${reads} -F ${rPath}/${bamName}_R1.fq.gz --SECOND_END_FASTQ ${rPath}/${bamName}_R2.fq.gz\n'''
        
        outsh.write('%s\n' % header)
        outsh.close()

    # Start data staging of all files in a smaple folder and wait for it to end.
    roadrunner.parse(sample, ','.join(bamfile), script_name="./SCRIPTS/ScatterFq.sh", jobname="scatter")
    roadrunner.saveCommand()
    
    return [bamfile.replace(".bam", "_R1.fq.gz"), bamfile.replace(".bam", "_R2.fq.gz")]



# FastQC analysis
def FastQC(opts, samples, sample_dict, filename, outcommand, nstep, nsamples, roadrunner):

    prname = "./SCRIPTS/" + next(nstep) + '-FastQC_pipeline.sh'

    # Start create header for SH file.
    dependencies = ["java", "fastqc"]
    h = Header(opts)
    pname = creates_pname("fq")
    h.processname(pname)
    h.setcores(opts.nthreads)
    minmemory_tot = 32
    h.setnjobs(opts.nsamples)
    h.setconcurrency(opts.nprocesses)
    h.setmemory(minmemory_tot/int(opts.nthreads))
    h.runtime(6)
    h.addmailaddr(opts.mailaddr)
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    # opts.wait += pname + ","
    opts.waitad += pname + ","
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''sample=$( sed "${} q;d" $1 '''.format(roadrunner.jobArrayN) + '''| awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d" $1 '''.format(roadrunner.jobArrayN) + '''| awk '{print $2}' | tr ',' ' ' )\n\n'''
    header += '''if [ !  -e ./QC ]; then mkdir ./QC; fi\n'''
    header += '''if [ ! -e ./QC/$sample ]; then mkdir ./QC/$sample; fi\n'''


    try: os.mkdir('./QC/')
    except: 'Folder ./QC ready.'

    outf = open(prname, 'w')
    outf.write(header)
    
    outf.write('%s\n' % ' '.join(['fastqc $reads -o QC/$sample', '--threads ${}'.format(roadrunner.jobCores)]))
    outf.close()

    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()

    cmd = 'qsub {} {}'.format(prname, filename)
    outcommand.write('%s\n' % (cmd))
    print 'Done fastqc script.'



# PigZ fastq data to have faster access to the archive.
def PigZ(opts, samples, nstep, filename, outcommand, nsamples, roadrunner):
    prname = "./SCRIPTS/" + nstep + "-PigZ_pipeline.sh"

    outf = open(prname,'w')

    # Start create header for SH file.
    dependencies = ["java", "pigz"]
    h = Header(opts)
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    if "fq" in opts.wait:
        #h.processname("pigz1")
        #opts.wait = "pigz1"
        pname = creates_pname("p1")
        h.processname(pname)
        opts.wait += pname + ","
    elif opts.wait == "trimdata":
        #h.processname("pigz2")
        #opts.wait = "pigz2"
        pname = creates_pname("p2")
        h.processname(pname)
        opts.wait += pname + ","
    h.setcores(opts.nthreads)
    minmemory_tot = 24
    h.setnjobs(opts.nsamples)
    h.setconcurrency(opts.nprocesses)
    h.setmemory(minmemory_tot/int(opts.nthreads))
    h.runtime(24)
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    h.addmailaddr(opts.mailaddr)
    h.setprj(opts.prj)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''sample=$( sed "${} q;d" $1 '''.format(roadrunner.jobArrayN) + '''| awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d" $1 '''.format(roadrunner.jobArrayN) + '''| awk '{print $2}' | tr ',' ' ' )\n\n'''
    header += '''if [ !  -e ./QC ]; then mkdir ./QC; fi\n'''
    header += '''if [ ! -e ./QC/$sample ]; then mkdir ./QC/$sample; fi\n'''


    header += '''
    # Prepare data using pigz
    inputr=''
    for read in ${reads[@]}; do'''
    header += '''
        filepath=`dirname $read`
        readname=`basename $read .fastq.gz`
        ls $read | xargs zcat | pigz -p ${} >> $filepath"/"$readname"p.fastq.gz"
        if [ "$inputr" == '' ]; then
            inputr=$filepath"/"$readname"p.fastq.gz"
        else
            inputr=$inputr","$filepath"/"$readname"p.fastq.gz"
        fi
    done
    echo $sample $inputr >> pigz_reads.'''.replace('    ', "").format(roadrunner.jobCores )
    header += nstep + '.txt'

    outf.write(header)
    outf.close()

    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    cmd = 'qsub {} {}'.format(prname, filename)
    outcommand.write('%s\n' % (cmd))
    return 'pigz_reads.'+nstep+'.txt'
   


# Make trimmomatic submission script
def Trimmomatic(opts, samples, readDict, filename, outcommand, nstep, nsamples, roadrunner):

    prname = "./SCRIPTS/" + nstep + "-trimmomatic_pipeline.sh"

    # Start create header for SH file.
    dependencies = ["java", "pigz"]
    h = Header(opts)
    #h.processname("trimdata")
    pname = creates_pname("tm")
    h.processname(pname)
    h.setcores(opts.nthreads)
    minmemory_tot = 32
    h.setmemory(minmemory_tot/int(opts.nthreads))
    h.runtime(48)
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    h.setnjobs(opts.nsamples)
    h.setconcurrency(opts.nprocesses)
    h.holdjid(opts.wait)
    h.holdjid(opts.waitad)
    opts.waitad += pname + ","
    h.setprj(opts.prj)
    h.addmailaddr(opts.mailaddr)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get() 
    header += '''sample=$( sed "${} q;d" $1 '''.format(roadrunner.jobArrayN) + '''| awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d" $1 '''.format(roadrunner.jobArrayN) + '''| awk '{print $2}' | tr ',' ' ' )\n\n'''
    header += '''if [ !  -e ./QC ]; then mkdir ./QC; fi\n'''
    header += '''if [ ! -e ./QC/$sample ]; then mkdir ./QC/$sample; fi\n'''


    # Create main body of the sh script.
    if opts.ps == 'y': paired = 'PE'
    else: paired = 'SE'
    threads = '-threads ${}'.format(roadrunner.jobCores)

    if opts.adapter is not None and not Isfile(opts.adapter):
        print '''Can't trim adapters %s: files do not exists.''' %  opts.adapter
        return filename
    print 'Setting trimmomatic pipeline...'


    trimmomatic = 'java -jar ' + '-Xmx' + str(int(opts.nthreads) * 3) + "g" + ' ' + opts.path_to_trimmomatic

    
    outf = open(prname,'w')

    outf.write(header)
   
    
    INPUT_DEFINER='''# New loop to get read names.
    inputs=''
    outputs=''        
    for read in ${reads[@]}; do
        rname=`basename $read .fastq.gz`
        n1="./"$sample"/"$read
        echo $n1'''
    if opts.ps == 'y': 
        INPUT_DEFINER += '''
        n2="./TRIM/"$sample"/"$read".paired.fastq.gz"
        echo $n2
        n3="./TRIM/"$sample"/"$read".unpaired.fastq.gz"
        echo $n3
        inputs=$inputs" "$n1
        outputs=$outputs" "$n2" "$n3
    done'''
    else:
        INPUT_DEFINER += '''
        n2="./TRIM/"$sample"/"$rname".unpaired.fastq.gz"
        echo $n2
        inputs=$inputs" "$n1
        outputs=$outputs" "$n2" "$n3
    done'''

    outf.write('%s\n' % INPUT_DEFINER.replace('    ', ''))
    launcher_script = ' '.join([trimmomatic, paired, threads, '$inputs $outputs'])
    if opts.adapter is not None: launcher_script += ' ILLUMINACLIP:' + opts.adapter + ':2:30:10'
    launcher_script += ' SLIDINGWINDOW:4:10 MINLEN:36 LEADING:3 TRAILING:3'
    outf.write(launcher_script)

    outf.close()




    # Creating list of outputs for the alignment analysis.
    try: os.mkdir('TRIM')
    except: print 'Folder TRIM/ ready'
    trimmeddict = {}
    totrim = ''
    for sample in samples:
        # Making sample folder.
        try: os.mkdir(os.path.join('TRIM', sample))
        except: print 'Folder TRIM/' + sample + ' exists.'
        
        # Create input file names
        totrim = readDict[sample]
        # Define base names
        file_names = [os.path.basename(i).strip('.fastq.gz') for i in totrim]

        # Create output file names.
        trimmedlist = []
        if opts.ps == 'y':
            trimmedlist = [os.path.join('./TRIM/', sample, i + '.paired.fastq.gz') for i in rp(file_names, 2)]
        else:
            trimmedlist = [os.path.join('./TRIM/', sample, i + '.unpaired.fastq.gz') for i in rp(file_names, 2)]

        
        if opts.ps == 'y': trimmeddict[sample] = [i for i in trimmedlist if '.paired' in i]
        else: trimmeddict[sample] = trimmedlist
    

    outlist = open('./LISTS/trimmedlist.txt', 'w')
    for line in open(filename):
        sample, reads = line.strip().split()
        outlist.write('%s\t%s\n' % (sample, ','.join(trimmeddict[sample])))
    outlist.close()

    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    cmd = 'qsub {} {}'.format(prname, filename)
    outcommand.write('%s\n' % (cmd))
    print 'List of trimming outputs: ./LISTS/trimmedlist.txt'

    return './LISTS/trimmmedlist.txt'
  



# Make trimgalore submission script
def trimgalore(opts, samples, readDict, filename, outcommand, nstep, nsamples, roadrunner):

    prname = "./SCRIPTS/" + nstep + "-trimgalore.sh"
    
    # Start create header for SH file.
    if "trimgalore" in opts.customModules:
        dependencies = ["trimgalore"]
    else:
        dependencies = ["trimgalore", "anaconda", "CondaEnv"]
    h = Header(opts)
    #h.processname("trimdata")
    nlibraries = 0
    for sample in readDict:
        nlibraries += len(readDict[sample])
    pname = creates_pname("tg")
    h.processname(pname)
    h.setcores(opts.nthreadsAlignment)
    minmemory_tot = 24
    h.setmemory(minmemory_tot/int(opts.nthreadsAlignment))
    h.runtime(48)
    h.stderrSet("./LOGS/{}.${}.err".format(pname, roadrunner.taskIdFlag))
    h.stdoutSet("./LOGS/{}.${}.out".format(pname, roadrunner.taskIdFlag))
    h.setnjobs(nlibraries)
    h.setconcurrency(opts.nprocesses)
    h.holdjid(opts.wait)
    h.holdjidad(opts.waitad)
    h.setprj(opts.prj)
    opts.waitad += pname + ","
    h.addmailaddr(opts.mailaddr)
    for i in dependencies:
        h.add_dependency(i)
    header = h.get()
    header += '''\n''' 
    header += '''if [ ! -z $trimgalore ]; then export PATH=$PATH:$trimgalore; fi\n''' 
    header += '''sample=$( sed "${} q;d" $1 '''.format(roadrunner.jobArrayN) + '''| awk '{print $1}' )\n'''
    header += '''reads=$( sed "${} q;d" $1 '''.format(roadrunner.jobArrayN) + '''| awk '{print $2}' | tr ',' ' ' )\n\n'''
    header += '''flag=`python -c "import sys; print(['--paired' if len(sys.argv[1:]) > 1 or 'interleaved' in sys.argv[1] else ''][0])" $reads`\n'''
    header += '''rName=`python -c "import sys; print(sys.argv[1].replace('.interleaved', '').replace('.fq.gz', '.fastq.gz').replace('.fastq.gz', '').split('/')[-1] )" $reads`\n'''
    header += '''echo $rName\n'''
    header += '''if [ !  -e ./TRIM ]; then mkdir ./TRIM; fi\n'''
    header += '''if [ ! -e ./TRIM/$sample ]; then\n\tmkdir ./TRIM/$sample;\nfi\n'''
    header += '''if [ `trim_galore --version | awk ' $1 == "version" {print $2}' | python -c 'import sys; print(sys.stdin.readline() >= "0.6.0")' ` == "True" ]; then\n'''
    header += '''\ttrim_galore --cores ${} -q 10 --length 38 --trim-n --gzip --illumina -o ./TRIM/$sample $flag $reads\n'''.format(roadrunner.jobCores)
    header += '''else\n'''
    header += '''\ttrim_galore -q 10 --length 38 --trim-n --gzip --illumina -o ./TRIM/$sample $flag $reads\n'''
    header += '''fi\n'''
    outf = open(prname,'w')
    outf.write(header)
   
    print 'Setting trimmomatic pipeline...'    

    outf.close()




    # Creating list of outputs for the alignment analysis.
    try: os.mkdir('TRIM')
    except: print 'Folder TRIM/ ready'
    outlist = open('./LISTS/trimmedlist.txt', 'w')
    for line in open(filename):
        sample, reads = line.strip().split()
        # Making sample folder.
        try: os.mkdir(os.path.join('TRIM', sample))
        except: print 'Folder TRIM/' + sample + ' exists.'
        
        # Create input file names
        for lista in readDict[sample]:
            # Define base names
            file_names = [os.path.join("./TRIM", sample, i.split("/")[-1].replace('1.fq.gz', "1_val_1.fq.gz").replace('2.fq.gz', "2_val_2.fq.gz").replace('1.fastq.gz', "1_val_1.fq.gz").replace('2.fastq.gz', "2_val_2.fq.gz")) for i in lista]    
            outlist.write('%s\t%s\n' % (sample, ','.join(file_names)))
    outlist.close()

    roadrunner.parse(filename, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('{}\n'.format(roadrunner.commandlist[-1]))
    print 'List of trimming outputs: ./LISTS/trimmedlist.txt'

    return './LISTS/trimmedlist.txt'



# Pool multiple FastQ from  different libraries into a single library.
def combineFastQ(sample_dict):
    import os
    import subprocess as sbp
    for sample in sample_dict:
        PEreads1 = []
        PEreads2 = []
        SEreads = []
        for read in sample_dict[sample]:
            if len(read) > 1:
                PEreads1.append(read[0])
                PEreads2.append(read[1])
                continue
            SEreads.append(read[0])
        if len(PEreads1) + len(SEreads) > 1:
            print("Joining reads for {}".format(sample))
            PEsize = sum([os.stat(f).st_size for f in PEreads1]) + sum([os.stat(f).st_size for f in PEreads2])
            SEsize = sum([os.stat(f).st_size for f in SEreads])
            if SEsize > PEsize:
                for f in SEreads:
                    catZcat = "cat"
                    if ".gz" in f: catZcat = "zcat"
                    p = sbp.Popen("{} {} | bgzip -c >> {}.fq.gz".format(catZcat, f, sample))
                    p.wait()
                    p = sbp.Popen("rm {}".format(f))
                    p.wait()
                sample_dict[sample] = ["{}.fq.gz".format(sample)]
            else:
                for n, f1 in enumerate(PEreads1):
                    f2 = PEreads2[n]
                    catZcat = "cat"
                    if ".gz" in f1: catZcat = "zcat"
                    p = sbp.Popen("{} {} | bgzip -c >> {}_1.fq.gz".format(catZcat, f1, sample))
                    p.wait()
                    p = sbp.Popen("rm {}".format(f1))
                    p.wait()
                    p = sbp.Popen("{} {} | bgzip -c >> {}_2.fq.gz".format(catZcat, f2, sample))
                    p.wait()
                    p = sbp.Popen("rm {}".format(f2))
                    p.wait()
                sample_dict[sample] = ["{}_1.fq.gz".format(sample), "{}_2.fq.gz".format(sample)]
    return 0