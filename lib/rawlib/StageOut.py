########################################################
# This file contains all methods to stage data  out    #
########################################################


from lib.Utilities import Header
from lib.Utilities import creates_pname
import time
import os
import sys



# Datastore staging
def stagingOut(step, outcommand, nstep, nsamples, opts, roadrunner):

    steps = ['BQSR', 'VCALL', 'VPPROCESS']

    step = steps[step - 1]
    if step == "BQSR":
        fname = "./LISTS/recalibratedlist.txt"
    elif step == "VCALL":
        fname = "./LISTS/vcalledlist.txt"
    else:
        fname = "*.validated.annotated.vcf"

    prname = ''.join(["./SCRIPTS/",nstep,'-StagingOut.sh'])
    outsh = open(prname, 'w')
    
    # Start create header for SH file.
    #dependencies = ["pigz", "sratoolkit", "aspera"]
    h = Header(opts)
    #h.processname("stageout" + str(step))
    pname = creates_pname("s"+str(step))
    h.processname(pname)
    h.setcores(1)
    h.setmemory(2)
    h.runtime(12)
    h.stderrSet("./LOGS/{}.$TASK_ID.err".format(prname))
    h.stdoutSet("./LOGS/{}.$TASK_ID.out".format(prname))
    h.addmailaddr(opts.mailaddr)
    h.holdjid(opts.wait)
    h.setconcurrency(opts.nprocesses)
    opts.wait += pname + ","
    h.setprj(opts.prj)
    #opts.wait = "stageout" + str(step)
    h.addOpts("#$ -q staging")
    h.addOpts("#$ -r y")
    h.addOpts("#$ -notify")

    #for i in dependencies:
    #    h.add_dependency(i)
    header = h.get() 
    header += '''stepname=` basename $1 `'''

    header += '''
    if [ ! -e $2/$stepname ]; then mkdir $2/$stepname; fi
    cp -rf $stepname/* $2/$stepname/
    
    echo "Copied data from:" $stepname/ "to" $2/$stepname'''.replace('    ', '')
    outsh.write('%s\n' % header)
    outsh.close()

    roadrunner.parse(step, fname, opts.stageoutpath, script_name=prname, jobname=pname)
    roadrunner.saveCommand()
    outcommand.write('%s\n' % ' '.join(['qsub', prname, step, fname, opts.stageoutpath]))
    return 0


