
from lib.Utilities import Header
from lib.Utilities import creates_pname
import time
import os
import sys


# Remote files downloading/staging.
# SRA download
def SRA_DWNLD(sample, prj, opts, roadrunner):
    if not os.path.exists("./SRA"): os.mkdir("./SRA")
    if not os.path.exists("./SRA/"+sample): os.mkdir("./SRA/"+sample)
    if not os.path.exists('./SCRIPTS/SRAdownload.sh'):
        outsh = open('./SCRIPTS/SRAdownload.sh', 'w')

        # Start create header for SH file.
        dependencies = ["pigz", "sratoolkit", "aspera", "java", "gatk"]
        h = Header(opts)
        h.processname("sradwld")
        h.setcores(1)
        h.setmemory(32)
        h.runtime(47)
        h.stderrSet("./LOGS/${}.${}.err".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag))
        h.stdoutSet("./LOGS/${}.${}.out".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag))
        h.addmailaddr(opts.mailaddr)
        h.setprj(opts.prj)
        for i in dependencies:
            h.add_dependency(i)
        header = h.get() 
        if not os.path.exists("./SRA"): os.mkdir("./SRA")
        if not os.path.exists("./SRA/"+sample): os.mkdir("./SRA/"+sample)

        # Add remaining operations for the script
        header += '''sample=$1\n'''
        header += '''read=$2\n\n'''
        #header += '''gatk={}\n'''.format(opts.path_to_GATK)
        header += '''file=${read}.sra\n'''
        header += '''cd ./SRA/${sample}\n'''
        header += '''if [ -e ./SRA/${sample}/done ]; then echo "File already downloaded."; exit 0; fi\n'''
        #header += '''wget ftp-trace.ncbi.nlm.nih.gov:/sra/sra-instant/reads/ByRun/sra/${file:0:3}/${file:0:6}/${file%.sra}/${file} --no-remove-listing -P ./SRA/$sample\n'''
        header += '''FTPUSER='anonymous'\nFTPPASSWD='mymail@myaddress.uk'\n\nftp -in ftp-trace.ncbi.nlm.nih.gov <<END_SCRIPT\nuser $FTPUSER $FTPPASSWD\nbin\ncd /sra/sra-instant/reads/ByRun/sra/${file:0:3}/${file:0:6}/${file%.sra}/\nget ${file}\nquit\nEND_SCRIPT\necho "Bye."\n\n'''.replace('    ', '')
        header += '''cd ../../\n'''
        header += '''if [[ ! -e ${file%.sra}.aspx && ! -e ${file%.sra}.fastq.gz ]]; then
            echo -n "  Extracting data into FASTQ format ... "
            fastq-dump --split-files --outdir ./SRA/${sample} --gzip ./SRA/${sample}/${file} && rm ./SRA/${sample}/${file}
            echo "DONE"
        else
            echo "Skipping"
        fi\n'''.replace('    ', '')
        header += '''if [ ! -e ./SRA/${sample}/${file} ]; then\n\ttouch ./SRA/${sample}/done\nfi'''
        outsh.write('%s\n' % header)
        outsh.close()


    # Start data staging of all files in a smaple folder and wait for it to end.
    roadrunner.parse(sample, prj, script_name="./SCRIPTS/SRAdownload.sh", jobname="sradwnld")
    roadrunner.saveCommand()

    return 0


# ENA download
def ENA_DWNLD(sample, prj, opts, roadrunner):
    if not os.path.exists("./ENA"): os.mkdir("./ENA")
    if not os.path.exists("./ENA/"+sample): os.mkdir("./ENA/"+sample)
    if not os.path.exists('./SCRIPTS/ENAdownload.sh'):
        outsh = open('./SCRIPTS/ENAdownload.sh', 'w')

        # Start create header for SH file.
        h = Header(opts)
        h.processname("enadwld")
        h.setcores(1)
        h.setmemory(32)
        h.runtime(95)
        h.stderrSet("./LOGS/${}.${}.err".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag) )
        h.stdoutSet("./LOGS/${}.${}.out".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag) )
        h.addmailaddr(opts.mailaddr)
        h.setprj(opts.prj)
        dependencies = ["java", "gatk"]
        for i in dependencies:
            h.add_dependency(i)
        header = h.get() 
        header += '''sample=$1\n'''
        header += '''accession=$2\n'''
        #header += '''gatk={}\n'''.format(opts.path_to_GATK)
        header += '''echo $sample\n'''
        header += '''echo $accession\n'''
        header += '''code1=${accession:0:6}\n'''
        header += '''code2=$(printf "%03d" ${accession: -1})\n\n\n'''
        #header += '''if [ ${#accession} -eq 9 ]; then\necho "wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/${code1}/${accession}/*.fastq.gz --no-remove-listing -P ./ENA/${sample}/"\nwget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/${code1}/${accession}/*.fastq.gz --no-remove-listing -P ./ENA/${sample}/;\nfi\n'''.replace('    ', '')
        #header += '''if [ ${#accession} -eq 10 ]; then\necho "wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/${code1}/${code2}/${accession}/*.fastq.gz --no-remove-listing -P ./ENA/${sample}/"\nwget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/${code1}/${code2}/${accession}/*.fastq.gz --no-remove-listing -P ./ENA/${sample}/;\nfi\n'''.replace('    ', '')
        header += '''cd ./ENA/${sample}/\n\nif [ ${#accession} -eq 9 ]; then\n'''
        header += '''FTPUSER='anonymous'\nFTPPASSWD='mymail@myaddress.uk'\n\nftp -in ftp.sra.ebi.ac.uk <<END_SCRIPT\nuser $FTPUSER $FTPPASSWD\nbin\ncd vol1/fastq/${code1}/${accession}\nmget *\nquit\nEND_SCRIPT\necho "Bye."\nfi\n\n'''.replace('    ', '')
        header += '''if [ ${#accession} -eq 10 ]; then\n'''
        header += '''FTPUSER='anonymous'\nFTPPASSWD='mymail@myaddress.uk'\n\nftp -in ftp.sra.ebi.ac.uk <<END_SCRIPT\nuser $FTPUSER $FTPPASSWD\nbin\ncd vol1/fastq/${code1}/${code2}/${accession}\nmget *\nquit\nEND_SCRIPT\necho "Bye."\nfi\n\n'''.replace('    ', '')
        header += '''echo "Downloaded $accession"\n'''
        outsh.write('%s\n' % header)
        outsh.close()


    # Start data staging of all files in a smaple folder and wait for it to end.
    roadrunner.parse(sample, prj, script_name="./SCRIPTS/ENAdownload.sh", jobname="enadwld")
    roadrunner.saveCommand()

    return 0


# datastore staging of single sample.
def DSTAGE(sample, path, opts, roadrunner):

    if not os.path.exitst('./SCRIPTS/DATASTOREstaging.sh'):
        outsh = open('./SCRIPTS/DATASTOREstaging.sh', "w")
        
        # Start create header for SH file.
        h = Header(opts)
        h.processname("datastore")
        h.setcores(None)
        h.setmemory(None)
        h.runtime(2)
        h.stderrSet("./LOGS/${}.${}.err".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag))
        h.stdoutSet("./LOGS/${}.${}.out".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag))
        h.addmailaddr(opts.mailaddr)
        h.setprj(opts.prj)
        header = h.get() 
        if not os.path.exists("./DATASTORE"): os.mkdir("./DATASTORE")
        if not os.path.exists("./DATASTORE/"+sample): os.mkdir("./DATASTORE/"+sample)

        header += '''sample=$1\n'''
        header += '''path=$2\n\n'''


        header += '''cp -rf $path/*.fastq.gz $path/*.fq.gz ./DATASTORE/$sample && touch DATASTORE/$sample/*'''
        outsh.write('%s\n' % header)
        outsh.close()

    # Start data staging of all files in a smaple folder and wait for it to end.
    roadrunner.parse(sample, path, script_name="./SCRIPTS/DATASTOREstaging.sh", jobname="datastore", options="-q staging")
    roadrunner.saveCommand()
    
    return 0
