# Functions to scatter a series of data into smaller chunks

from lib.Utilities import Header
from lib.Utilities import creates_pname
import time
import os
import sys



# Check if file have tile info in header
def hasTile(read):
    import gzip
    opn = open
    if ".gz" in read:
        opn = gzip.open

    line = None
    for n, l in enumerate(opn(read)):
        if n == 0:
            line = l
            break
    try:
        line = line.strip().split(":")[4]
        return 0
    except:
        return 1

# Chunk in tiles
def tile(sample, pair, opts, roadrunner):
    import os
    if not os.path.exists('./SCRIPTS/TileFq.sh'):
        outsh = open('./SCRIPTS/TileFq.sh', "w")
        
        # Start create header for SH file.
        h = Header(opts)
        h.processname("tiler")
        h.setcores(1)
        h.setmemory(48)
        h.runtime(48)
        h.stderrSet("./LOGS/${}.${}.err".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag))
        h.stdoutSet("./LOGS/${}.${}.out".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag))
        h.addmailaddr(None)
        h.setprj(opts.prj)
        dependencies = ["java", "samtools", "gatk"]
        for i in dependencies:
            h.add_dependency(i)
        header = h.get() 
        header += '''sample=$1\n'''
        header += '''pair=$2\n'''
        header += '''reads=`echo $pair | tr ',' ' ' ` \n'''
        header += '''read1=($reads)\n'''
        header += '''tName=$(basename $read1)\n'''
        header += '''accession=$(python -c 'import sys; print [sys.argv[1].replace(".fq.gz", ".fastq.gz").replace(".fastq.gz", ""), sys.argv[1].replace(".fq.gz", ".fastq.gz").replace("_1.fastq.gz", "").replace("_R1.fastq.gz", "").replace("_R2.fastq.gz", "").replace("_2.fastq.gz", "")]["_1.fastq.gz" in sys.argv[1].replace(".fq.gz", ".fastq.gz") or "_R1.fastq.gz" in sys.argv[1].replace(".fq.gz", ".fastq.gz")]' $tName)\n'''
        header += '''IFS=', ' read -r -a readsarray <<< "$reads"\n'''
        header += '''R1="${readsarray[0]}"\n'''
        header += '''R2="${readsarray[1]}"\n'''
        header += '''gunzip -c $R1 | awk 'NR % 4 == 1 {print}' | cut -d: -f5 | uniq > ./DATASTORE/${sample}/${accession}_tile.txt\n'''
        header += 'python {}/Tiler.py'.format( os.path.dirname(os.path.realpath(__file__)).replace("/lib/", "/misc/") ) + ' $R1 ./DATASTORE/${sample}/${accession}_tile.txt\n'
        header += 'python {}/Tiler.py'.format( os.path.dirname(os.path.realpath(__file__)).replace("/lib/", "/misc/") ) + ' $R2 ./DATASTORE/${sample}/${accession}_tile.txt\n'    
        header += 'echo "Done tiling of:\n - $R1\n - $R2\n"'
        outsh.write('%s\n' % header)
        outsh.close()

    # Start data staging of all files in a smaple folder and wait for it to end.
    if not os.path.exists("./DATASTORE"): os.mkdir("./DATASTORE")
    if not os.path.exists("./DATASTORE/"+sample): os.mkdir("./DATASTORE/"+sample)
    roadrunner.parse(sample, ','.join(pair), script_name="./SCRIPTS/TileFq.sh", jobname="tiler")
    roadrunner.saveCommand()
    
    return "./DATASTORE/"+sample

# Function to scatter based on size.
def scatter(sample, pair, opts, roadrunner):
    import os
    if not os.path.exists('./SCRIPTS/ScatterFq.sh'):
        outsh = open('./SCRIPTS/ScatterFq.sh', "w")
        
        # Start create header for SH file.
        h = Header(opts)
        h.processname("scatter")
        h.setcores(1)
        h.setmemory(16)
        h.runtime(48)
        h.stderrSet("./LOGS/${}.${}.err".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag) )
        h.stdoutSet("./LOGS/${}.${}.out".format(roadrunner.jobNameFlag, roadrunner.jobIdFlag) )
        h.addmailaddr(None)
        h.setprj(opts.prj)
        dependencies = ["java", "gatk"]
        for i in dependencies:
            h.add_dependency(i)
        if opts.scatterSize == -1:
            sctSize = 100000000
        else:
            sctSize = opts.scatterSize
        header = h.get() 
        header += '''sample=$1\n'''
        header += '''pair=$2\n'''
        #header += '''gatk={}\n'''.format(opts.path_to_GATK)
        #header += '''reads=`python -c "import os, sys; print(' '.join(sorted([i for i in os.listdir(sys.argv[1]) if '.fq' in i or '.fastq' in i and '.md5' not in i])))" $path` \n'''
        header += '''reads=`echo $pair | tr ',' ' ' ` \n'''
        header += '''read1=($reads)\n'''
        header += '''tName=$(basename $read1)\n'''
        header += '''accession=$(python -c 'import sys; print [sys.argv[1].replace(".fq.gz", ".fastq.gz").replace(".fastq.gz", ""), sys.argv[1].replace(".fq.gz", ".fastq.gz").replace("_1.fastq.gz", "").replace("_R1.fastq.gz", "").replace("_R2.fastq.gz", "").replace("_2.fastq.gz", "")]["_1.fastq.gz" in sys.argv[1].replace(".fq.gz", ".fastq.gz") or "_R1.fastq.gz" in sys.argv[1].replace(".fq.gz", ".fastq.gz")]' $tName)\n'''
        header += '''IFS=', ' read -r -a readsarray <<< "$reads"\n'''
        header += '''R1="${readsarray[0]}"\n'''
        header += '''R2="${readsarray[1]}"\n'''
    # Check whether apply scatter-gather approach to alignment
        header += '''if [ -z $R1 ] && [ -z $R2 ]; then param="-F1 "$reads; else param="-F1 "$R1" -F2 "$R2; fi \n'''
        header += '''gatk --java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0} -Xmx12G" FastqToSam \
                -SM $sample \
                $param \
                -O ./DATASTORE/$sample/$accession.unaligned.bam \
                --TMP_DIR ./DATASTORE/$sample\n'''.format(roadrunner.jobCores)
        header += '''gatk --java-options "-XX:ConcGCThreads=${0} -XX:ParallelGCThreads=${0} -Xmx12G" SplitSamByNumberOfReads \
                -N_READS {1} \
                -I ./DATASTORE/$sample/$accession.unaligned.bam \
                --OUT_PREFIX ./DATASTORE/$sample/$accession \
                -O `pwd` && rm ./DATASTORE/$sample/$accession.unaligned.bam\n'''.format(roadrunner.jobCores, sctSize)
        header += '''for i in `ls ./DATASTORE/${sample}/${accession}*.bam`; do\n'''
        header += '''\tbamName=`basename -s ".bam" $i`\n'''
        header += '''\tif [ -z $R1 ] && [ -z $R2 ]; then \n\t\tparam="-F ./DATASTORE/$sample/${bamName}.fq.gz";\n\telse\n\t\tparam="-F ./DATASTORE/$sample/${bamName}_R1.fq.gz --SECOND_END_FASTQ ./DATASTORE/$sample/${bamName}_R2.fq.gz";\n\tfi \n'''
        header += '''\tgatk --java-options "-Xmx12G" SamToFastq -I ${i} ${param} && rm ${i}\n'''
        header += '''done #&& rm ./DATASTORE/$sample/$accession*.bam\n'''
        
        outsh.write('%s\n' % header)
        outsh.close()

    # Start data staging of all files in a smaple folder and wait for it to end.
    if not os.path.exists("./DATASTORE"): os.mkdir("./DATASTORE")
    if not os.path.exists("./DATASTORE/"+sample): os.mkdir("./DATASTORE/"+sample)
    roadrunner.parse(sample, ','.join(pair), script_name="./SCRIPTS/ScatterFq.sh", jobname="scatter")
    roadrunner.saveCommand()
    
    return "./DATASTORE/"+sample
