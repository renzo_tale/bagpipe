########################################################
# This file contains all methods to manage data        #
# including SRA, ENA and Datastore downloader/uploader #
########################################################


from lib.Utilities import Header
from lib.Utilities import creates_pname
from lib.Utilities import levenshtein_ratio_and_distance
import time
import os
import sys

try: 
    import prettytable
except: 
    print '\nNo prettytable package available.'
    print 'This is recommended to get nice tables within the script.'
    print 'The program will work in any case.\n\n'


# Read the input file and define which need to be downloaded, which are local and which are on datastore
def defineInputs(myfile, sample_list, sample_paths, completed, opts, roadrunner, nsamples):
    import sys
    from lib.rawlib.Parser import SRA_DWNLD, ENA_DWNLD, DSTAGE
    for i in open(myfile):
        try: sample, pathToFiles = i.strip().replace(' ','\t').split()
        except: sys.exit("Error! File list must contain:\n - Sample ID\n - Path to sample folder\n\nClosing.")
        # Fix bad characters in sample name:
        sample = sample.replace("'", "-").replace("/", "-").replace("\\", "-")
        sample_list += [sample]
        if sample not in sample_paths:
            sample_paths[sample] = []
            
        if 'L:' in pathToFiles:
            pathToFiles = pathToFiles.replace('L:', '')
            sample_paths[sample] += [pathToFiles]
            nsamples += 1

        elif 'S:' in pathToFiles:
            SRA_DWNLD(sample, pathToFiles.replace("S:", ''), opts, roadrunner)
            sample_paths[sample] += [os.path.join("./SRA", sample)]
            nsamples += 1

        elif 'E:' in pathToFiles:
            ENA_DWNLD(sample, pathToFiles.replace("E:", ''), opts, roadrunner)
            sample_paths[sample] += [os.path.join("./ENA", sample)]
            nsamples += 1

        elif 'D:' in pathToFiles:
            DSTAGE(sample, pathToFiles.replace("D:", ''), opts, roadrunner)
            sample_paths[sample] += [os.path.join("./DATASTORE", sample)]
            completed += [sample]
            nsamples += 1
    return sample_list, sample_paths, completed, opts, roadrunner, nsamples


# Run the actual scatter gather pipeline
def scatterGatherPipe(sample_dict, sample_paths, opts, roadrunner):
    from lib.rawlib.Chunking import scatter, tile, hasTile
    newPaths = {}
    for sample in sample_dict:
        for pair in sample_dict[sample]:
            # Run by tile or by size
            if hasTile(pair[0]) == 0 and opts.scatterSize == -1:
                newPaths[sample]= tile(sample, pair, opts, roadrunner) 
            else: 
                newPaths[sample]= scatter(sample, pair, opts, roadrunner)
    # If commands need to be run, execute them
    if len(roadrunner.commandlist) > 0:
        completed_idx, failed_idx = roadrunner.run(opts.nprocesses)
        print "Data preparation completed.\n"
        time.sleep(5)
    # Generate the new paths and files, and return them
    for sample in newPaths:
        fastq_reads = checkEndings(list(set([os.path.join(path, read) for path in newPaths[sample] for read in os.listdir(path) if '.md5' not in read and ('.fastq.gz' in read or '.fq.gz' in read)]))) 
        bam_reads = list(set([os.path.join(path, read) for path in newPaths[sample] for read in os.listdir(path) if '.md5' not in read and '.bam' in read and '.bai' not in read]))
        sample_dict[sample] = fastq_reads + bam_reads
    
    return sample_dict



# Grep input files
def CheckInputFiles(opts, outcommand, nstep, engine, roadrunner):
    from lib.rawlib.FastQpreprocess import bamToFastq
    sample_dict = {}
    sample_list = []
    sample_paths = {}
    completed = []
    failed_list = []
    nsamples = 0
    
    # Create file list for the analysis.
    print "Start reading {}".format(opts.filename)
    # Reading input dataset and creating all the different jobs to run, if necessary
    sample_list, sample_paths, completed, opts, roadrunner, nsamples = \
        defineInputs(opts.filename, sample_list, sample_paths, completed, opts, roadrunner, nsamples)

    # Submit/run some jobs
    if len(roadrunner.commandlist) > 0:
        completed_idx, failed_idx = roadrunner.run(opts.nprocesses)
        print "Data preparation completed.\n"
        time.sleep(10)
        completed += [sample_list[idx] for idx in completed_idx]
        failed_list += [sample_list[idx] for idx in failed_idx]

    # Get definitive list of succeeded and failded downloads per sample
    sample_list = list(set(sample_list + completed))
    print "Completed samples: {}\n".format(", ".join(sample_list))
    print "Failed samples: {}\n\n".format(", ".join(failed_list))

    # Pairing reads
    for sample in sample_list:
        fastq_reads = checkEndings(list(set([os.path.join(path, read) for path in sample_paths[sample] for read in os.listdir(path) if '.md5' not in read and ('.fastq.gz' in read or '.fq.gz' in read)]))) 
        bam_reads = list(set( [ [os.path.join(path, read)] for path in sample_paths[sample] for read in os.listdir(path) if '.md5' not in read and '.bam' in read and '.bai' not in read] ))
        sample_dict[sample] = fastq_reads + bam_reads
    nsamples = len(sample_list)

    # Check if bam files are present. If so, convert to fastq
    for sample in sample_list:
        reads = sample_dict[sample]
        for n, library in enumerate(reads):
            bams = [i for i in library if ".bam" in library]
            if len(bams) > 1:
                print("Problem with {}: more than 1 bam file clustered".format(sample))
                print("{}".format(",".join(bams))) 
                sys.exit("Exiting")
            elif len(bams) == 1:
                sample_dict[sample][n] = bamToFastq(sample, bams[0], opts, roadrunner)
    if len(roadrunner.commandlist) > 0:
        print("Bam files detected.")
        print("Start conversion to fastq.")
        completed_idx, failed_idx = roadrunner.run(opts.nprocesses)
        print("Bam conversion completed.\n")
        time.sleep(5)

    # Prepare for scatter-gather of datasets 
    if opts.scatterSize > 0 or opts.scatterSize == -1:
        sample_dict = scatterGatherPipe(sample_dict, sample_paths, opts, roadrunner)


    # Correct max number of processes based on actual number of samples
    if opts.nprocesses > nsamples and nsamples > 1: opts.nprocesses = nsamples

    # Saving files to process.
    of = open('./LISTS/filelist.txt', 'w')
    for sample in sample_dict:
        reads = sample_dict[sample]
        for read in reads:
            of.write('%s\t%s\n' % (sample, ','.join(read)))
    of.close()

    # Saving files that failed.
    of = open('./LISTS/failedSamples.txt', 'w')
    for sample in failed_list:
        of.write('%s\n' % (sample))          
    of.close()

    print "Got data for {} samples.".format(nsamples)
    opts.nsamples = nsamples

    if opts.nprocesses is None:
        print "No number of concurrent processes specified."
        print "Assumes that all job are run together."
        opts.nprocesses = nsamples


    # Print sample table.
    print '\nSample, path and reads found:'
    try:
        table=prettytable.PrettyTable()
        [table.add_row([n+1, sample, ','.join(set([os.path.dirname(x) for x in f])), ','.join([os.path.basename(x) for x in f])]) for n, sample in enumerate(sample_list) for f in sample_dict[sample]]
        table.field_names = ["N", 'Sample','Path','Reads']
        print table
    except:
        table_field_names = ['Sample','Path','Reads']
        table_rows = [[n+1, sample, ','.join(set([os.path.dirname(x) for x in f])), ','.join([os.path.basename(x) for x in f])] for n, sample in enumerate(sample_list) for f in sample_dict[sample]]
        mLen = max([len(str(k)) for j in table_rows for k in j])
        print '|{:^{width}}|{:^{width}}|{:^{width}}|'.format(table_field_names[0], table_field_names[1], table_field_names[2], width = mLen)
        for row in table_rows: 
            print '|{:^{width}}|{:^{width}}|{:^{width}}|'.format(row[0], row[1], row[2], width = mLen)
        print

    return sample_list, sample_dict, 'LISTS/filelist.txt', nsamples



# Check paired/single end
def checkEndings_vintage(reads):
    # Sort all reads
    reads = sorted(reads)
    #names = ['_'.join(read.split('_')[0:-1]) if "_" in read else read for read in reads]
    names = ['_'.join(read.split("/")[-1].split('_')[0:-1]) if "_" in read.split("/")[-1] else read for read in reads]
    # detect single-end reads
    SEreads = [[name] for n, name in enumerate(names) if name == reads[n]]
    # detect paired-end reads
    CheckReads = [read for n, read in enumerate(reads) if read != names[n]]
    CheckNames = [name for n, name in enumerate(names) if reads[n] != name]
    PEreads = [[CheckReads[n], CheckReads[n + 1]] for n in xrange(0, len(CheckReads) - 1, 2) if CheckNames[n] == CheckNames[n + 1]]
    # Return list of reads
    return SEreads + PEreads

def checkEndings(reads):
    # Sort all reads
    reads = sorted(reads)
    if len(reads) == 1:
        return [reads]
    PEreads = []
    SEreads = []
    for n, library1 in enumerate(reads):
        for m, library2 in enumerate(reads):
            if m<=n: continue
            if levenshtein_ratio_and_distance(library1, library2) == 1:
                if (library1, library2) not in PEreads and (library2, library1) not in PEreads:
                    PEreads += [[library1, library2]]
                else: continue
            if sum([ 1 for i in PEreads if library1 in i ]) == 0: PEreads += [[library1]]
    if len(SEreads) + sum([len(i) for i in PEreads]) != len(reads):
        # try rescuing with vintage file checker
        SEreads, PEreads = checkEndings_vintage(reads)
    return PEreads + SEreads 


