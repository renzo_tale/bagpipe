import sys
import os
import argparse

def levenshtein_ratio_and_distance(s, t, ratio_calc = False):
    """ levenshtein_ratio_and_distance:
        Calculates levenshtein distance between two strings.
        If ratio_calc = True, the function computes the
        levenshtein distance ratio of similarity between two strings
        For all i and j, distance[i,j] will contain the Levenshtein
        distance between the first i characters of s and the
        first j characters of t
    """
    # Initialize matrix of zeros
    rows = len(s)+1
    cols = len(t)+1
    distance = [ [0 for i in range(0, cols)] for w in range(0, rows)]
    # Populate matrix of zeros with the indeces of each character of both strings
    for i in range(1, rows):
        for k in range(1,cols):
            distance[i][0] = i
            distance[0][k] = k
        # Iterate over the matrix to compute the cost of deletions,insertions and/or substitutions    
    for col in range(1, cols):
        for row in range(1, rows):
            if s[row-1] == t[col-1]:
                cost = 0 # If the characters are the same in the two strings in a given position [i,j] then the cost is 0
            else:
                # In order to align the results with those of the Python Levenshtein package, if we choose to calculate the ratio
                # the cost of a substitution is 2. If we calculate just distance, then the cost of a substitution is 1.
                if ratio_calc == True:
                    cost = 2
                else:
                    cost = 1
            distance[row][col] = min(distance[row-1][col] + 1,      # Cost of deletions
                                    distance[row][col-1] + 1,          # Cost of insertions
                                    distance[row-1][col-1] + cost)     # Cost of substitutions
    if ratio_calc == True:
        # Computation of the Levenshtein Distance Ratio
        Ratio = ((len(s)+len(t)) - distance[row][col]) / (len(s)+len(t))
        return Ratio
    else:
        # print(distance) # Uncomment if you want to see the matrix showing how the algorithm computes the cost of deletions,
        # insertions and/or substitutions
        # This is the minimum number of edits needed to convert string a to string b
        return distance[row][col]

def validate_url(myurl):
    try:
        # python2
        from urlparse import urlparse
    except:
        # python3
        from urllib.parse import urlparse
    try:
        result = urlparse(myurl)
        return all([result.scheme, result.netloc, result.path])
    except:
        return False


def areFile(stringa):
    from os.path import isfile as Isfile
    if stringa is None: return False
    filelist = stringa.split(",")
    for f in filelist:
        if not Isfile(f): return False
    return True 


# Counter of values
def counter(i=40):
    values = ["%02d"%i for i in xrange(0,i)]
    for value in values:
        yield value

# Autocomplete function within software
def complete(text, state):
    import glob
    return (glob.glob(text + '*') + [None])[state]


# Convert memory string to integer value of bytes
def memToInt(value):
    if "G" in value.upper():
        return int( float(value.upper().replace("G","")) * 1024 * 1024 * 1024 )
    if "M" in value.upper():
        return int( float(value.upper().replace("M","")) * 1024 * 1024 )
    if "K" in value.upper():
        return int( float(value.upper().replace("K","")) * 1024 )

# Check folder structure for issues
def checkFolder(opts, engine):
    import os
    from lib.roadrunner.RoadRunner import roadRunnerParallel
    from shutil import rmtree
    from MasterClass import masterOfPuppets

    folders = [ "./SCRIPTS", "./LISTS", "./LOGS", "./SRA", "./ENA", "./TRIM", "./LOCALGRAPH", "./FASTQC", "./ALIGN", "./BQSR",
                "./GRIDSS_SV", "./DELLY_SV", "./DEDUP", "./VCALL", "./VJOIN", "./VREC", "./VCFs", "./jVCF", "./VPPROCESS", "./GENRICH", 
                "./REFERENCE", "./QC", "./METHYL", "./VCFS", "./FILTER" ]

    print "\nFolder structure seems complete."
    print "Do you want to recreated everything? (y/N)"
    choice = raw_input("My choice: ").lower()
    if choice == "y":
        print("This will delete EVERYTHING in this folder.")
        choiceF = raw_input("Are you sure????(y/n): ")
        if choiceF.lower() == "y":
            if os.path.isfile("CommandToRun.txt"): os.remove("CommandToRun.txt")
            for folder in folders: 
                if os.path.exists(folder): 
                    rmtree(folder)
            return 0               
        else:
            return 1
    else:
        print "\nDo you want to:\n\n1) check file lists for errors\n2) regenerate scripts and files\n3) continue?\n"
        choice = raw_input("Choice: ")
        if choice.lower() == "1":
            fileListSanityCheck()
            skip_data_creation = True
            return 1
        elif choice.lower() == "2":
            skip_data_creation = True
            sample_list, sample_dict, filename, nsamples = regenerateInfo(opts)
            [os.remove("./SCRIPTS/{}".format(i)) for i in os.listdir("./SCRIPTS")]
            [os.remove("./LISTS/{}".format(i)) for i in os.listdir("./LISTS") if i != "filelist.txt"]
            return 0
        else:
            # Run the master of puppets
            roadrunner = roadRunnerParallel(engine)
            for line in open("./CommandToRun.txt"):
                roadrunner.commandlist.append(line.strip())
            if opts.superviseAll == True:
                m = masterOfPuppets(roadrunner, opts)
                m.start()
            # ...or autosubmit to a certain point.
            else:
                autosubmit(opts, roadrunner.commandlist)
            print "All done."
            return 1

# Define intervals given a certain karyotype.
def intervals(opts):
    bpi = []
    bpe = []
    chrs = []
    for line in open(opts.ref_genome_path + ".fai"):
        chrom, length = line.strip().split()[0:2]
        length = int(length)
        if length < opts.chunkSize:
            bpi += [1]
            bpe += [length]
            chrs += [chrom]
            continue
        nbpi = list(range(1, length + 1, opts.chunkSize - opts.chunkOvlp))
        bpi += nbpi
        bpe += [nbpi[i] + opts.chunkOvlp - 1 for i in range(1, len(nbpi))] + [length]
        chrs += [chrom for i in range(0, len(nbpi))]
    
    of = open("./LISTS/Intervals.txt", "w")
    for k in range(0, len(chrs)):
        of.write("{}\t{}\t{}\n".format(chrs[k], bpi[k], bpe[k]))
    return "./LISTS/Intervals.txt"


# Sanity check of file lists.
def fileListSanityCheck():
    import os
    flist = ["./LISTS/filelist.txt",  
            "./LISTS/alignedlist.txt", 
            "./LISTS/processedlist.txt", 
            "./LISTS/sample2merge.txt",  
            "./LISTS/mergedSamples.txt",  
            "./LISTS/deduppedlist.txt",     
            "./LISTS/recalibratedlist.txt",
            "./LISTS/vcalledlist.txt",  
            "./LISTS/vcflist.txt",
            "./LISTS/diploidVcfFiles.txt",  
            "./LISTS/failedSamples.txt",
            "./LISTS/chromosomes.txt"]
    samples = {}
    nfiles = 0
    for line in open(flist[0]):
        nfiles += 1
        sample, files = line.strip().split()
        rName = '_'.join(files.split(",")[0].split("/")[-1].replace(".fastq.gz", "").split("_")[:-1])
        if len(rName) == 0:
            rName = files.split(",")[0].split("/")[-1].replace(".fastq.gz", "")
        if sample not in samples:
            samples[sample] = [rName]
            continue
        samples[sample].append(rName)
    # Aligned file list
    if os.path.exists(flist[1]):
        lines = []
        for line in open(flist[1]):
            sample, read = line.strip().split()
            rName = read.replace(".tmp.bam", "")
            lines.append([sample, rName])
        if len(lines) < nfiles:
            of = open(flist[1], 'a+')
            for sample in samples:
                if [sample, samples[sample]] not in lines:
                    fName = samples[sample] + ".tmp.bam"
                    of.write("{}\t{}\n".format(sample, fName))
            of.close()
    else:
        of = open(flist[1], 'w')
        for sample in samples:
            for read in samples[sample]:
                fName = "./ALIGN/{}/{}.tmp.bam".format(sample, read)
                of.write("{}\t{}\n".format(sample, fName))
        of.close()

    # Processed file list
    if os.path.exists(flist[2]):
        lines = []
        for line in open(flist[2]):
            sample, read = line.strip().split()
            rName = read.replace(".bam.sort", "")
            lines.append([sample, rName])
        if len(lines) < nfiles:
            of = open(flist[2], 'a+')
            for sample in samples:
                for read in samples[sample]:
                    if [sample, "./ALIGN/{}/{}".format(sample, read)] not in lines:
                        fName = read + ".bam.sort"
                        of.write("{}\t{}\n".format(sample, fName))
            of.close()
    else:
        of = open(flist[2], 'w')
        for sample in samples:
            for read in samples[sample]:
                fName = "./ALIGN/{}/{}.bam.sort".format(sample, read)
                of.write("{}\t{}\n".format(sample, fName))
        of.close()

    # Sample to merge list
    if os.path.exists(flist[3]):
        lines = []
        samples2merge = [sample for sample in samples if len(samples[sample]) > 1]
        for line in open(flist[3]):
            sample = line.strip()
            lines.append(sample)
        if len(samples2merge) > lines:
            of = open(flist[3], 'a+')
            [of.write("{}\n".format(sample)) for sample in samples2merge if sample not in lines]
            of.close()
    else:
        samples2merge = [sample for sample in samples if len(samples[sample]) > 1]
        of = open(flist[3], "w")
        of.write("{}".format("\n".join(samples2merge)))
        of.close()

    # Check Merged list
    if os.path.exists(flist[4]):
        lines = []
        for line in open(flist[4]):
            sample, fileName = line.strip().split()
            lines.append([sample, fileName])
        if len(lines) != len(samples):
            of = open(flist[4], 'a+')
            for sample in samples:
                if sample in samples2merge:
                    if [sample, "./ALIGN/{0}/{0}.merged.bam.sort".format(sample)] not in lines:
                        of.write("{0}\t./ALIGN/{0}/{0}.merged.bam.sort\n".format(sample))
                else:
                    if [sample, samples[sample] + ".bam.sort"] not in lines:
                        of.write("{0}/t{1}.bam.sort\n".format(sample, samples[sample]))
            of.close()
    else:
        of = open(flist[4], 'a+')
        for sample in samples:
            if sample in samples2merge:
                of.write("{0}\t./ALIGN/{0}/{0}.merged.bam.sort\n".format(sample))
            else:
                of.write("{0}\t./ALIGN/{0}/{1}.bam.sort\n".format(sample, samples[sample]))
        of.close()

    # Check dedupped list
    if os.path.exists(flist[5]):
        lines = []
        for line in open(flist[5]):
            sample, fileName = line.strip().split()
            lines.append([sample, fileName])
        if len(lines) != len(samples):
            of = open(flist[5], 'a+')
            for sample in samples:
                fName = "./DEDUP/{0}/{0}.dedup.bam".format(sample)
                if [sample, fName] not in lines:
                    of.write("{0}\t{1}\n".format(sample, fName))
            of.close()
    else:
        of = open(flist[5], 'a+')
        for sample in samples:
            fName = "./DEDUP/{0}/{0}.dedup.bam".format(sample)
            of.write("{0}\t{1}\n".format(sample, fName))
        of.close()

    # Check BQSR list
    if os.path.exists(flist[6]):
        lines = []
        for line in open(flist[6]):
            sample, fileName = line.strip().split()
            lines.append([sample, fileName])
        if len(lines) != len(samples):
            of = open(flist[6], 'a+')
            for sample in samples:
                fName = "./BQSR/{0}/{0}.bqsr.bam".format(sample)
                if [sample, fName] not in lines:
                    of.write("{0}\t{1}\n".format(sample, fName))
            of.close()
    elif os.path.exists(flist[6]) and os.path.isdir("./BQSR"):
        of = open(flist[6], 'a+')
        for sample in samples:
            fName = "./BQSR/{0}/{0}.bqsr.bam".format(sample)
            of.write("{0}\t{1}\n".format(sample, fName))
        of.close()
    print "\nDone file lists sanity check.\n"
    
    return 0            


def regenerateInfo(opts):
    import os
    sample_list = []
    sample_dict = {}
    filename = "./LISTS/filelist.txt"
    nsamples = 0
    if os.path.exists(filename):
        for line in open(filename):
            sample, reads = line.strip().split()
            reads = reads.split(",")
            if sample not in sample_dict: 
                sample_dict[sample] = [reads]
                continue
            sample_dict[sample].append(reads)
        sample_list = list(set(sample_dict))
        nsamples = len(sample_list)
    else:
        sys.exit("No ./LISTS/filelist.txt existing.\nExit.")
    return sample_list, sample_dict, filename, nsamples


def autosubmit(opts, commandlist):
    import subprocess as sbp
    import time
    val = int(opts.submit)
    steps = ["Quit",
            "FastQC_pipeline",
            "SummarizeAlignmentStats", 
            "ProcessReads", 
            "SummarizeDepth", 
            "BaseQualityReCal_pipeline", 
            "VariantCall_pipeline", 
            "VariantJointCall"]
    # check submission command
    if opts.BQSR == 'n' and val == 5:
        val -= 1
    if opts.fqc == "n" and val == 1:
        val -= 1
    
    if val == 0:
        print "No self-submission."
        return 0
    elif val > 0 and val not in range(1, 8): 
        print "Cannot submit automatically."
        return 0
    
    # Start creation of submission string
    stopStep = steps[val]
    submissions = ""
    for cmd in commandlist:
        if submissions == "": submissions = cmd
        else: submissions = submissions + "; " + cmd
        if stopStep in cmd: break
    # Do real submission
    print "Submitting command:\n\n{}\n\n".format(submissions)
    sbp.Popen(submissions, shell = True)
    time.sleep(5)
    return 0
        

def GetKaryotypes(refgenome):
    mykaryo = []
    for line in open(refgenome + '.fai'):
        line = line.strip().split()
        if line[0].isdigit() or line[0] == 'X' or line[0] == 'Y' or line[0] == 'MT':
            mykaryo.append(line[0])

    return mykaryo


def qstat():
    import subprocess as sbp
    stats = sbp.check_output(["qstat"])
    stats = stats.splitlines()
    return stats


def creates_pname(rootname):
    import random
    try : 
        stats = qstat()
    except: 
        print "Cannot run qstat. Use random numbers."
        stats = []
    n = "%05d" % random.randint(1,99999)
    isin = sum([True for i in stats if "{}_{}".format(rootname[0:2], n) in i])
    while isin > 0:
        try: 
            stats = qstat()
        except:
            stats = []
        n = "%05d" % random.randint(1,99999)
        isin = [True for i in stats if n in i]
    name = rootname[0:2] + "_" + n
    return name




class Header():
    def __init__(self, opts = None):
        from lib.GridOptions import GridEngine
        self.myEngine = GridEngine()
        if opts is not None:
            for mod in opts.customModules:
                if mod in self.myEngine.modules: del(self.myEngine.modules[mod])
                if mod in self.myEngine.localExe: del(self.myEngine.localExe[mod])
            for exe in opts.customExe:
                if exe in self.myEngine.modules: del(self.myEngine.modules[exe])
                if exe in self.myEngine.localExe: del(self.myEngine.localExe[exe])
            for env in opts.customEnv:
                if env in self.myEngine.localEnv: del(self.myEngine.localEnv[env])
            self.myEngine.modules.update(opts.customModules)
            self.myEngine.localExe.update(opts.customExe)
            self.myEngine.localEnv.update(opts.customEnv)
        self.modules = self.myEngine.modules
        self.header = self.myEngine.header
        self.localExe = self.myEngine.localExe
        self.localEnv = self.myEngine.localEnv
        self.importCommand = self.myEngine.importCommand
        self.importingScript = self.myEngine.importingScript
        self.memory=2
        self.threads=1
        self.rtime=12
        self.njobs = None
        self.concurrent = None
        self.pname="myprocess"
        self.mailaddr=None
        self.spwnopt = "e"
        self.jid=None
        self.jidad=None
        self.addedModules = []
        self.addedExe =  []
        self.addedEnv =  []
        self.additionalOpts = []
        self.stdoutFldName = ""
        self.stderrFldName = ""
        self.prjid = None
        

    # Return customized header
    def get(self):
        tmpheader = self.header
        if self.pname is not None:
            tmpheader = tmpheader.replace("NAMEFILE", self.pname)
        else:
            tmph = '\n'.join([line for line in tmpheader.split("\n") if "NAMEFILE" not in line])
            tmpheader = tmph

        tmpheader = tmpheader.replace("RTIME", str(self.rtime) + ":00:00")

        # Check Thread number
        if self.stdoutFldName != "":
            tmpheader = tmpheader.replace("STOUT","{}".format(self.stdoutFldName))
        else:
            tmpheader = tmpheader.replace("STOUT","{}".format(self.stdoutFldName))

        if self.stderrFldName != "":
            tmpheader = tmpheader.replace("STERR","{}".format(self.stderrFldName))

        if self.jid is not None and self.jid != "":
            tmpheader = tmpheader.replace("JID", str(self.jid.strip(",")))
        else:
            tmph = '\n'.join([line for line in tmpheader.split("\n") if "JID" not in line])
            tmpheader = tmph

        if self.jidad is not None and self.jidad != "":
            tmpheader = tmpheader.replace("JAD", str(self.jidad.strip(",")))
        else:
            tmph = '\n'.join([line for line in tmpheader.split("\n") if "JAD" not in line])
            tmpheader = tmph

        if self.memory is not None:
            tmpheader = tmpheader.replace("MEMGB", str(self.memory))
        else:
            tmpheader = tmpheader.replace("MEMGB", "2")

        # Check Thread number
        if self.threads is not None:
            tmpheader = tmpheader.replace("PROCN", str(self.threads))
        else:
            tmpheader = tmpheader.replace("PROCN", "1")
            

        # Check Mail address
        if self.mailaddr is not None:
            tmpheader = tmpheader.replace("MAILADDR", self.mailaddr)
            tmpheader = tmpheader.replace("SPWNOPT", self.spwnopt)
        else: 
            tmph = '\n'.join([line for line in tmpheader.split("\n") if "MAILADDR" not in line and "SPWNOPT" not in line])
            tmpheader = tmph

        # Add project ID
        if self.prjid is not None:
            tmpheader = tmpheader.replace("PRJCT", self.prjid)
        else: 
            tmph = '\n'.join([line for line in tmpheader.split("\n") if "PRJCT" not in line])
            tmpheader = tmph

        # Define job number
        if self.njobs is not None:
            tmpheader = tmpheader.replace("NJOBS", "1-{}".format(self.njobs))
        else: 
            tmph = '\n'.join([line for line in tmpheader.split("\n") if "NJOBS" not in line and "NCONCUR" not in line])
            tmpheader = tmph

        # Define job cuncurrency
        if self.njobs is not None:
            tmpheader = tmpheader.replace("NCONCUR", "{}".format(self.concurrent))
        else: 
            tmph = '\n'.join([line for line in tmpheader.split("\n") if "NCONCUR" not in line])
            tmpheader = tmph            

        # Add additional options
        if len(self.additionalOpts) > 0:
            newopts = ''
            for opt in self.additionalOpts:
                if "#$" not in opt: opt = "#$ " + opt
                newopts = opt + "\n" 
            tmpheader = tmpheader.replace("#ADDITIONALOPTS", newopts)
        else:
            tmpheader = tmpheader.replace("#ADDITIONALOPTS\n", "")

        # Check module needed
        if len(self.addedModules) > 0 :
            modulestring = "{}\n".format(self.importingScript)
            for module in self.addedModules:
                modulestring += "{} {}\n".format(self.importCommand, self.modules[module])
            tmpheader = tmpheader.replace("#MODULES", modulestring)
        else: 
            tmpheader = tmpheader.replace("#MODULES\n", "")

        # Check module needed
        if len(self.addedExe) > 0 :
            exestring = ""
            for exe in self.addedExe:
                exestring += "export PATH={}:$PATH\n".format(self.localExe[exe])
            tmpheader = tmpheader.replace("#EXE", exestring)
        else: 
            tmpheader = tmpheader.replace("#EXE", "")

        # Check environments needed
        if len(self.addedEnv) > 0 :
            envstring = ""
            if "anaconda" not in self.addedExe and "anaconda" not in self.addedModules:
                condamod = [i for i in [self.localExe.get("anaconda", None), self.modules.get("anaconda", None)] if i is not None][0]
                envstring += ". /etc/profile.d/modules.sh\nmodule load {}\n".format(condamod)
            for env in self.addedEnv:
                envstring += "source activate {}\n".format(self.localEnv[env])
            tmpheader = tmpheader.replace("#ENV", envstring)
        else: 
            tmpheader = tmpheader.replace("#ENV", "")

        return tmpheader + "\n"


    def local(self):
        tmpheader = self.header
        tmpheader = '\n'.join([line for line in tmpheader.split("\n") if "EXE" in line or "ENV" in line or "/bin/bash" in line])

        # Check module needed
        if len(self.addedExe) > 0 :
            exestring = ""
            for exe in self.addedExe:
                exestring += "export PATH={}:$PATH\n".format(self.localExe[exe])
            tmpheader = tmpheader.replace("#EXE", exestring)
        else: 
            tmpheader = tmpheader.replace("#EXE", "")

        # Check environments needed
        if len(self.addedEnv) > 0 :
            envstring = ""
            for env in self.addedEnv:
                envstring += "source activate {}\n".format(self.localEnv[env])
            tmpheader = tmpheader.replace("#ENV", envstring)
        else: 
            tmpheader = tmpheader.replace("#ENV", "")
        return tmpheader + "\n"



    # include new modules
    def add_dependency(self, modulename):
        if modulename in self.modules:
            self.addedModules.append(modulename)
        if modulename in self.localExe:
            self.addedExe.append(modulename)
        if modulename in self.localEnv:
            self.addedEnv.append(modulename)

    # reset modules
    def resetmodules(self):
        self.addedModules = []

    # set stdOut:
    def stdoutSet(self, stdoutpath):
        self.stdoutFldName = stdoutpath
    # set stdErr:
    def stderrSet(self, stderrpath):
        self.stderrFldName = stderrpath

    # change parameters
    def setcores(self, ncores):
        if ncores is None:
            self.threads = None
        elif int(ncores) >= 1:
            self.threads = int(ncores)

    # change parameters
    def setprj(self, prjID):
        if prjID is not None and prjID is not "":
            self.prjid = prjID

    # set n jobs
    def setnjobs(self, njobs):
        if njobs is None:
            self.njobs = None
        elif int(njobs) >= 1:
            self.njobs = int(njobs)

    # set n jobs
    def setconcurrency(self, nconcur):
        if nconcur is None:
            self.concurrent = None
        elif int(nconcur) > 0:
            self.concurrent = int(nconcur)

    # Add new options for eddie
    def addOpts(self, mystring):
        self.additionalOpts.append(mystring)

    # include new modules
    def setmemory(self, ramgb):
        if ramgb is None:
            self.memory = None
        elif int(ramgb) <= 2:
            self.memory = 2.0
        else:
            self.memory = round(float(ramgb),1)

    # include new modules
    def runtime(self, hours):
        if int(hours) >=1:
            self.rtime = int(hours)

    # include new modules
    def holdjid(self, jid):
        if self.jid is None:
            self.jid = jid

    # include new modules
    def holdjidad(self, jid):
        if self.jidad is None:
            self.jidad = jid

    # include new modules
    def resetjid(self, jid):
        self.jid = None
        self.jidad = None

    # include new modules
    def processname(self, process):
        if process is None: self.pname = None
        else: self.pname = process.replace(" ","").replace("\t","")

    # include email information
    def addmailaddr(self, mailaddr):
        self.mailaddr=mailaddr

    # include email information
    def addspwn(self, spwnopt):
        self.spwnopt=spwnopt

    # include email information
    def resetmailaddr(self):
        self.mailaddr=None
        
    # Reset all
    def resetall(self):
        self.header = self.myEngine.header
        self.memory=2
        self.threads=1
        self.rtime=12
        self.pname="myprocess"
        self.mailaddr=None
        self.jid=None
        self.addedModules = []
        self.addedExe = []
        self.additionalOpts = []
        self.stdoutFldName = ""
        self.stderrFldName = ""
        
    # End of header class.




class settings():

    def __init__(self):
        # Arguments.
        self.args = None

        # Param file name
        self.params = None

        # Specified fields
        self.shortname = "BAGPIPE"
        self.extendedName = "BwA-Gatk PIPeline for Eddie"
        self.version = "1.4.1"
        self.logo = '''
                                                                        
         '                                                              

             ++                                              ':           
              +#                               ''            +            
              `+,                              #,           +            
               ;+                     ,''      #,          ,;            
                #+         .:          #:      #,          #             
                 +#        '',         +       #,         +             
                  +;        ;#         +       #,        .;             
                   :+         +        +       #.        #              
                     +#        +       +       #+       ;              
                      `+,      ##      +       +`      ;                
                        ;+      `+     +.      +`     .                 
                         #+      #'    ++      +`     #                 
                      :.  +#      +    :#      +`   '.#                 
                     .##;  +;     +:   `+      +`   ###'                
                    ###++## #+`    +`   +      #;  #+###                
                   ++++######+##   `#,  ##     #;:###+++                
                  ;#++++###++####,  ,#  +#    ###+##++#+                
                  #++####+++#####++####+###++####+++###                 
                  #+++##+######+##+++++++++#++#######                   
                   +###+++####++######+######++###+`                    
                   +###+++++##+#######+######++#++,                     
                     #++#####+++++++++++++++###                         
                        ++++##+#########++;                             
                         ,#+++++++++++++#                               
                                 ``                                     

___.                         .__               
\_ |__ _____     ____ ______ |__|_____   ____  
 | __ \\__  \   / ___\\____ \|  \____ \_/ __ \ 
 | \_\ \/ __ \_/ /_/  >  |_> >  |  |_> >  ___/ 
 |___  (____  /\___  /|   __/|__|   __/ \___  >
     \/     \//_____/ |__|      |__|        \/ 
     
     '''

        self.path = None
        self.filename = None
        self.nthreads = None
        self.nthreadsAlignment = None
        self.ref_genome_path = None
        self.paired = None
        self.outfile = None
        self.indelsf = None
        self.dbsnpf = None
        self.adapter = None
        self.path_to_GATK = None
        self.GATK_tmp_opt = "tmp-dir"
        self.vcaller = "HaplotypeCaller"
        self.aligner = "bwa"
        self.path_to_trimmomatic = None
        self.trimmomatic = None
        self.trimgalore = None
        self.BQSR = None
        self.PigZ = None
        self.fqc = None
        self.indesparam = None
        self.snpparam = None
        self.nprocesses = None
        self.stageoutpath = None
        self.stageoutstep = None
        self.chromosomes = None
        self.mailaddr = None
        self.prj = None
        self.superviseAll = False
        self.wait = ''
        self.waitad = ''
        self.nsamples = None
        self.sexcheck = None
        self.submit = None
        self.ploidy = 2
        self.pool = 1
        # Grid engine settings
        self.memoryset = "-l h_vmem"
        self.timeset = "-l h_rt"
        self.procname = "-N"
        self.procset = "-pe sharedmem"
        self.stdoutlog = "-o"
        self.stderrlog = "-e"
        self.threading = "-t"
        self.concurrentProc = "-tc"
        self.project = "-P"
        # Run short-reads based SV calling
        self.gridss = ""
        self.delly = 0
        self.getsv = "n"
        # Gatk additional settings
        self.GATKoptions = {}
        # Cluster option
        self.clustermode = "SGE"
        # Reads type
        self.atac = False
        self.rrbs = False
        # Get a spike-in genome. 
        # Can be a local path or a valid url
        self.spikein = None 
        # Graph genome settings
        self.graph_genome_path = ""
        self.GPC_graph = ""
        self.do_graph_alignment = "n"
        # Scattering settings
        self.chunking = False
        self.chunkSize = 0
        self.chunkOvlp = 0
        self.scatterSize = 50000000
        # Interleave reads
        self.interleave = False
        # Custom executables
        self.customExe = {}
        self.customModules = {}
        self.customEnv = {}
        # Working directory of bagpipe
        self.wdir = os.path.dirname(os.path.realpath(__file__))


    # Create the parsing method
    def parser(self):
        # Create argument parser.
        parser = argparse.ArgumentParser(prog = "bagpipe", description='Whole genome sequencing analysis, from scratch to variants.')



        # Defining the arguments.
        parser.add_argument('-P','--params', metavar = 'file.param', type = str, help = 'Give all parameters in a param file', \
                            default = None, dest = 'params', required=True)

        parser.add_argument('-G','--grid', metavar = 'y/N', type = str, help = 'Use trimgalore?', default = None,  \
                            dest = 'gridP', required=False)

        parser.add_argument('-v', '--version', action='version', version='%(prog)s 1.0.0')
        

        # Parse all arguments and settings.
        self.args = parser.parse_args()
        
    
    def parseGridChanges(self, infile):
        vtype = [0, 0, 0]
        customModules = {}
        customExe = {}
        customEnv = {}
        for line in open(infile):
            if "#MOD" in line.strip():
                vtype = [1, 0, 0]
                continue
            elif "#EXE" in line.strip():
                vtype = [0, 1, 0]
                continue
            elif "#ENV" in line.strip():
                vtype = [0, 0, 1]
                continue
            if vtype[0]:
                name, mod = line.strip().split("=")
                customModules[name] = mod
                continue
            elif vtype[1]:
                name, exe = line.strip().split("=")
                customExe[name] = exe
                continue
            elif vtype[2]:
                name, env = line.strip().split("=")
                if env == "None": env = None
                customEnv[name] = env
                continue
        return customModules, customExe, customEnv


    def define_arguments(self):
        import os
        if self.args.gridP is not None:
            self.customModules, self.customExe, self.customEnv = self.parseGridChanges(self.args.gridP)
        else:
        	print "Parsing param file"
        for i in open(self.args.params):
            if i[0] == "#" or len(i.strip()) == 0: continue
            i = i.strip().split('=$')
            if len(i) != 2: continue
            if i[0].lower() == 'path': self.path = i[1]
            if i[0].lower() == 'filename': self.filename = i[1]
            if i[0].lower() == 'nthreads': self.nthreads = i[1]
            if i[0].lower() == 'nthreadsaligner' or i[0].lower() == 'nthreadsbwa': self.nthreadsAlignment = i[1]
            if i[0].lower() == 'ref_genome_path': self.ref_genome_path = i[1]
            if i[0].lower() == 'paired': self.paired = i[1]
            if i[0].lower() == 'outfile': self.outfile = i[1]
            if i[0].lower() == 'indelsf': self.indelsf = i[1]
            if i[0].lower() == 'dbsnpf': self.dbsnpf = i[1]
            if i[0].lower() == 'adapter': self.adapter = i[1]
            if i[0].lower() == 'path_to_gatk': 
                if os.path.isfile(i[1]) == True:
                    i[1] = '/'.join(i[1].split("/")[0:-1])
                self.customExe["gatk"] = i[1]
            if i[0].lower() == 'trimmomatic': self.trimmomatic = i[1]
            if i[0].lower() == 'trimgalore': self.trimgalore = i[1]
            if i[0].lower() == 'bqsr': self.BQSR = i[1]
            if i[0].lower() == 'pigz': self.PigZ = i[1]
            if i[0].lower() == 'fqc': self.fqc = i[1]
            if i[0].lower() == 'indelsparam': self.indelsparam = i[1]
            if i[0].lower() == 'snpparam': self.snpparam = i[1]
            if i[0].lower() == 'nprocesses': self.nprocesses = i[1]
            if i[0].lower() == 'stageoutpath': self.stageoutpath = i[1]
            if i[0].lower() == "email": self.mailaddr = i[1] 
            if i[0].lower() == "project": self.prj = i[1]
            if i[0].lower() == "checksex": self.sexcheck = i[1]
            if i[0].lower() == "submit": self.submit = i[1]
            if i[0].lower() == "interleave": 
                if i[1].lower() == "true" or i[1].lower() == "y" or i[1] == "1": 
                    self.interleave = True
            if i[0].lower() == "save_unmapped": self.saveUnmap = i[1]
            if i[0].lower() == "gatk_tmp_opt": self.GATK_tmp_opt = i[1]
            if i[0].lower() == "clustermode" and i[1].upper() in ["SGE", "SLURM", "LOCAL"]: self.mode = i[1].upper()
            if i[0].lower() == "ploidy": self.ploidy = int(i[1])
            if i[0].lower() == "pool": self.pool = int(i[1])
            if i[0].lower() == "spikein": 
                try: self.spikein = i[1] 
                except: self.spikein = None
            if i[0].lower() == "atac": 
                if i[1].lower() == "true" or i[1].lower() == "y" or i[1] == "1": 
                    self.atac = True
            if i[0].lower() == "rrbs": 
                if i[1].lower() == "true" or i[1].lower() == "y" or i[1] == "1": 
                    self.rrbs = True
            if i[0].lower() == "supervise": 
                if i[1].lower() == "true" or i[1].lower() == "y" or i[1] == "1": 
                    self.superviseAll = True
            if i[0].lower() == "type":
                if "atac" in i[1].lower(): 
                    self.atac = True 
                elif "rrbs" in i[1].lower(): 
                    self.rrbs = True
                else: 
                    self.atac = False
                    self.rrbs = False
            if i[0].lower() == "scatter" or i[0].lower() == "chunking": 
                self.chunking = bool(int(i[1]))
            if i[0].lower() == "chunksize": 
                self.chunkSize = int(i[1])
            if i[0].lower() == "chunkovlp": 
                self.chunkOvlp = int(i[1])
            if i[0].lower() == "scattersize": self.scatterSize = int(i[1])
            if i[0].lower() == "graph": 
                self.graph_genome_path = i[1]
                self.do_graph_alignment = "y"
            if i[0].lower() == "GPC_graph": 
                self.GPC_graph = i[1]
                if self.GPC_graph[-1] != "/": self.GPC_graph = self.GPC_graph + "/"
                self.do_graph_alignment = "y"
            if i[0].lower() == "graphpipeline":
                if bool(int(i[1])): 
                    self.do_graph_alignment = "y"
            if i[0].lower() == "variantcaller": 
                self.vcaller = i[1]
            if i[0].lower() == "aligner": 
                self.aligner = i[1]
            if i[0].lower() == "gridss": 
                self.gridss = i[1]
                self.getsv = "y"
            if i[0].lower() == "delly": 
                self.delly = i[1]
                self.getsv = "y"
            if i[0].lower() == 'stageoutstep': 
                self.stageoutstep = i[1]
                try: self.stageoutstep = [int(k) for k in self.stageoutstep.split(',')]
                except: sys.exit('No numeric values passed in --stageoutStep.\nTry again.\n')
            if i[0].lower() == 'karyotype': 
                if "all" in i[0]: 
                    self.chromosomes = "all"
                else:
                    self.chromosomes = self.GetChromosomeIntervals(i[1])
            if i[0].lower() == "gatk-opts":
                funct, options = i[1].split("|") 
                self.GATKoptions[funct] = options
            
            print '{:20s} {:s}'.format(i[0], i[1])

        if self.do_graph_alignment == "y":
            if self.aligner.lower() == "graphaligner":
                self.aligner = "graphaligner"
            else:
                self.aligner = "vg"
            self.vcaller = "vg"
            print "\nA graph genome VCF has been specified.\nChanged aligner and variant caller to {}\n".format(self.aligner)
        
        if self.atac != 0 and self.rrbs != 0:
            print("Data are flagged as both ATAC and RRBS.")
            sys.exit("Please, check configuration file to specify the correct data type.")


        if int(self.nthreads) > int(self.nthreadsAlignment) and int(self.nthreadsAlignment) == 1:
            print 'Setting thread number for BWA to twice the given threads.'
            self.nthreadsAlignment = str(2 * int(self.nthreads))


    # Define chromosomal set by custom intervals
    def GetChromosomeIntervals(self, inputstr):
        if inputstr == "all":
            return inputstr
        inputstr = inputstr.split(',')
        chromosomes = []
        for chrom in inputstr:
            if '-' in chrom and len(chrom.split('-')) == 2: 
                chrom = chrom.split('-')
                if chrom[0].isdigit() and chrom[1].isdigit(): chromosomes += [str(i) for i in xrange(int(chrom[0]), int(chrom[1]) + 1)]
            elif chrom.isdigit() or chrom.lower() == 'mt' or chrom.lower() == 'x' or chrom.lower() == 'y': chromosomes.append(chrom)
            else: continue
        return chromosomes

    # Import and return additional GATK options
    def getGATKoptions(self, inputstr):
        inputstr = inputstr.split(",")
        GATKfunctions = {"AddOrReplaceReadGroups".lower(): '',
                        "PrintReads".lower(): '',
                        "MarkDuplicates".lower(): '',
                        "BaseRecalibrator".lower(): '',
                        "ApplyBQSR".lower(): '',
                        "HaplotypeCaller".lower(): '',
                        "GenomicsDBImport".lower(): '',
                        "GenotypeGVCFs".lower(): '',
                        "GatherVcfs".lower(): '',
                        "ValidateVariants".lower(): '',
                        "VariantAnnotator".lower(): ''}
        for dat in inputstr:
            function, options = dat.split("|")
            if function.lower() not in GATKfunctions.keys(): continue
            GATKfunctions[function.lower()] = options
        
        return GATKfunctions

    #### End class to parse arguments.




