import sys
import os
import gzip

def countFqBases(fq):
    if ".gz" in fq:
        opn = gzip.open
    else:
        opn = open
    sys.stderr.write("{}\n".format(fq))
    nbp = 0
    for n, f in enumerate(opn(fq)):
        if n % 4 == 1: nbp += len(f)
    return nbp
    

def parser():
    import argparse as agp
    parser = agp.ArgumentParser()
    parser.add_argument("-s", "--sample", metavar = 'samplename', type = str, help = 'Input sample to process',\
                             dest = 'sample', required = False, default=None)
    parser.add_argument("-p", "--path", metavar = '/PATH/TO/SAMPLE/FLD', type = str, help = 'Path to sample folder with fq files to process',\
                             dest = 'path', required = False, default=None)
    parser.add_argument("-l", "--list", metavar = 'sampleList.txt', type = str, help = 'List of samples and paths to process.',\
                             dest = 'list', required = False, default=None)
    args = parser.parse_args()
    return args


def main():
    args = parser()
    if args.list is not None:
        samples = {line.strip().split()[0]: [line.strip().split()[1], 0] for line in open(args.list)}
    elif args.path is not None and args.sample is not None and os.path.exists(args.path):
        samples = { args.sample: [args.path, 0] }
    elif args.path is not None and args.sample is not None and not os.path.exists(args.path):
        sys.exit("ERROR: path for {} does not exists.\nTry again.\n\n".format(args.sample))
    else:
        sys.exit("No parameter provided.")

    for sample in samples:
        path = samples[sample][0]
        fqlist = [i for i in os.listdir("{}".format(path)) if i.strip().split(".")[-1] == "fq" or i.strip().split(".")[-1] == "fastq" or i.strip().split(".")[-2] == "fastq" or i.strip().split(".")[-2] == "fq" or i.strip().split(".")[-1] == ".gz" ]
        samples[sample][1] += sum([ countFqBases(os.path.join(path, fq)) for fq in fqlist ])
        sys.stderr.write("{}\t{}\n".format(sample, samples[sample][1]))
        print("{}\t{}".format(sample, samples[sample][1]))
    return 0


if __name__ == "__main__":
    main()

