###
# This script check whether the logs from GenomicDBImport and 
# GenotypeGVCFs are complete.
# To run, simply:
#
#   python CheckLogs.py MyQsubCommands.txt
#
# where MyQsubCommands.txt is the file generated from JointTyper.py
###


def main():
    import sys, os

    try:
        a = ['./LOGS/{}'.format(i) for i in os.listdir("LOGS/") if ".err" in i and ("hc_" in i or "fb_" in i)]
    except:
        sys.exit("No logs found. Exit.\n")
    for i in a:
        complete = 0
        for line in open(i):
            if 'Import completed!' in line or "Traversal complete." in line: complete = 1
        if not complete: print "Check {}".format(i)




if __name__ == "__main__":
    main()


