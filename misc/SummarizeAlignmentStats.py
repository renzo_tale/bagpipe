import sys
import os

listdirs = [i for i in os.listdir("./ALIGN") if not os.path.isfile(i) and i[0] != "."]

samples = []
values = {}

for n, folder in enumerate(listdirs):
        try: 
                files = os.listdir(os.path.join("./ALIGN",folder))
        except OSError:
                continue
        samples += [folder]
        fnames = [f for f in files if ".alignmentStats" in f]
        if len(fnames) == 0: continue
        for fname in fnames:
                cd = '/'.join([folder,fname])
                values[cd] = {}
                for e, line in enumerate(open(os.path.join("./ALIGN",folder,fname))):
                        line = line.strip().split(' ')
                        if e == 0:
                                values[cd]["total"] = line[0]
                        if e == 1:
                                values[cd]["secondary"] = line[0]
                        if e == 4:
                                values[cd]["mapped"] = line[0]
                                values[cd]["mapped%"] = line[4].lstrip("(")
                        if e == 5: 
                                values[cd]["paired"] = line[0]
                        if e == 8:
                                values[cd]["prop.paired"] = line[0]
                                values[cd]["%prop.paired"] = line[5].lstrip("(")

print("Sample;Total;Secondary;Mapped;Mapped%;paired;prop_paired;%prop_paired")
keys = values.keys()
for sample in keys:
        print("{};{};{};{};{};{};{};{}".format(sample, values[sample]["total"], values[sample]["secondary"], values[sample]["mapped"], values[sample]["mapped%"], values[sample]["paired"], values[sample]["prop.paired"], values[sample]["%prop.paired"]))