#!/bin/bash
#
#Grid Engine options (lines prefixed with #$ or #!)
#$ -cwd
#$ -l h_rt=24:00:00
#$ -pe sharedmem 1
#$ -R y
#$ -l h_vmem=32.0G
#$ -o ./LOGS/$JOB_NAME.$JOB_ID.$TASK_ID.out
#$ -e ./LOGS/$JOB_NAME.$JOB_ID.$TASK_ID.err
#$ -P roslin_ctlgh
#$ -N BaseCount
. /etc/profile.d/modules.sh
module load anaconda 
source activate DataPy



sample=`head -n $SGE_TASK_ID $1 | tail -1 | awk '{print $1}'`
fld=`head -n $SGE_TASK_ID $1 | tail -1 | awk '{print $2}'`

if [ ! -e TMP ]; then mkdir TMP; fi

python CountBases.py -s $sample -p $fld > ./TMP/${sample}.$SGE_TASK_ID.txt
