###
# This script check whether the logs from GenomicDBImport and 
# GenotypeGVCFs are complete.
# To run, simply:
#
#   python CheckLogs.py MyQsubCommands.txt
#
# where MyQsubCommands.txt is the file generated from JointTyper.py
###


def main():
    import sys, os

    for n, line in enumerate(open(sys.argv[1])):
            if n == 0: continue
            pid = line.strip().split()[-1].split(".")[0]
            try:
                    a = ['./LOGS/{}'.format(i) for i in os.listdir("LOGS/") if "{}.".format(pid) in i and ".err" in i]
            except:
                    print pid
                    sys.exit()
            if len(a) == 1:
                    if n % 2 == 1:
                            complete = 0
                            for line in open(a):
                                    if 'Import completed!' in line: complete = 1
                            if not complete: print "Check {}".format(a)
                    else:
                            complete = 0
                            for line in open(a):
                                    if 'Traversal complete.' in line: complete = 1
                            if not complete: print "Check {}".format(a) 
            else:
                    for m,i in enumerate(a):
                            complete = 0
                            for line in open(i):
                                    if 'Import completed!' in line or "Traversal complete." in line: complete = 1
                            if not complete: print "Check {}".format(i)




if __name__ == "__main__":
    main()


