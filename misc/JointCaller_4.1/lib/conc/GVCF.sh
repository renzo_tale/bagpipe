#!/bin/bash
#
#Grid Engine options (lines prefixed with #$ or #!)
#$ -N gg_000001
#$ -cwd
#$ -l h_rt=47:00:00
#$ -pe sharedmem 1
#$ -R y
#$ -l h_vmem=48.0G
#$ -o ./LOGS/$JOB_NAME.$JOB_ID.out
#$ -e ./LOGS/$JOB_NAME.$JOB_ID.err
#$ -P roslin_ctlgh
. /etc/profile.d/modules.sh
module load java/jdk/1.8.0
module load igmm/apps/vcftools/0.1.13
module load igmm/apps/tabix

# 
# Start program
# 

flist=$1

if [ ! -e JOINEDVCF ]; then mkdir JOINEDVCF; fi

vcf-concat -f $flist | bgzip -c > ./JOINEDVCF/Joined.vcf.gz && tabix -p vcf ./JOINEDVCF/Joined.vcf.gz

while read i; do rm $i ${i}.tbi; done < $flist

echo "Variants joined."