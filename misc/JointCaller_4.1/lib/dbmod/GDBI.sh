#!/bin/bash
#
#Grid Engine options (lines prefixed with #$ or #!)
#$ -cwd
#$ -l h_rt=47:59:59
#$ -pe sharedmem 4
#$ -R y
#$ -l h_vmem=8.0G
#$ -P roslin_ctlgh
#$ -o ./LOGS/$JOB_NAME.$JOB_ID.$TASK_ID.out 
#$ -e ./LOGS/$JOB_NAME.$JOB_ID.$TASK_ID.err 
. /etc/profile.d/modules.sh
module load java/jdk/1.8.0

echo "File:      ./TMP/tmplist.txt"
echo "N samples: "`wc -l ./TMP/tmplist.txt`


itv=`head -$SGE_TASK_ID $1 | tail -1 | awk '{print $1":"$2"-"$3}'`
nname=`head -n $SGE_TASK_ID $1 | tail -1 | awk '{print $1"."$2"-"$3}'`
of=`realpath ./GENOMICDBI`

if [ -e ${of}/Join_${nname} ]; then
    /exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/gatk-4.1.6.0/gatk --java-options "-XX:ConcGCThreads=$NSLOTS -XX:ParallelGCThreads=$NSLOTS -Xmx24g" \
        GenomicsDBImport \
        --sample-name-map ./TMP/tmplist.txt \
        --genomicsdb-update-workspace-path ${of}/Join_${nname} \
        --tmp-dir $of \
        --reader-threads $NSLOTS \
        -L $itv 
else
    /exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/gatk-4.1.6.0/gatk --java-options "-XX:ConcGCThreads=$NSLOTS -XX:ParallelGCThreads=$NSLOTS -Xmx24g" \
        GenomicsDBImport \
        --sample-name-map ./TMP/tmplist.txt \
        --genomicsdb-workspace-path ${of}/Join_${nname} \
        --tmp-dir $of \
        --reader-threads $NSLOTS \
        -L $itv
fi




