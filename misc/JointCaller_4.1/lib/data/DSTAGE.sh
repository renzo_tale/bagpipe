#!/bin/bash
#
#Grid Engine options (lines prefixed with #$ or #!)
#$ -cwd
#$ -l h_rt=3:00:00
#$ -R y
#$ -l h_vmem=1.0G
#$ -o ./LOGS/$JOB_NAME.$JOB_ID.$TASK_ID.out
#$ -e ./LOGS/$JOB_NAME.$JOB_ID.$TASK_ID.err
#$ -P roslin_ctlgh
#$ -q staging

sample=`head -$SGE_TASK_ID $1 | tail -1 | awk '{print $1}'`
path=`head -$SGE_TASK_ID $1 | tail -1 | awk '{print $2}'`

if [ ! -e ./DATASTORE/$sample ]; then mkdir DATASTORE/$sample; fi
cp -f $path* ./DATASTORE/$sample
echo "Compied $sample in ./DATASTORE/$sample"

