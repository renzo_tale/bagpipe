# List of python configuration for the grid engine.

class GridEngine:
        def __init__(self):
                self.submissionCMD = "qsub"
                self.checkStatus = "qstat"
                self.deleteJID = "qdel"
                self.jobArrayN = "SGE_TASK_ID"
                self.jobCores = "NSLOTS"
                self.taskIdFlag = "TASK_ID"
                self.jobIdFlag = "JOB_ID"
                self.jobNameFlag = "JOB_NAME"
                self.checkOutcome = "qacct -j"
                self.name = "SGE"
                self.header = '''#!/bin/bash
#
# Grid Engine options (lines prefixed with #$ or #!)
#$ -N NAMEFILE
#$ -cwd
#$ -l h_rt=RTIME
#$ -pe sharedmem PROCN
#$ -R y
#$ -l h_vmem=MEMGBG
#$ -hold_jid JID
#$ -hold_jid_ad JAD
#$ -M MAILADDR
#$ -m SPWNOPT
#$ -t NJOBS
#$ -tc NCONCUR
#$ -o STOUT
#$ -e STERR
#$ -P PRJCT
#MODULES
#EXE
#ENV

trap 'exit 99' sigusr1 sigusr2 sigterm

# 
# Start program
# 
'''
                # Module list.
                self.importingScript = ". /etc/profile.d/modules.sh"
                self.importCommand = "module load"
                self.modules = {"samtools":"roslin/samtools/1.9",
                                "stringtie":"igmm/apps/stringtie/1.3.5",
                                "bamtools":"roslin/bamtools/2.4.2",
                                "fastqc":"roslin/fastqc/0.11.7",
                                "pigz":"igmm/apps/pigz/2.3.3",
                                "sratoolkit":"igmm/apps/sratoolkit/2.8.2-1",
                                "bwa":"roslin/bwa/0.7.17",
                                "java":"java/jdk/1.8.0",
                                "hisat2" : "igmm/apps/HISAT2/2.1.0",
                                "aspera":"igmm/apps/asperaconnect/3.7.2",
                                "vcftools":"igmm/apps/vcftools/0.1.13",
                                "picard":"igmm/apps/picard/1.139",
                                "trimmomatic":"igmm/apps/trimmomatic/0.36",
                                "last":"igmm/apps/last",
                                "tabix" : "igmm/apps/tabix", 
                                "gcc5": "igmm/compilers/gcc/5.5.0",
                                "gcc": "igmm/compilers/gcc/5.5.0",
                                "htslib": "igmm/libs/htslib/1.9",
                                "bismark":"igmm/apps/bismark/0.18.1",
                                "bowtie2": "igmm/apps/bowtie/2.3.1",
                                "ncurses": "igmm/libs/ncurses/6.0",
                                "bedtools": "igmm/apps/BEDTools/2.27.1",
                                "bcftools": "roslin/bcftools/1.9",
                                "cmake": "igmm/apps/cmake/3.12.2",
                                "R":"R",
                                "R3.5": "igmm/apps/R/3.5.0",
                                "anaconda": "anaconda"}
                self.localExe = {"LASTZ": "/exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/lastz-distrib-1.04.00/src/lastz_32",
                                "UCSC":"/exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/UCSC/",
                                "minimap2": "/exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/minimap2/",
                                "freebayes": "/exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/freebayes/bin",
                                "vg": "/exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/vg",
                                "genrich" : "/exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/Genrich",
                                "trimgalore": "/exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/TrimGalore-0.6.3",
                                "gatk": "/exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/gatk-4.1.6.0"}
                self.localEnv = {"CondaEnv": "DataPy3", "GraphPeakCallerEnv":"graph_peak_caller"}
        

        @staticmethod
        def processJobOutput(outputs):
                from lib.Utilities import memToInt
                tasks = {}
                taskid = ""
                exit_status = ""
                failed = ""
                maxvmem = ""
                for line in outputs:
                        if "=" * 30 in line and len(tasks) != 0:
                                tasks[taskid] = [failed, exit_status, maxvmem]
                                continue
                        elif "taskid" in line:
                                taskid = line.strip().split()[1]
                        elif "failed" in line:
                                failed = line.strip().split()[1]
                        elif "exit_status" in line:
                                exit_status = line.strip().split()[1]
                        elif "maxvmem" in line:
                                maxvmem = memToInt(line.strip().split()[1])
                                

                return None




class SlurmEngine:
    def __init__(self):
        self.submissionCMD = "sbatch"
        self.checkStatus = "squeue"
        self.deleteJID = "scancel"
        self.jobArrayN = "SLURM_ARRAY_TASK_ID"
        self.jobCores = "SLURM_NPROCS"
        self.taskIdFlag = "SLURM_TASK_ID"
        self.jobIdFlag = "SLURM_JOB_ID"
        self.jobNameFlag = "SLURM_JOB_NAME"
        self.checkOutcome = "sacct -j"
        self.name = "SLURM"
        self.header = '''#!/bin/bash
#
# SLURM options (lines prefixed with #SBATCH)
#SBATCH --job-name NAMEFILE
#SBATCH -t RTIME
#SBATCH -n PROCN
#SBATCH --mem-per-cpu=MEMGBG
#SBATCH --depend=after:JID
#SBATCH --depend=aftercorr:JAD
#SBATCH --mail-user MAILADDR
#SBATCH --mail-type SPWNOPT
#SBATCH --array=NJOBS%NCONCUR
#SBATCH -o STOUT
#SBATCH -e STERR
#SBATCH --wckey PRJCT
#MODULES
#EXE
#ENV

trap 'exit 99' sigusr1 sigusr2 sigterm

# 
# Start program
# 
'''
        # Module list.
        self.importingScript = ". /etc/profile.d/modules.sh"
        self.importCommand = "module load"
        self.modules = {"samtools":"roslin/samtools/1.9",
                    "stringtie":"igmm/apps/stringtie/1.3.5",
                    "bamtools":"roslin/bamtools/2.4.2",
                    "fastqc":"roslin/fastqc/0.11.7",
                    "pigz":"igmm/apps/pigz/2.3.3",
                    "sratoolkit":"igmm/apps/sratoolkit/2.8.2-1",
                    "bwa":"roslin/bwa/0.7.17",
                    "java":"java/jdk/1.8.0",
                    "hisat2" : "igmm/apps/HISAT2/2.1.0",
                    "aspera":"igmm/apps/asperaconnect/3.7.2",
                    "vcftools":"igmm/apps/vcftools/0.1.13",
                    "picard":"igmm/apps/picard/1.139",
                    "trimmomatic":"igmm/apps/trimmomatic/0.36",
                    "last":"igmm/apps/last",
                    "tabix" : "igmm/apps/tabix", 
                    "gcc5": "igmm/compilers/gcc/5.5.0",
                    "gcc": "igmm/compilers/gcc/5.5.0",
                    "htslib": "igmm/libs/htslib/1.9",
                    "bismark":"igmm/apps/bismark/0.18.1",
                    "bowtie2": "igmm/apps/bowtie/2.3.1",
                    "ncurses": "igmm/libs/ncurses/6.0",
                    "bedtools": "igmm/apps/BEDTools/2.27.1",
                    "bcftools": "roslin/bcftools/1.9",
                    "cmake": "igmm/apps/cmake/3.12.2",
                    "R":"R",
                    "R3.5": "igmm/apps/R/3.5.0",
                    "anaconda": "anaconda"}
        self.localExe = {"LASTZ": "/exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/lastz-distrib-1.04.00/src/lastz_32",
                    "UCSC":"/exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/UCSC/",
                    "minimap2": "/exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/minimap2/",
                    "freebayes": "/exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/freebayes/bin",
                    "vg": "/exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/vg",
                    "genrich" : "/exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/Genrich",
                    "trimgalore": "/exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/TrimGalore-0.6.3",
                    "gatk": "/exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/gatk-4.0.11.0"}
        self.localEnv = {"CondaEnv": "DataPy3"}

