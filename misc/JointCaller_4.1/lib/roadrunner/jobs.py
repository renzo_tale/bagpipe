import os

# Define a job subclass
class job:
    def __init__(self):
        import os
        self.jobs = {
            "Issued": {}, 
            "Queued": {}, 
            "Running": {},
            "Succeed": {}, 
            "Failed": {},
            "Killed": {}
            }
        self.njobs = 0
        self.nprocessing = 0
        self.nsub = 0
        self.nrun = 0
        self.nfinished = 0
        self.checkpointer = "./.rr"
        self.archives = {}
        if os.path.exists(self.checkpointer):
            self.loadArchive()
            self.loadCheckPoint()

    # Method to add a new command to run
    def add(self, cmd):
        toAdd, tmppid, tmpstatus = self.screenArchive(cmd)
        if toAdd:
            print("Command: {}\nhas been previously run and {} with process {}.".format(cmd, tmpstatus, tmppid))
            if tmpstatus == "Succeed":
                print("Process will not be submitted again.")
                return False
        else:
            print("Command: {}\nis new.".format(cmd))
        if cmd not in [self.jobs["Issued"][n] for n in self.jobs["Issued"]]: 
            self.jobs["Issued"][self.njobs] = [cmd, None]
            self.njobs += 1
            return True

    # Return total number of issued jobs
    def issued(self):
        return len(self.jobs["Issued"])


    # Return total number of submitted, running and completed jobs
    def get_n_processes(self):
        return self.nsub + self.nrun + self.nfinished

    # Method to reset the list of jobs
    def reset(self):
        self.dumpArchive()
        self.jobs = {
            "Issued": {}, 
            "Queued": {}, 
            "Running": {},
            "Succeed": {},
            "Failed": {},
            "Killed": {}
            }
        self.njobs = 0
        self.nprocessing = 0
        self.nsub = 0
        self.nrun = 0
        self.nfinished = 0
        

    # Method to add submitted details
    def submitted(self, n, PID):
        self.jobs["Queued"][n] = [self.jobs["Issued"][n], PID]
        self.nsub += 1
        self.nprocessing += 1
        
    # Method to add running details
    def running(self, n):
        if n not in self.jobs["Running"] and n in self.jobs["Queued"]:
            self.jobs["Running"][n] = self.jobs["Queued"][n]
            del self.jobs["Queued"][n]
            self.nsub -= 1
            self.nrun += 1

    # Method to add success details
    def succeed(self, n):
        if n not in self.jobs["Succeed"] and n in self.jobs["Running"]:
            self.jobs["Succeed"][n] = self.jobs["Running"][n]
            del self.jobs["Running"][n]
            self.nrun -= 1
            self.nfinished += 1
        if n not in self.jobs["Succeed"] and n in self.jobs["Queued"]:
            self.jobs["Succeed"][n] = self.jobs["Queued"][n]
            del self.jobs["Queued"][n]
            self.nsub -= 1
            self.nfinished += 1

    # Add killed job to list
    def killed(self, n):
        if n in self.jobs["Running"]:
            self.jobs["Killed"][n] = self.jobs["Running"][n]
            del self.jobs["Running"][n]
            self.nrun -= 1
            self.nfinished += 1
        elif n in self.jobs["Queued"]:
            self.jobs["Killed"][n] = self.jobs["Queued"][n]
            del self.jobs["Queued"][n]
            self.nsub -= 1
            self.nfinished += 1

    # Method to add failure details
    def failed(self, n):
        if n not in self.jobs["Failed"] and n in self.jobs["Running"]:
            self.jobs["Failed"][n] = self.jobs["Running"][n]
            del self.jobs["Running"][n]
            self.nrun -= 1
            self.nfinished += 1

    def getNext(self):
        if self.nprocessing > len(self.jobs["Issued"]):
            return None, None
        return self.nprocessing, self.jobs["Issued"][self.nprocessing][0]
        

    # Return running jobs
    def get_running(self, n = None):
        if n is None:
            return self.jobs["Running"].keys() + self.jobs["Queued"].keys()
        else:
            if n in self.jobs["Running"]: return self.jobs["Running"][n]
            else: return self.jobs["Queued"][n]

        
    # Return running jobs
    def get_finished(self, n = None):
        if n is None:
            return self.jobs["Succeed"].keys() + self.jobs["Failed"].keys()
        else:
            if n in self.jobs["Failed"]: return self.jobs["Failed"][n]
            else: return self.jobs["Succeed"][n]

    # Load checkpoint from current save file.
    def loadCheckPoint(self):
        inchkp = open( os.path.join(self.checkpointer, "rrcp") , "r")
        for line in inchkp:
            cmd, status, iid, pid = line.strip().split("$%$")
            if pid == "NOPID": pid = None  
            self.jobs[status][int(iid)] = [cmd, pid]
            self.njobs += 1
            self.nprocessing += 1
            if status == "Succeed" or status == "Failed" or status == "Killed":
                self.nfinished += 1
            elif status == "Running":
                self.nrun += 1
            elif status == "Queued":
                self.nsub += 1 
        return None

    # Save checkpoint of current situation on a text file.
    def saveCheckpoint(self):
        if not os.path.exists(self.checkpointer):
            os.mkdir(self.checkpointer)
            os.mkdir( os.path.join( self.checkpointer, "stages" ) )
        outchkp = open( os.path.join(self.checkpointer, "rrcp") , "w")
        for NJOB in range(0, self.njobs):
            JOB = self.jobs["Issued"][NJOB][0]
            STATUS = ["Succeed" if NJOB in self.jobs["Succeed"] 
                        else "Killed" if NJOB in self.jobs["Killed"] 
                        else "Failed" if NJOB in self.jobs["Failed"] 
                        else "Queued" if NJOB in self.jobs["Queued"] 
                        else "Running" if NJOB in self.jobs["Running"] 
                        else "Issued"][0]
            PID = [self.jobs["Succeed"][NJOB][1] if NJOB in self.jobs["Succeed"] 
                    else self.jobs["Killed"][NJOB][1] if NJOB in self.jobs["Killed"] 
                    else self.jobs["Failed"][NJOB][1] if NJOB in self.jobs["Failed"] 
                    else self.jobs["Queued"][NJOB][1] if NJOB in self.jobs["Queued"] 
                    else self.jobs["Running"][NJOB][1] if NJOB in self.jobs["Running"] 
                    else "NOPID"][0]
            outchkp.write("{0}$%${1}$%${2}$%${3}\n".format(JOB, STATUS, NJOB, PID))
        return None

    # Method to screen archive for former jobs.
    def screenArchive(self, cmd):
        for archive in self.archives:
            try:
                result = self.archives[archive].keys()[0]
                iid = self.archives[archive][result].keys()[0]
                oldcmd, pid = self.archives[archive][result][iid]
                if cmd == oldcmd:
                    return True, pid, result
            except:
                continue
        return False, None, None

    # Load archive of jobs.
    def loadArchive(self):
        archives = [i.strip()[0:25] for i in open( os.path.join(self.checkpointer, "rrck") , "r") ]
        for archive in archives:
            self.archives[archive] = {}
            for line in open( os.path.join(self.checkpointer, "stages", archive), "r"):
                cmd, status, iid, pid = line.strip().split("$%$")
                if pid == "NOPID": pid = None  
                self.archives[archive] = {status: { int(iid) : [cmd, pid] } }
        return None

    # Dump data to archive of jobs.
    def dumpArchive(self):
        if self.njobs == 0: return None
        import binascii
        found = 0
        while found == 0 :
            rckName = str(binascii.b2a_hex(os.urandom(15)).decode())[0:25]
            if rckName not in os.listdir(os.path.join(self.checkpointer, "stages")): 
                found = 1
        of = open(os.path.join(self.checkpointer, "rrck"), "a")
        of.write("{}\n".format(rckName))
        of.close()
        
        outArchive = open(os.path.join(self.checkpointer, "stages", rckName) , "w")
        for NJOB in range(0, self.njobs):
            JOB = self.jobs["Issued"][NJOB][0]
            STATUS = ["Succeed" if NJOB in self.jobs["Succeed"] 
                        else "Failed" if NJOB in self.jobs["Failed"] 
                        else "Killed" if NJOB in self.jobs["Killed"] 
                        else "Queued" if NJOB in self.jobs["Queued"] 
                        else "Running" if NJOB in self.jobs["Running"] 
                        else "Issued"][0]
            PID = [self.jobs["Succeed"][NJOB][1] if NJOB in self.jobs["Succeed"] 
                    else self.jobs["Queued"][NJOB][1] if NJOB in self.jobs["Queued"] 
                    else self.jobs["Running"][NJOB][1] if NJOB in self.jobs["Running"] 
                    else "NOPID"][0]
            outArchive.write("{0}$%${1}$%${2}$%${3}\n".format(JOB, STATUS, NJOB, PID))
        return None








