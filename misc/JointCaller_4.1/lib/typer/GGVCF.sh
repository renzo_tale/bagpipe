#/bin/sh
# Grid Engine options (lines prefixed with #$)
#$ -cwd
#$ -l h_rt=47:59:59
#$ -pe sharedmem 4
#$ -R y
#$ -l h_vmem=8G
#$ -P roslin_ctlgh
#$ -o ./LOGS/$JOB_NAME.$JOB_ID.$TASK_ID.out 
#$ -e ./LOGS/$JOB_NAME.$JOB_ID.$TASK_ID.err 

. /etc/profile.d/modules.sh
module load java/jdk/1.8.0

reference=$1
nname=`head -n $SGE_TASK_ID $2 | tail -1 | awk '{print $1"."$2"-"$3}'`
of=`realpath ./GENOMICDBI`


if [ ! -e "./VCFs" ]; then mkdir ./VCFs; fi

/exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/gatk-4.0.11.0/gatk \
    --java-options "-XX:ConcGCThreads=$NSLOTS -XX:ParallelGCThreads=$NSLOTS -Xmx24g" \
    GenotypeGVCFs \
    -V gendb://${of}/Join_${nname} \
    -R $reference \
    --tmp-dir `pwd`/VCFs \
    --use-new-qual-calculator \
    -O ./VCFs/Joined_${nname}.vcf.gz && rm -r ${of}/Join_${nname}

echo ./VCFs/Joined_${nname}.vcf.gz >> allVCFs.txt

echo "Created "./VCFs/Joined_${nname}.vcf.gz
