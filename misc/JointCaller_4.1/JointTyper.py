def qstat():
    import subprocess as sbp
    stats = sbp.check_output(["qstat"])
    stats = stats.splitlines()
    return stats


def GetChromosomeIntervals(inputstr):
    if inputstr == "all":
        return inputstr
    inputstr = inputstr.split(',')
    chromosomes = []
    for chrom in inputstr:
        if '-' in chrom and len(chrom.split('-')) == 2: 
            chrom = chrom.split('-')
            if chrom[0].isdigit() and chrom[1].isdigit(): chromosomes += [str(i) for i in range(int(chrom[0]), int(chrom[1]) + 1)]
        elif chrom.isdigit() or chrom.lower() == 'mt' or chrom.lower() == 'x' or chrom.lower() == 'y': chromosomes.append(chrom)
        else: continue
    return chromosomes


def intervals(refGenome, winsize, ovlp):
    bpi = []
    bpe = []
    chrs = []
    for line in open(refGenome + ".fai"):
        chrom, length = line.strip().split()[0:2]
        length = int(length)
        if length < winsize:
            bpi += [1]
            bpe += [length]
            chrs += [chrom]
            continue
        nbpi = list(range(1, length + 1, winsize))
        bpi += nbpi
        bpe += [nbpi[i] + ovlp - 1 for i in range(1, len(nbpi))] + [length]
        chrs += [chrom for i in range(0, len(nbpi))]
    
    of = open("./Intervals.txt", "w")
    for k in range(0, len(chrs)):
        of.write("{}\t{}\t{}\n".format(chrs[k], bpi[k], bpe[k]))
    return "./Intervals.txt"


def filterItvs(intervals, karyotype):
    fname = "./Intervals.filt.txt"
    outf = open(fname, "w")
    for line in open(intervals):
        chrom = line.strip().split()[0]
        if chrom in karyotype:
            outf.write(line)
    outf.close()
    return fname


def creates_pname(rootname, rnum):
    n = "%06d" % rnum
    name = rootname[0:2] + "_" + n
    return name


def checkFiles(filelist):
    import os
    pairs = {}
    success = []
    noTbi = []
    noGvcf = []
    total = 0
    for line in open(filelist):
        total += 1
        sample, fname = line.strip().split()
        pairs[sample] = fname
        if os.path.exists(fname) and os.path.exists(fname + ".tbi"):
            success.append(sample)
        if not os.path.exists(fname):
            noGvcf.append(sample)
        if not os.path.exists(fname + ".tbi"):
            noTbi.append(sample)
    print("Samples in the list: {}".format(total))
    print("Complete file sets: {}".format(len(success)))
    print("Missing tbi: {}".format(len(noTbi)))
    for i in noTbi:
        print(i, pairs.get(i))
    print("Missing gvcf: {}".format(len(noGvcf)))
    for i in noGvcf:
        print(i, pairs.get(i))


def get_sample_data(fname, samplen):
    for n, line in enumerate(open(fname)):
        if n + 1 == samplen:
            sampleID, sampleP = line.strip().split()
            sampleF = sampleP.split("/")[-1]
            return sampleID, "./DATASTORE/{}".format(sampleF)


def parser():
    import argparse as agp
    parser = agp.ArgumentParser()
    parser.add_argument("-s", "--samples", metavar = 'samplelist.txt', type = str, help = 'Input list of gvcfs for GenomicDBImport',\
                             dest = 'samples', required = True)
    parser.add_argument("-i", "--intervals", metavar = 'intervallist.txt', type = str, help = 'List of intervals to process',\
                             dest = 'itvs', required = False)
    parser.add_argument("-l", "--limit-to", metavar = 'X[,Y,Z,...]', type = str, help = 'Limit to specific intervals (comma separated or ranges)',\
                             dest = 'limit', required = False, default = None)
    parser.add_argument("-r", "--ref", metavar = '/path/to/reference.fasta', type = str, help = 'Fasta reference genome.',\
                             dest = 'reference', required = True)
    parser.add_argument("-w", "--winsize", metavar = 'windowBP', type = int, help = 'Size of the interval to process, (if a list is not provided).',\
                             dest = 'winsize', required = False, default = 10000000)
    parser.add_argument("-o", "--overlap", metavar = 'overlapBP', type = int, help = 'Size of the overlap between intervals to process (if a list is not provided).',\
                             dest = 'overlap', required = False, default = 0)
    parser.add_argument("-c", "--concurrent-intervals", metavar = 'nItvs', type = int, help = 'Number of concurrent intervals to process (only for new method).',\
                             dest = 'concur', required = False, default = 1)
    parser.add_argument("-C", "--concurrent-samples", metavar = 'nSamples', type = int, help = 'Number of concurrent samples to process (only for new method).',\
                             dest = 'samp_concur', required = False, default = 1)
    parser.add_argument("-k", "--karyotype", metavar = '1-N,X,Y', type = str, help = 'Limit analysis to given karyotype.',\
                             dest = 'karyo', required = False, default = None)
    parser.add_argument("-m", "--method", metavar = 'old/new', type = str, help = 'Use array-bound (new, default) or job-bound (old) locking.',\
                             dest = 'method', required = False, default = "new", choices=["old", "new"])
    parser.add_argument("--cluster", metavar = 'SGE/SLURM', type = str, help = 'Cluster engine of choice (SGE or SLURM).',\
                             dest = 'clustermode', required = False, default = "SGE", choices=["SGE", "SLURM"])
    args = parser.parse_args()
    return args





def method(args):
    import sys
    import subprocess as sbp
    import os
    import random as rn
    import time as tm
    from lib.roadrunner.RoadRunner import roadRunnerParallel
    from lib.GridOptions import GridEngine, SlurmEngine

    if args.clustermode == "SGE":
        engine = GridEngine()
    elif args.clustermode == "SLURM":
        engine = SlurmEngine()

    roadrunner = roadRunnerParallel(engine)

    nsamples = sum([1 for line in open(args.samples)])
    print("Reading intervals from: {}".format(args.itvs))
    nitvs = sum([1 for line in open(args.itvs)])


    intervals = range(0, nitvs, args.concur)
    if args.limit is not None:
        print("Limiting to some specific intervals")
        intervals = [ int(i) - 1 for i in GetChromosomeIntervals(args.limit) ]
        nitvs = len(intervals)
        args.concur = 1
        print("Setting concurrency of intervals to 1")

    print("Processing {} intervals in blocks of {}".format(nitvs, args.concur))

    # Generate random numbers
    print("Start iteration\n")
    for nitv in range(0, nitvs, args.concur):
        i_init = nitv + 1
        e_init = nitv + args.concur
        if e_init > nitvs: e_init = nitvs
        for nsample in range(1, nsamples + 1, args.samp_concur):
            i_samp = nsample
            e_samp = nsample + args.samp_concur - 1
            if e_samp > (nsamples + 1): e_samp = nsamples

            p1name = "DS_I_{}_{}_S_{}_{}".format(i_init, e_init, i_samp, e_samp)
            p2name = "GI_I_{}_{}_S_{}_{}".format(i_init, e_init, i_samp, e_samp)


            roadrunner.parse(args.samples, script_name="./lib/data/DSTAGE.sh", jobname=p1name, options="-t {}-{}".format(i_samp, e_samp))
            cmd, added = roadrunner.saveCommand()
            if added: roadrunner.run(1)
            roadrunner.reset("all")

            # Creating filelists
            of = open("./TMP/tmplist.txt", "w")
            for subdir in os.listdir("./DATASTORE"):
                tmpfilename = [ '{}/DATASTORE/{}/{}'.format(os.getcwd(), subdir, fname) for fname in os.listdir("./DATASTORE/{}".format(subdir)) if ".vcf.gz" in fname and ".tbi" not in fname][0]
                of.write("{}\t{}\n".format(subdir, tmpfilename) )
            of.close()

            roadrunner.parse(args.itvs, script_name="./lib/dbmod/GDBI.sh", jobname=p2name, options="-t {}-{}".format(i_init, e_init))
            cmd, added = roadrunner.saveCommand()
            if added: roadrunner.run(1)
            roadrunner.reset("all")
            proc = sbp.Popen("rm -r ./DATASTORE/* ./TMP/tmplist.txt", shell = True)
            proc.wait()

        p3name = "GG_I_{}_{}".format(i_init, e_init)
        roadrunner.parse(args.reference, args.itvs, script_name="./lib/typer/GGVCF.sh", jobname=p3name, options="-t {}-{}".format(i_init, e_init))
        cmd, added = roadrunner.saveCommand()
        if added: roadrunner.run(1)
        roadrunner.reset("all")

    # Create VCF list.
    if args.limit is not None and not os.path.exists("vcflist.txt"):
        of = open("vcflist.txt", "w")
        for n,line in enumerate(open(args.itvs)):
            chrom, bpi, bpe = line.strip().split()
            of.write( "./VCFs/Joined_{}.{}-{}.vcf.gz\n".format(chrom, bpi, bpe) )
        of.close()
        
        roadrunner.parse("vcflist.txt", script_name="./lib/conc/GVCF.sh", jobname="GV")
        cmd, added = roadrunner.saveCommand()
        if added: roadrunner.run(1)
    else:
        roadrunner.parse("vcflist.txt", script_name="./lib/conc/GVCF.sh", jobname="GV")
        cmd, added = roadrunner.saveCommand()
        print("File vcflist.txt exists.")
        print("To combine the files just rerun the following command:")
        print(cmd)
    print("Bye")
    return 0


def main():
    import sys
    import subprocess as sbp
    import os
    import random as rn
    import time as tm

    # Generate folder structure
    try: os.mkdir('./GENOMICDBI/')
    except: 'Folder ./GENOMICDBI ready.'
    try: os.mkdir('./LOGS/')
    except: 'Folder ./LOGS ready.'
    try: os.mkdir('./VCFs/')
    except: 'Folder ./VCFs ready.'
    try: os.mkdir('./DATASTORE/')
    except: 'Folder ./DATASTORE ready.'
    try: os.mkdir('./TMP/')
    except: 'Folder ./TMP ready.'

    # Get arguments
    args = parser()

    # Generate intervals if they do not exists.
    if args.itvs is None:
        args.itvs = intervals(args.reference, args.winsize, args.overlap)

    # Filter intervals if required
    if args.karyo is not None:
        print( "Selecting chromosomes in {}".format(','.join(GetChromosomeIntervals(args.karyo))) )
        newitvs = filterItvs(args.itvs, GetChromosomeIntervals(args.karyo))
        args.itvs = newitvs

    # Checking input GVCFs
    checkFiles(args.samples)
    method(args)


if __name__ == "__main__":
    main()
    print("All done")