import sys
import os

if os.path.exists("./DEDUP"): 
        listdirs = [i for i in os.listdir("./DEDUP") if not os.path.isfile(i)]
        ischunk=False
        isDedup=True
        fld = "DEDUP"
elif os.path.exists("./CHUNK"): 
        listdirs = [i for i in os.listdir("./CHUNK") if not os.path.isfile(i)]
        ischunk=True
        isDedup=False
        fld = "CHUNK"

samples = []
values = {}

if sys.argv[1] != "all":
        chr_orig = "all"
        chromosomes = sys.argv[1].split(",")
else:
        chromosomes = "all"

for n, folder in enumerate(listdirs):
        files = os.listdir(os.path.join("./{}".format(fld),folder))
        if isDedup:
                fname = [f for f in files if ".avrgDepth" in f]
        elif ischunk:
                fname = [f for f in files if ".chunkDepths" in f]

        if len(fname) == 1: fname = fname[0]
        elif len(fname) == 0: continue
        samples += [folder]
        values[folder] = {}
        for e, line in enumerate(open(os.path.join("./{}".format(fld),folder,fname))):
                line = line.strip().split(' ')
                if chromosomes == "all":
                        values[folder][line[0]] = line[1]
                if chromosomes != "all" and line[0] in chromosomes:
                        values[folder][line[0]] = line[1]

if chromosomes == "all":
        chr_orig = "all"
        chromosomes = sorted(list(set([key for folder in values for key in values[folder]])))
        print("Sample;{}".format(';'.join(chromosomes)))
else:
        print("Sample;{}".format(';'.join(chromosomes)))

if "X" in chromosomes:
        males = open("./LISTS/males.txt", 'w')
        for sample in samples:
                print("{};{}".format(sample, ';'.join([values[sample].get(i,"0") for i in chromosomes])))
                chrXdepth = float(values[sample].get("X"))
                ratios = [chrXdepth / float(values[sample].get(chromosome)) for chromosome in chromosomes if chromosome != "X" and chromosome != "MT" and chromosome != "Y"]
                avgRatio = sum(ratios) / len(ratios)
                if avgRatio < 0.66:
                        males.write("{}\n".format(sample))
else:
        for sample in samples:
                print("{};{}".format(sample, ';'.join([values[sample].get(i,"0") for i in chromosomes])))
                

