import sys

indepth=sys.argv[1]

chrom_stats = {}
chrom_conv = {}


for e,i in enumerate(open(indepth)):
        contig, bp, depth = i.strip().split()
        if contig not in chrom_stats:
                chrom_stats[contig] = [int(depth), 1]
        else:
                chrom_stats[contig][0] += int(depth)
                chrom_stats[contig][1] += 1.0
        if (e + 1) % 10000000 == 0: 
                sys.stderr.write(str(e+1)+"\n")
                sys.stderr.write("Example Status: "+" ".join([chrom_stats.keys()[0],str(chrom_stats[chrom_stats.keys()[0]][0]),str(chrom_stats[chrom_stats.keys()[0]][1])]) + "\n")


for c in chrom_stats:
        print c, chrom_stats[c][0] / chrom_stats[c][1]