
def main():
    import sys
    import gzip
    tiles = {}
    bname = sys.argv[1].replace(".fq.gz", "").replace(".fastq.gz", "").split("/")[-1]
    path = '/'.join(sys.argv[2].split("/")[0:-1])
    if path == "": path = "."
    for tile in open(sys.argv[2]):
        tile = tile.strip()
        if tile not in tiles:
            tiles[tile] = gzip.open("{}/{}.{}.fastq.gz".format(path, tile, bname), "w")

    opn = open
    if ".gz" in sys.argv[1]:
        opn = gzip.open

    tmp = []
    for n, line in enumerate(opn(sys.argv[1])):
        if n > 0 and n % 4 == 3:
            tile = tmp[0].strip().split(":")[4]
            tiles[tile].write("{}\n".format('\n'.join(tmp)))
            continue
        tmp.append(line.strip())
    return 0



if __name__ == "__main__":
    main()
    pass