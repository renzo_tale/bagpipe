#!/bin/bash
#
#Grid Engine options (lines prefixed with #$ or #!)
#$ -cwd
#$ -l h_rt=24:00:00
#$ -pe sharedmem 1
#$ -R y
#$ -l h_vmem=32.0G
#$ -o ./LOGS/$JOB_NAME.$JOB_ID.$TASK_ID.out
#$ -e ./LOGS/$JOB_NAME.$JOB_ID.$TASK_ID.err
#$ -P roslin_ctlgh
. /etc/profile.d/modules.sh
module load java/jdk/1.8.0


filelist=$1
itv=`head -n $SGE_TASK_ID $2 | tail -1 | awk '{print $1":"$2"-"$3}'`
nname=`head -n $SGE_TASK_ID $2 | tail -1 | awk '{print $1"."$2"-"$3}'`
of=`realpath ./GENOMICDBI`

if [ -e ${of}/Join_${nname} ]; then rm -r ${of}/Join_${nname}; fi

/exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/gatk-4.0.11.0/gatk --java-options "-XX:ConcGCThreads=$NSLOTS -XX:ParallelGCThreads=$NSLOTS -Xmx24g" \
    GenomicsDBImport \
    --sample-name-map $filelist \
    --genomicsdb-workspace-path ${of}/Join_${nname} \
    --tmp-dir $of \
    --reader-threads $NSLOTS \
    -L $itv



