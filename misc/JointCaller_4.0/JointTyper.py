def qstat():
    import subprocess as sbp
    stats = sbp.check_output(["qstat"])
    stats = stats.splitlines()
    return stats

def GetChromosomeIntervals(inputstr):
    if inputstr == "all":
        return inputstr
    inputstr = inputstr.split(',')
    chromosomes = []
    for chrom in inputstr:
        if '-' in chrom and len(chrom.split('-')) == 2: 
            chrom = chrom.split('-')
            if chrom[0].isdigit() and chrom[1].isdigit(): chromosomes += [str(i) for i in range(int(chrom[0]), int(chrom[1]) + 1)]
        elif chrom.isdigit() or chrom.lower() == 'mt' or chrom.lower() == 'x' or chrom.lower() == 'y': chromosomes.append(chrom)
        else: continue
    return chromosomes


def intervals(refGenome, winsize, ovlp):
    bpi = []
    bpe = []
    chrs = []
    for line in open(refGenome + ".fai"):
        chrom, length = line.strip().split()[0:2]
        length = int(length)
        if length < winsize:
            bpi += [1]
            bpe += [length]
            chrs += [chrom]
            continue
        nbpi = list(range(1, length + 1, winsize))
        bpi += nbpi
        bpe += [nbpi[i] + ovlp - 1 for i in range(1, len(nbpi))] + [length]
        chrs += [chrom for i in range(0, len(nbpi))]
    
    of = open("./Intervals.txt", "w")
    for k in range(0, len(chrs)):
        of.write("{}\t{}\t{}\n".format(chrs[k], bpi[k], bpe[k]))
    return "./Intervals.txt"


def filterItvs(intervals, karyotype):
    fname = "./Intervals.filt.txt"
    outf = open(fname, "w")
    for line in open(intervals):
        chrom = line.strip().split()[0]
        if chrom in karyotype:
            outf.write(line)
    outf.close()
    return fname


def creates_pname(rootname, rnum):
    n = "%06d" % rnum
    name = rootname[0:2] + "_" + n
    return name


def checkFiles(filelist):
    import os
    pairs = {}
    success = []
    noTbi = []
    noGvcf = []
    total = 0
    for line in open(filelist):
        total += 1
        sample, fname = line.strip().split()
        pairs[sample] = fname
        if os.path.exists(fname) and os.path.exists(fname + ".tbi"):
            success.append(sample)
        if not os.path.exists(fname):
            noGvcf.append(sample)
        if not os.path.exists(fname + ".tbi"):
            noTbi.append(sample)
    print("Samples in the list: {}".format(total))
    print("Complete file sets: {}".format(len(success)))
    print("Missing tbi: {}".format(len(noTbi)))
    for i in noTbi:
        print(i, pairs.get(i))
    print("Missing gvcf: {}".format(len(noGvcf)))
    for i in noGvcf:
        print(i, pairs.get(i))



def parser():
    import argparse as agp
    parser = agp.ArgumentParser()
    parser.add_argument("-s", "--samples", metavar = 'samplelist.txt', type = str, help = 'Input list of gvcfs for GenomicDBImport',\
                             dest = 'samples', required = True)
    parser.add_argument("-i", "--intervals", metavar = 'intervallist.txt', type = str, help = 'List of intervals to process',\
                             dest = 'itvs', required = False)
    parser.add_argument("-r", "--ref", metavar = '/path/to/reference.fasta', type = str, help = 'Fasta reference genome.',\
                             dest = 'reference', required = True)
    parser.add_argument("-w", "--winsize", metavar = 'windowBP', type = int, help = 'Size of the interval to process, (if a list is not provided).',\
                             dest = 'winsize', required = False, default = 10000000)
    parser.add_argument("-o", "--overlap", metavar = 'overlapBP', type = int, help = 'Size of the overlap between intervals to process (if a list is not provided).',\
                             dest = 'overlap', required = False, default = 0)
    parser.add_argument("-c", "--concurrency", metavar = 'nJobs', type = int, help = 'Number of concurrent jobs to run (only for new method).',\
                             dest = 'concur', required = False, default = 200)
    parser.add_argument("-k", "--karyotype", metavar = '1-N,X,Y', type = str, help = 'Limit analysis to given karyotype.',\
                             dest = 'karyo', required = False, default = None)
    parser.add_argument("-m", "--method", metavar = 'old/new', type = str, help = 'Use array-bound (new, default) or job-bound (old) locking.',\
                             dest = 'method', required = False, default = "new", choices=["old", "new"])
    args = parser.parse_args()
    return args





def method(args):
    import sys
    import subprocess as sbp
    import os
    import random as rn
    import time as tm
    print("Reading intervals from: {}".format(args.itvs))
    nitvs = sum([1 for line in open(args.itvs)])

    # Generate random numbers
    print("Generating IDs...")

    rn1 = rn.randint(1, 999999)
    rn2 = rn.randint(1, 999999)
    rn3 = rn.randint(1, 999999)

    lista = []
    p1name = creates_pname("GI", rn1)
    p2name = creates_pname("GG", rn2)
    p3name = creates_pname("GV", rn3)
    qsubline1 = 'qsub -t 1-{3} -tc {4} -N {0} GDBI.sh {1} {2}'.format(p1name, args.samples, args.itvs, nitvs, args.concur)
    qsubline2 = 'qsub -t 1-{4} -tc {5} -N {0} -hold_jid_ad {1} GGVCF.sh {2} {3}'.format(p2name, p1name, args.reference, args.itvs, nitvs, args.concur)
    qsubline3 = 'qsub -N {0} -hold_jid {1},{2} GVCF.sh vcflist.txt'.format(p3name, p1name, p2name)
    lista.append(qsubline1)
    lista.append(qsubline2)
    lista.append(qsubline3)

    of = open("MyQsubCommands.txt", "w")
    of.write("QSUB\tPID\n")
    for n, i in enumerate(lista):
        p = sbp.Popen(i, shell = True, stdout=sbp.PIPE)
        stout = p.stdout.read().decode("utf-8")
        print(stout.strip())
        pid = stout.strip().split()[2]
        of.write("{}\t{}\n".format(i, pid))
        tm.sleep(0.5)
    of.close()

    of = open("vcflist.txt", "w")
    for n,line in enumerate(open(args.itvs)):
        chrom, bpi, bpe = line.strip().split()
        of.write( "./VCFs/Joined_{}.{}-{}.vcf.gz\n".format(chrom, bpi, bpe) )
    of.close()
    return 0


def old_method(args):
    import sys
    import subprocess as sbp
    import os
    import random as rn
    import time as tm
    print("Reading intervals from: {}".format(args.itvs))

    # Generate random numbers
    print("Generating IDs...")
    rN = list(range(1, sum([1 for line in open(args.itvs)]) + 1))

    list1 = []
    list2 = []
    for n,line in enumerate(open(args.itvs)):
        chrom, bpi, bpe = line.strip().split()
        itv = "{}:{}-{}".format(chrom, bpi, bpe)
        bpname = itv.replace(":", ".")
        p1name = creates_pname("GI", rN[n])
        p2name = creates_pname("GG", rN[n])

        qsubline1 = 'qsub -N {0} GDBI_single.sh {1} {2} {3}'.format(p1name, args.samples, itv, bpname)
        qsubline2 = 'qsub -N {0} -hold_jid {1} GGVCF_single.sh {2} {3}'.format(p2name, p1name, args.reference, bpname)
        list1.append(qsubline1)
        list2.append(qsubline2)

    of = open("MyQsubCommands.txt", "w")
    of.write("QSUB\tPID\n")
    for n,i in enumerate(list1):
        p1 = sbp.Popen(i, shell = True, stdout=sbp.PIPE)
        stout1 = p1.stdout.read().decode("utf-8")
        print(stout1.strip())
        pid1 = stout1.strip().split()[2]
        tm.sleep(0.5)
        p2 = sbp.Popen(list2[n], shell = True, stdout=sbp.PIPE)
        stout2 = p2.stdout.read().decode("utf-8")
        print(stout2.strip())
        pid2 = stout2.strip().split()[2]
        of.write("{}\t{}\n".format(i, pid1))
        of.write("{}\t{}\n".format(list2[n], pid2))
    of.close()

    of = open("vcflist.txt", "w")
    for n,line in enumerate(open(args.itvs)):
        chrom, bpi, bpe = line.strip().split()
        of.write( "./VCFs/Joined_{}.{}-{}.vcf.gz\n".format(chrom, bpi, bpe) )
    of.close()
    return 0


def main():
    import sys
    import subprocess as sbp
    import os
    import random as rn
    import time as tm

    # Generate folder structure
    try: os.mkdir('./GENOMICDBI/')
    except: 'Folder ./GENOMICDBI ready.'
    try: os.mkdir('./LOGS/')
    except: 'Folder ./LOGS ready.'
    try: os.mkdir('./VCFs/')
    except: 'Folder ./VCFs ready.'

    # Get arguments
    args = parser()

    # Generate intervals if they do not exists.
    if args.itvs is None:
        args.itvs = intervals(args.reference, args.winsize, args.overlap)

    # Filter intervals if required
    if args.karyo is not None:
        newitvs = filterItvs(args.itvs, GetChromosomeIntervals(args.karyo))
        args.itvs = newitvs

    # Checking input GVCFs
    checkFiles(args.samples)

    if args.method == "old":
        old_method(args)
    else:
        method(args)


if __name__ == "__main__":
    main()
    print("All done")