#/bin/sh
# Grid Engine options (lines prefixed with #$)
#$ -cwd
#$ -l h_rt=23:59:59
#$ -pe sharedmem 1
#$ -R y
#$ -l h_vmem=32G
#$ -P roslin_ctlgh
#$ -o LOGS/$JOB_NAME.$JOB_ID.out
#$ -e LOGS/$JOB_NAME.$JOB_ID.err

. /etc/profile.d/modules.sh
module load java/jdk/1.8.0

#filelist=$1

reference=$1
nname=$2
#reference="/exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/1000BullsRef/ARS-UCD1.2_Btau5.0.1Y.fasta"
of=`realpath ./GENOMICDBI`


if [ ! -e "./VCFs" ]; then mkdir ./VCFs; fi

/exports/cmvm/eddie/eb/groups/prendergast_grp/Andrea/gatk-4.0.11.0/gatk \
    --java-options "-XX:ConcGCThreads=$NSLOTS -XX:ParallelGCThreads=$NSLOTS -Xmx24g" \
    GenotypeGVCFs \
    -V gendb://${of}/Join_${nname} \
    -R $reference \
    --tmp-dir `pwd`/VCFs \
    --use-new-qual-calculator \
    -O ./VCFs/Joined_${nname}.vcf.gz && rm -r ${of}/Join_${nname}

echo ./VCFs/Joined_${nname}.vcf.gz >> allVCFs.txt

echo "Created "./VCFs/Joined_${nname}.vcf.gz
