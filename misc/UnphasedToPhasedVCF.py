import sys
import gzip
import argparse as agp

parser = agp.ArgumentParser()
parser.add_argument('-v','--vcf', metavar = 'file.vcf(.gz)', type = str, help = 'Specify vcf file (can be gzipped)', \
                            default = None, dest = 'vcf', required=True)
args = parser.parse_args()


if ".gz" in args.vcf:
    infile = gzip.open(args.vcf)
elif args.vcf == "stdin" or args.vcf == "-":
    infile = sys.stdin
else:
    infile = open(args.vcf)

checked = 0
PGTIDX = None

for line in infile:
    if "#" in line:
        print line.strip(); continue
    
    line = line.strip().split()

    # If PGT not in line, print and proceed 
    if "PGT" not in line[8]:
        print '\t'.join(line); continue

    PGTIDX = line[8].split(":").index("PGT")
    converted = []
    #for n, l in enumerate(line[9:]):
    #    if len(l.split(":"))==len(line[8].split(":")) and "|" in l.split(":")[PGTIDX]:
    #        converted += [':'.join([l.split(":")[PGTIDX]]+l.split(":")[1:])]
    #        continue
    #    converted += [l]

    converted = [
        ':'.join([l.split(":")[PGTIDX]]+l.split(":")[1:])
        if len(l.split(":"))==len(line[8].split(":")) and "|" in l.split(":")[PGTIDX] else l
        for l in line[9:]
    ]
    print '\t'.join(line[0:9] + converted)
