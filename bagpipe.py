#########################################
####            BAGPIPE              ####
####   BwA-Gatk PIPeline for Eddie   ####
#### This tool get all NGS files in  ####
#### a folder and run the whole pipe ####
#### extracting variants in a vcf.   ####
####             v 1.0.0             ####
#########################################

# Main method
def main():
    
    #############################
    ### Load modules required ###
    #############################
    import os
    from os.path import isfile as Isfile
    from lib.roadrunner.RoadRunner import roadRunnerParallel
    from lib.Utilities import areFile, counter, settings, autosubmit
    from lib.Utilities import regenerateInfo, fileListSanityCheck, checkFolder
    from lib.rawlib.DataManagement import CheckInputFiles
    from lib.MasterClass import masterOfPuppets
    from lib.GridOptions import SlurmEngine, GridEngine


    #############################
    ####   Initial settings  ####
    #############################
    # Getting options and settings from command line.
    opts = settings()
    # Print name and logo
    print opts.logo
    print opts.extendedName
    print "Version: {}\n\n".format(opts.version)

    # Parsing options
    opts.parser()
    opts.define_arguments()
    skip_data_creation = False
    # Getting number of steps.
    nstep = counter()
    prgFolder = os.path.dirname(os.path.realpath(__file__))
    # Define global grid engine
    if opts.clustermode == "SGE":
        engine = GridEngine()
    elif opts.clustermode == "SLURM":
        engine = SlurmEngine()
        
    
    # Check if folder structure is complete    
    if (os.path.isfile("./LISTS/allready.txt") or os.path.isfile("./LISTS/allDone")) and os.path.isfile("./CommandToRun.txt"):
        res = checkFolder(opts, engine)
        if res:
            return 0
        

    # Making folder structure to save scripts, lists and logs.
    if not os.path.exists("./SCRIPTS"): os.mkdir("./SCRIPTS")
    if not os.path.exists("./LISTS"): os.mkdir("./LISTS")
    if not os.path.exists("./LOGS"): os.mkdir("./LOGS")
    outcommand=open('CommandToRun.txt', 'w')

    # Create job manager
    roadrunner = roadRunnerParallel(engine)
    
    # Check input data
    if Isfile(opts.filename) and not skip_data_creation:
        sample_list, sample_dict, filename, nsamples = CheckInputFiles(opts, outcommand, nstep, engine, roadrunner)
    elif Isfile(opts.filename) and skip_data_creation:
        print "Skipping data creation"
    else:
        exit('No file with samples provided.')   

    #############################
    #### Generate file lists ####
    #############################
    if (opts.aligner == "vg" or opts.aligner == "graphaligner") and opts.do_graph_alignment.lower() == "y" and not opts.atac:
        from lib.Pipelines import runGraphPipeline
        runGraphPipeline(opts, sample_dict, sample_list, filename, roadrunner, outcommand, nstep, nsamples, prgFolder)
    elif (opts.aligner == "vg" or opts.aligner == "graphaligner") and opts.do_graph_alignment.lower() == "y" and opts.atac:
        from lib.Pipelines import runAtacGraphPipeline
        runAtacGraphPipeline(opts, sample_dict, sample_list, filename, roadrunner, outcommand, nstep, nsamples)

    elif opts.atac:
        from lib.Pipelines import runAtacPipeline
        runAtacPipeline(opts, sample_dict, sample_list, filename, roadrunner, outcommand, nstep, nsamples)

    elif opts.rrbs:
        from lib.Pipelines import runRRBSpipeline
        runRRBSpipeline(opts, sample_dict, sample_list, filename, roadrunner, outcommand, nstep, nsamples)

    else:
        #############################
        ####  Process raw fastq  ####
        #############################
        from lib.Pipelines import runWGSpipeline
        runWGSpipeline(opts, sample_dict, sample_list, filename, roadrunner, outcommand, nstep, nsamples, prgFolder)
            

    # Closing command collector.
    outcommand.close()

    # Create that specifies that everything is ready.
    of = open("LISTS/allDone", "w")
    of.write("All ready to run.")
    of.close()
    if not opts.atac and opts.graph_genome_path.lower() is None and opts.graph_genome_path.lower() == "" and opts.do_graph_alignment.lower() == "n":
        fileListSanityCheck()

    # Autosubmit up to the final staging out.
    if skip_data_creation == False:
        if opts.superviseAll == True:
            m = masterOfPuppets(roadrunner, opts)
            m.start()
        else:
            autosubmit(opts, roadrunner.commandlist)
    print 'Everything done.'
    return 0




if __name__ == '__main__':

    import sys
    if sys.version_info[0] >= 3:
        raise Exception("Must be using Python 2")

    main()

    print "\n\n\n\n"
    print "Run the following command, all together or one by one:\n"
    for i in open("CommandToRun.txt"): print i.strip()
    print "\n\nNote that if you submit more than one script, it will wait for the previous step to end before start"
    print "\nYou can find the command to run in the exact order within the file CommandToRun.txt\nThat's all folks!!!\n"
