# BAGpipe
## BwA-Gatk pipeline for eddie

## Introduction
*BAGpipe* (BwA-Gatk pipeline for eddie) allow to create the set of script needed to process short-reads whole genome sequencing data. 
The pipeline is meant to work better on a SGE cluster environment, exploiting several parameters to optimize the run time. However, the scripts generated can be run locally, although this function has not been fully tested. 

## Dependencies
As a full genome data processing pipeline, *BAGpipe* require several dependencies which can be found [here](https://bitbucket.org/renzo_tale/bagpipe/src/master/config/modules.set).
This file also work as a guide for bagpipe to import modules, locally compiled executables and anaconda environments. Prior to run bagpipe, it is recommended to copy this file and edit it for your environment. You will have to pass it to bagpipe as an additional argument at run time.

## System
Currently, bagpipe is written to run on cluster environments. It currently supports gridEngine and SLURM (untested due to lack of access to a SLURM system). 
A local implementation is currently under development, and will be available in the future.

# Usage
The best way to use *BAGpipe* is to fill the configuration file provided, and then 
run the software on that:

	python bagpipe.py -P configfile.config -G modules.set

If you don't need some steps, simply comment them with "#" (e.g. dropping the GRIDSS jar file will automatically lock the SV detection).

## Configuration files
Within the configuration file you hav to specify all the pieces of information needed by the software to generate the lists and scripts. Among them, the most relevant are:
 1. The full path to the reference genome
 2. A file with the list of samples to process
This file will need to contain the information to where find the data. Each line will be as follow:

    Sample1 L:./SAMPLES/S1
    Sample2 S:SRR123431221
    Sample2 E:SRR123431222

The lines can be read as follow:
 1. Sample called Sample1 can be found locally (L:) in the path specified. Within the folder there can be multiple libraries, either paired or single end (the software will identify them and gather them)
 2. Sample called Sample2 has a library on SRA (S:) with code SRR123431221. The script will submit a job that will download the data from SRA ftp server and then convert them to fastq.gz
 3. Sample called Sample2 has a second library on ENA (E:) with code SRR123431222. The script will submit another job to download the data from ENA ftp server.

To properly download the data from SRA/ENA, the software need to have access to the submission system. If this is not possible, the only solution is to download the data manually, create a file list with the local file position and process them. 

## Whole genome re-sequencing processing
Whole genome re-sequencing processing is the main feature in *BAGpipe*, used as default for most of the analysis unless otherwise specified. 
Starting from raw FASTQ data, the pipeline will perform:

 - Genome and vcf indexing using [BWA](http://bio-bwa.sourceforge.net/) and [GATK4](https://software.broadinstitute.org/gatk/)
 - Creation of folder structure, identification of pairs of libraries, download from ENA/SRA using read access codes
 - Perform quality evaluation of reads using [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)
 - Trim sequence using either [trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic) or [trimGalore](https://www.bioinformatics.babraham.ac.uk/projects/trim_galore/)
 - Align sequences using [BWA](http://bio-bwa.sourceforge.net/) or [minimap2](https://github.com/lh3/minimap2)
 - Process aligned reads using [samtools](http://samtools.sourceforge.net/) and [GATK4](https://software.broadinstitute.org/gatk/)
 - Mark duplicates using [GATK4](https://software.broadinstitute.org/gatk/)
 - Perform Base Quality Recalibration using [GATK4](https://software.broadinstitute.org/gatk/)
 - Call SV using [GRIDSS](https://github.com/PapenfussLab/gridss)
 - Perform Variant call using HaplotypeCaller built in [GATK4](https://software.broadinstitute.org/gatk/) or [freebayes](https://github.com/ekg/freebayes)
 
*BAGpipe* also include a series of script to perform joint genotyping through GenomicDBImport and GenotypeGVCFs.
To run analyses faster, it is possible to scatter the dataset in two separate moments:

 1. Alignment: specify the number of reads per sub-fastq, allowing for multiple alignments concurrently. The pipeline will take care of combining them back to a single dataset after the post-processing steps  (can reduce the run time from several days to < 24h).
 2. HaplotypeCaller/FreeBayes: process the genome in chunks of 10Mb a time to speed up the processing (can reduce the run time from several days to few hours).

## ATAC-seq processing
*BAGpipe* now implements a fully-functional Genrich pipeline. To use this pipeline, simply set atac=$1 in the parameter file.
Starting from raw FASTQ data, the pipeline will perform:

 - Genome indexing using [bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml)
 - Trim sequence using [trimGalore](https://www.bioinformatics.babraham.ac.uk/projects/trim_galore/)
 - Perform quality evaluation of reads using [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)
 - Align sequences using [bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml)
 - Call ATAC peaks using [Genrich](https://github.com/jsh58/Genrich)

The pipeline will automatically download the mouse genome and use it to remove chimeric reads and peaks.
Similarly to the WGS pipeline, it is possible to reduce the run time performing the alignment chunking the original fastq into multiple smaller fastqs.

## RRBS processing
*BAGpipe* implements also a Reduced representation bisulfite sequencing pipeline (still under testing). To use this pipeline, simply set rrbs=$1 in the parameter file.
Starting from raw FASTQ data, the pipeline will perform:

 - Genome indexing using [Bismark](https://www.bioinformatics.babraham.ac.uk/projects/bismark/)
 - Trim sequence using [trimGalore](https://www.bioinformatics.babraham.ac.uk/projects/trim_galore/)
 - Perform quality evaluation of reads using [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)
 - Align sequences using [Bismark](https://www.bioinformatics.babraham.ac.uk/projects/bismark/)
 - Call ATAC peaks using [Bismark](https://www.bioinformatics.babraham.ac.uk/projects/bismark/)

The pipeline will automatically download the mouse genome and use it to remove chimeric reads and peaks.
Similarly to the WGS pipeline, it is possible to reduce the run time performing the alignment chunking the original fastq into multiple smaller fastqs.

## Graph whole genome sequencing pipeline
If a graph genome is available (vcf file, vg or xg+GCSA2 files), it is possible to run *BAGpipe* in the graph genome mode. 
Most of the steps in this pipeline are performed through [vg](https://github.com/vgteam/vg), including genome indexing, alignment, chunking, augmentation and variant calling. Be sure to be using the version >=1.22.0, since it's the version that proven to be running within this pipeline from top to bottom without issues.
If long reads are available, it possible to use [GraphAligner](https://github.com/maickrau/GraphAligner) instead of [vg](https://github.com/vgteam/vg), which provides faster mapping for long reads (PacBio and ONT). 
If no variants are specified, it is possible to force the use of the graph pipeline anyway by setting graphpipeline=$1. This will generate a linear graph, without variants into it. 

# Upcoming functionalities
## CHiP-seq graph pipeline
The CHiP-seq graph pipeline is ready to use, but still untested. 

## ATAC-seq graph pipeline 
The ATAC-seq graph pipeline still presents some issues that need to be addressed, so it is still not recommended to use it.  

## Samtools mpileup
Samtools is currently used for several steps within the pipeline, but at the moment is not supported for variant calling. This function will be added in future releases

## Pool-seq
Despite the ability to handle polyploid genomes, support for pool-seq is currently unavaible. It will be implemented in future releases.

## Miscellaneous folder
In this folder we provide additional scripts and pipelines, implementing functions that are outside the scope of bagpipe, but that can be useful.
Right now, there are several stand-alone scripts and one pipeline:
1. JointTyper v4: perform joint genotyping on a set of samples. It uses GATK v4.0 or newer and high parallel processing on a distributed system. Very fast, can call hundreds of samples in few days
2. JointTyper V4.1: perform joint genotyping on a set of samples. It uses GATK v4.1 or newer, adding samples iteratively to intervals, processing them in batches. Slower than the previous version, but saves a lot of spaces using just some samples at the same time.

# Compiled version of the software
It is possible to run the script as a compiled, stand-alone software using pyinstaller:

    pyinstaller compile.spec

This will create the executable:
    
    ./dist/bagpipe

That can be run as a stnd-alone software.
